#  Required tools:
- Java JDK 1.8
- Maven
- Eclipse (Cucumber Eclipse Plugin)
- Project Lombok (Install Lombok: https://www.baeldung.com/lombok-ide)

- Jackson Databind

##   How to run: 
- Go to project folder
- $ mvn clean verify -Dwebdriver.driver=chrome -Denvironment=default -Dcucumber.filter.tags="@smoke"
