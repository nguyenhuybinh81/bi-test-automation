@offer @programs-management
Feature: Programs Management

	Background: 
    Given the admin launches the web
    And the admin 'admin01' has been already existed in the system
    And the 'Program' entity has not been already existed any 'Auto_%' in the system
    And the admin login to the system successfully
    And the admin goes to 'Programs List' page
    And the admin is on 'Programs List' page
    And the admin clicks 'Add New Item' button on Programs List page
    And the admin is on 'Add New Program Detail' page
    
  @regression @smoke 
  Scenario: Verify the admin creates new program successfully
    When the admin inputs information on Program Detail page for 'Required Info' tab following details:
      | denomination         | deliveryDate | address            | city            | mainCityAround | mainCityAroundPostalCode | country        | accountingUnit  | accountingCenter  | cleOperationCommerciale       |
      | Auto_Denomination 03 | 07/07/2021   | 135 Madison Avenue | Aix-en-Provence | MainCityA1     |                      099 | American Samoa | accountingUnit1 | accountingCenter1 | Qu'est-ce que le Lorem Ipsum? |
    And the admin inputs information on Program Detail page for 'Localisation' tab following details:
      | complementAddress1 | complementAddress2 | complementAddress3  | postalCode | longitude  | latitude  |
      | 123 Phan Xich Long | 456 Phan Dang Luu  | 789 Dinh Tien Hoang |        084 | 106.660172 | 10.762622 |
    And the admin inputs information on Program Detail page for 'ADV' tab following details:
      | iBANNumber  | programNotarySocialName  | programNotaryContactName  | programNotaryPhoneNumber | notaryAddress     |
      | IBANNumber1 | ProgramNotarySocialName1 | ProgramNotaryContactName1 | 03.40.49.91.81           | 69 Place Napoléon |
    And the admin inputs information on Program Detail page for 'Commercial' tab following details:
      | commercialProgramPhoneNumber | commercialOpeningDate | teasingDateStarts | teasingDateEnds | privateSellingCode | programCommercialPunchLine | programCommercialArgumentShortText | programCommercialArgumentLongText | programCommercialArgumentLongHTML                                                             | orbital3DModel            |
      | 05.83.14.13.33               | 08/07/2021            | 09/07/2021        | 10/07/2021      |                099 |                         01 | Pourquoi l'utiliser?               | Où puis-je m'en procurer?         | Contrairement à une opinion répandue, le Lorem Ipsum n'est pas simplement du texte aléatoire. | Où puis-je m'en procurer? |
    And the admin inputs information on Program Detail page for 'Media' tab following details:
      | immersive3DVisitURL | programSuburbVisit  | programVideoURL      | programLegalGeneralNotice  | programExternalPerspective1 | programExternalPerspective2 | programInternalPerspective1 | programInternalPerspective2 | programExternalPerspective1Description  | programExternalPerspective2Description  | programInternalPerspective1Description  | programInternalPerspective2Description  | programGroundPlane  |
      | http://www.3d.com   | programSuburbVisit1 | http://www.video.com | programLegalGeneralNotice1 | programExternalPerspective1 | programExternalPerspective2 | programInternalPerspective1 | programInternalPerspective2 | programExternalPerspective1Description1 | programExternalPerspective2Description2 | programInternalPerspective1Description1 | programInternalPerspective2Description2 | programGroundPlane1 |
    And the admin clicks 'Save' button on Program Detail page
    And the admin should see 'The new program has been created successfully' message on Confirmation popup
    And the admin is on 'Programs List' page
    And the admin clicks Edit button on the record just added on 'Programs List' page
    Then the admin is on 'Edit Program Detail' page
    And the admin should see information on 'Required Info' tab on Program Detail page
    And the admin should see information on 'Localisation' tab on Program Detail page
    And the admin should see information on 'ADV' tab on Program Detail page
    And the admin should see information on 'Commercial' tab on Program Detail page
    And the admin should see information on 'Log' tab on Program Detail page

  @regression @smoke
  Scenario: Verify the admin updates an existing program successfully
    And the admin inputs information on Program Detail page for 'Required Info' tab following details:
      | denomination         | deliveryDate | address            | city            | mainCityAround | mainCityAroundPostalCode | country        | accountingUnit  | accountingCenter  | cleOperationCommerciale       |
      | Auto_Denomination 05 | 07/07/2021   | 135 Madison Avenue | Aix-en-Provence | MainCityA1     |                      099 | American Samoa | accountingUnit1 | accountingCenter1 | Qu'est-ce que le Lorem Ipsum? |
    And the admin clicks 'Save' button on Program Detail page
    And the admin is on 'Programs List' page
    When the admin clicks Edit button on the record just added on 'Programs List' page
    And the admin is on 'Edit Program Detail' page
    And the admin inputs information on Program Detail page for 'Required Info' tab following details:
      | denomination                 | deliveryDate | address                    | city  | mainCityAround     | mainCityAroundPostalCode | country | accountingUnit          | accountingCenter          | cleOperationCommerciale  |
      | Auto_Denomination 05 updated | 07/12/2021   | 135 Madison Avenue updated | Brest | MainCityA1 updated |                      100 | Albania | accountingUnit1 updated | accountingCenter1 updated | Qu'est-ce que le updated |
    And the admin inputs information on Program Detail page for 'Localisation' tab following details:
      | complementAddress1         | complementAddress2        | complementAddress3          | postalCode | longitude          | latitude          |
      | 123 Phan Xich Long updated | 456 Phan Dang Luu updated | 789 Dinh Tien Hoang updated | +33        | 106.660172 updated | 10.762622 updated |
    And the admin inputs information on Program Detail page for 'ADV' tab following details:
      | iBANNumber          | programNotarySocialName          | programNotaryContactName          | programNotaryPhoneNumber | notaryAddress             |
      | IBANNumber1 updated | ProgramNotarySocialName1 updated | ProgramNotaryContactName1 updated | 03.40.49.91.81 updated   | 69 Place Napoléon updated |
    And the admin inputs information on Program Detail page for 'Commercial' tab following details:
      | commercialProgramPhoneNumber | commercialOpeningDate | teasingDateStarts | teasingDateEnds | privateSellingCode | programCommercialPunchLine | programCommercialArgumentShortText | programCommercialArgumentLongText | programCommercialArgumentLongHTML                                                                     | orbital3DModel                    |
      | 05.83.14.13.33 updated       | 08/12/2021            | 09/12/2021        | 10/12/2021      | 099 updated        | 01 updated                 | Pourquoi l'utiliser? updated       | Où puis-je m'en procurer? updated | Contrairement à une opinion répandue, le Lorem Ipsum n'est pas simplement du texte aléatoire updated. | Où puis-je m'en procurer? updated |
    And the admin inputs information on Program Detail page for 'Media' tab following details:
      | immersive3DVisitURL | programSuburbVisit          | programVideoURL              | programLegalGeneralNotice          | programExternalPerspective1         | programExternalPerspective2         | programInternalPerspective1         | programInternalPerspective2         | programExternalPerspective1Description          | programExternalPerspective2Description          | programInternalPerspective1Description          | programInternalPerspective2Description          | programGroundPlane          |
      | http://www.5d.com   | programSuburbVisit1 updated | http://www.video-updated.com | programLegalGeneralNotice1 updated | programExternalPerspective1 updated | programExternalPerspective2 updated | programInternalPerspective1 updated | programInternalPerspective2 updated | programExternalPerspective1Description1 updated | programExternalPerspective2Description2 updated | programInternalPerspective1Description1 updated | programInternalPerspective2Description2 updated | programGroundPlane1 updated |
    And the admin clicks 'Save' button on Program Detail page
    And the admin should see 'The program has been modified successfully' message on Confirmation popup
    And the admin is on 'Programs List' page
    And the admin clicks Edit button on the record just added on 'Programs List' page
    Then the admin is on 'Edit Program Detail' page
    And the admin should see information on 'Required Info' tab on Program Detail page
    And the admin should see information on 'Localisation' tab on Program Detail page
    And the admin should see information on 'ADV' tab on Program Detail page
    And the admin should see information on 'Commercial' tab on Program Detail page
    And the admin should see information on 'Log' tab on Program Detail page

  @regression @smoke
  Scenario: Verify the admin deletes a program successfully
    And the admin inputs information on Program Detail page for 'Required Info' tab following details:
      | denomination         | deliveryDate | address            | city            | mainCityAround | mainCityAroundPostalCode | country        | accountingUnit  | accountingCenter  | cleOperationCommerciale       |
      | Auto_Denomination 06 | 07/07/2021   | 135 Madison Avenue | Aix-en-Provence | MainCityA1     |                      099 | American Samoa | accountingUnit1 | accountingCenter1 | Qu'est-ce que le Lorem Ipsum? |
    And the admin clicks 'Save' button on Program Detail page
    And the admin is on 'Programs List' page
    And the admin clicks Edit button on the record just added on 'Programs List' page
    And the admin is on 'Edit Program Detail' page
    When the admin clicks 'Delete' button on Program Detail page
    And the admin clicks 'Delete anyway' on Confirmation popup
    Then the admin should see 'The program has been deleted successfully' message on Confirmation popup
    And the admin should not see the record just deleted on 'Programs List' page
