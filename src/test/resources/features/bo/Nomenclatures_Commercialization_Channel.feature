@nomenclatures @commercialization-channel
Feature: Commercialization Channel

  Background: 
    Given the admin launches the web
    And the admin 'admin01' has been already existed in the system
    And the admin login to the system successfully

  @regression @ui 
  Scenario: Verify UI Commercialization Channel list page
    When the admin goes to 'Commercialization Channel List' page
    Then the admin is on 'Commercialization Channel List' page
    And the admin should see breadscrumb 'Dashboard›Nomenclatures›Commercialization Channel' on 'Commercialization Channel List' page
    And the admin should see information on Commercialization Channel List page following details:
      | component                   | text                               |
      | change display order button | Change display order on front page |
      | show delete items checkbox  | Show deleted items                 |
      | add new button              | Add Commercialization Channel      |
    And the admin should see pagination on 'Commercialization Channel List' page
    And the admin should see table on 'Commercialization Channel List' page following details:
      | columnIndex | columnName             |
      |           1 | Name                   |
      |           2 | ID                     |
      |           3 | Last Modification Date |
      |           4 | Status                 |
      |           5 | Action                 |

  @regression @ui 
  Scenario: Verify the admin changes display order successfully on Commercialization Channel list
    Given the admin goes to 'Commercialization Channel List' page
    And the admin is on 'Commercialization Channel List' page
    When the admin clicks 'Change display order on front page' button on Commercialization Channel List page
    And the admin changes display order on 'Commercialization Channel List' page
    Then the admin should see 'The nomenclature has been modified successfully' message on Confirmation popup
    And the admin should see new display order on 'Commercialization Channel List' page

  @regression @smoke 
  Scenario: Verify the admin creates Commercialization Channel successfully when inputting all fields
    Given the 'Commercialization Channel' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Commercialization Channel List' page
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks 'Add Commercialization Channel' button on Commercialization Channel List page
    When the admin inputs information on Commercialization Channel Detail page following details:
      | name                              |
      | Auto_Commercialization_Channel 01 |
    And the admin clicks 'Save' button on Commercialization Channel Detail page
    And the admin should see 'The new nomenclature has been created successfully' message on Confirmation popup
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks Edit button on the record just added on 'Commercialization Channel List' page
    Then the admin is on 'Edit Commercialization Channel Detail' page
    And the admin should see information on Commercialization Channel Detail page

  @regression 
  Scenario: Verify the admin creates Commercialization Channel successfully when inputting required fields
    Given the 'Commercialization Channel' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Commercialization Channel List' page
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks 'Add Commercialization Channel' button on Commercialization Channel List page
    When the admin inputs information on Commercialization Channel Detail page following details:
      | name                              |
      | Auto_Commercialization_Channel 01 |
    And the admin clicks 'Save' button on Commercialization Channel Detail page
    And the admin should see 'The new nomenclature has been created successfully' message on Confirmation popup
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks Edit button on the record just added on 'Commercialization Channel List' page
    Then the admin is on 'Edit Commercialization Channel Detail' page
    And the admin should see information on Commercialization Channel Detail page

  @regression @smoke 
  Scenario: Verify the admin updates Commercialization Channel successfully when inputting all fields
    Given the 'Commercialization Channel' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Commercialization Channel List' page
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks 'Add Commercialization Channel' button on Commercialization Channel List page
    And the admin inputs information on Commercialization Channel Detail page following details:
      | name                              |
      | Auto_Commercialization_Channel 02 |
    And the admin clicks 'Save' button on Commercialization Channel Detail page
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks Edit button on the record just added on 'Commercialization Channel List' page
    And the admin is on 'Edit Commercialization Channel Detail' page
    When the admin inputs information on Commercialization Channel Detail page following details:
      | name                                      |
      | Auto_Commercialization_Channel 02 updated |
    And the admin clicks 'Save' button on Commercialization Channel Detail page
    And the admin should see 'The nomenclature has been modified successfully' message on Confirmation popup
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks Edit button on the record just added on 'Commercialization Channel List' page
    Then the admin is on 'Edit Commercialization Channel Detail' page
    And the admin should see information on Commercialization Channel Detail page

  @regression 
  Scenario: Verify the admin updates Commercialization Channel successfully when inputting required fields
    Given the 'Commercialization Channel' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Commercialization Channel List' page
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks 'Add Commercialization Channel' button on Commercialization Channel List page
    And the admin inputs information on Commercialization Channel Detail page following details:
      | name                              |
      | Auto_Commercialization_Channel 03 |
    And the admin clicks 'Save' button on Commercialization Channel Detail page
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks Edit button on the record just added on 'Commercialization Channel List' page
    And the admin is on 'Edit Commercialization Channel Detail' page
    When the admin inputs information on Commercialization Channel Detail page following details:
      | name                                      |
      | Auto_Commercialization_Channel 03 updated |
    And the admin clicks 'Save' button on Commercialization Channel Detail page
    And the admin should see 'The nomenclature has been modified successfully' message on Confirmation popup
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks Edit button on the record just added on 'Commercialization Channel List' page
    Then the admin is on 'Edit Commercialization Channel Detail' page
    And the admin should see information on Commercialization Channel Detail page

  @regression @smoke 
  Scenario: Verify the admin deletes Commercialization Channel successfully
    Given the 'Commercialization Channel' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Commercialization Channel List' page
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks 'Add Commercialization Channel' button on Commercialization Channel List page
    And the admin inputs information on Commercialization Channel Detail page following details:
      | name                              |
      | Auto_Commercialization_Channel 04 |
    And the admin clicks 'Save' button on Commercialization Channel Detail page
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks Edit button on the record just added on 'Commercialization Channel List' page
    And the admin is on 'Edit Commercialization Channel Detail' page
    When the admin clicks 'Delete' button on Commercialization Channel Detail page
    And the admin clicks 'Cancel' on Confirmation popup
    And the admin is on 'Edit Commercialization Channel Detail' page
    And the admin clicks 'Delete' button on Commercialization Channel Detail page
    And the admin clicks 'Delete anyway' on Confirmation popup
    Then the admin should see 'The nomenclature has been deleted successfully' message on Confirmation popup
    And the admin should not see the record just deleted on 'Commercialization Channel List' page

  @regression 
  Scenario: Verify the admin restores Commercialization Channel successfully
    Given the 'Commercialization Channel' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Commercialization Channel List' page
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks 'Add Commercialization Channel' button on Commercialization Channel List page
    And the admin inputs information on Commercialization Channel Detail page following details:
      | name                              |
      | Auto_Commercialization_Channel 05 |
    And the admin clicks 'Save' button on Commercialization Channel Detail page
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks Edit button on the record just added on 'Commercialization Channel List' page
    And the admin is on 'Edit Commercialization Channel Detail' page
    And the admin clicks 'Delete' button on Commercialization Channel Detail page
    And the admin clicks 'Delete anyway' on Confirmation popup
    And the admin should not see the record just deleted on 'Commercialization Channel List' page
    And the admin goes to 'Commercialization Channel Detail' page
    And the admin is on 'Edit Commercialization Channel Detail' page
    And the admin clicks 'Restore' button on Commercialization Channel Detail page
    And the admin should see 'The nomenclature has been restored successfully' message on Confirmation popup
    And the admin is on 'Commercialization Channel List' page
    And the admin goes to 'Commercialization Channel Detail' page
    Then the admin is on 'Edit Commercialization Channel Detail' page
    And the admin should see information on Commercialization Channel Detail page

  @regression
  Scenario: Verify the admin updates an inactive Commercialization Channel successfully when inputting all fields
    Given the 'Commercialization Channel' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Commercialization Channel List' page
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks 'Add Commercialization Channel' button on Commercialization Channel List page
    And the admin inputs information on Commercialization Channel Detail page following details:
      | name                              |
      | Auto_Commercialization_Channel 06 |
    And the admin clicks 'Save' button on Commercialization Channel Detail page
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks Edit button on the record just added on 'Commercialization Channel List' page
    And the admin is on 'Edit Commercialization Channel Detail' page
    And the admin clicks 'Delete' button on Commercialization Channel Detail page
    And the admin clicks 'Delete anyway' on Confirmation popup
    And the admin should not see the record just deleted on 'Commercialization Channel List' page
    And the admin goes to 'Commercialization Channel Detail' page
    And the admin is on 'Edit Commercialization Channel Detail' page
    And the admin inputs information on Commercialization Channel Detail page following details:
      | name                                              |
      | Auto_Commercialization_Channel 06 update inactive |
    And the admin clicks 'Save' button on Commercialization Channel Detail page
    And the admin should see 'The nomenclature has been modified successfully' message on Confirmation popup
    And the admin is on 'Commercialization Channel List' page
    And the admin goes to 'Commercialization Channel Detail' page
    Then the admin is on 'Edit Commercialization Channel Detail' page
    And the admin should see information on Commercialization Channel Detail page

  @regression 
  Scenario: Verify the admin creates Commercialization Channel unsuccessfully when no inputting required fields
    Given the 'Commercialization Channel' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Commercialization Channel List' page
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks 'Add Commercialization Channel' button on Commercialization Channel List page
    When the admin inputs information on Commercialization Channel Detail page following details:
      | name |
      |      |
    Then the admin should see validation message on Commercialization Channel Detail page following details:
      | name             |
      | Name is required |

  @regression 
  Scenario: Verify the admin updates Commercialization Channel unsuccessfully when no inputting required fields
    Given the 'Commercialization Channel' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Commercialization Channel List' page
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks 'Add Commercialization Channel' button on Commercialization Channel List page
    And the admin inputs information on Commercialization Channel Detail page following details:
      | name                              |
      | Auto_Commercialization_Channel 06 |
    And the admin clicks 'Save' button on Commercialization Channel Detail page
    And the admin is on 'Commercialization Channel List' page
    And the admin clicks Edit button on the record just added on 'Commercialization Channel List' page
    And the admin is on 'Edit Commercialization Channel Detail' page
    When the admin inputs information on Commercialization Channel Detail page following details:
      | name |
      |      |
    Then the admin should see validation message on Commercialization Channel Detail page following details:
      | name             |
      | Name is required |
