Feature: Beds

  Background: 
    Given the admin launches the web
    And the admin 'admin01' has been already existed in the system
    And the admin login to the system successfully

  @regression @ui 
  Scenario: Verify UI Beds list page
    When the admin goes to 'Beds List' page
    Then the admin is on 'Beds List' page
    And the admin should see breadscrumb 'Dashboard›Nomenclatures›Beds' on 'Beds List' page
    And the admin should see information on Beds List page following details:
      | component                   | text                               |
      | change display order button | Change display order on front page |
      | show delete items checkbox  | Show deleted items                 |
      | add new button              | Add Bed                            |
    And the admin should see pagination on 'Beds List' page
    And the admin should see table on 'Beds List' page following details:
      | columnIndex | columnName             |
      |           1 |                        |
      |           2 | Name                   |
      |           3 | ID                     |
      |           4 | Number of beds adults  |
      |           5 | Number of beds child   |
      |           6 | Last Modification Date |
      |           7 | Status                 |
      |           8 | Action                 |

  @regression @ui 
  Scenario: Verify the admin changes display order successfully on Beds list
    Given the admin goes to 'Beds List' page
    And the admin is on 'Beds List' page
    When the admin clicks 'Change display order on front page' button on Beds List page
    And the admin changes display order on 'Beds List' page
    Then the admin should see 'The nomenclature has been modified successfully' message on Confirmation popup
    And the admin should see new display order on 'Beds List' page

  @regression @smoke 
  Scenario: Verify the admin creates Beds successfully when inputting all fields
    Given the 'Bed' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Beds List' page
    And the admin is on 'Beds List' page
    And the admin clicks 'Add Bed' button on Beds List page
    When the admin inputs information on Beds Detail page following details:
      | name        | numberOfBedsForAdults | numberOfBedsForChild | uploadPicture  |
      | Auto_Bed 01 |                     1 |                    2 | bed/bed-01.jpg |
    And the admin clicks 'Save' button on Beds Detail page
    And the admin should see 'The new nomenclature has been created successfully' message on Confirmation popup
    And the admin is on 'Beds List' page
    And the admin clicks Edit button on the record just added on 'Beds List' page
    Then the admin is on 'Edit Beds Detail' page
    And the admin should see information on Beds Detail page

  @regression 
  Scenario: Verify the admin creates Beds successfully when inputting required fields
    Given the 'Bed' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Beds List' page
    And the admin is on 'Beds List' page
    And the admin clicks 'Add Bed' button on Beds List page
    When the admin inputs information on Beds Detail page following details:
      | name        | numberOfBedsForAdults | numberOfBedsForChild | uploadPicture |
      | Auto_Bed 01 |                     1 |                    2 |               |
    And the admin clicks 'Save' button on Beds Detail page
    And the admin should see 'The new nomenclature has been created successfully' message on Confirmation popup
    And the admin is on 'Beds List' page
    And the admin clicks Edit button on the record just added on 'Beds List' page
    Then the admin is on 'Edit Beds Detail' page
    And the admin should see information on Beds Detail page

  @regression @smoke 
  Scenario: Verify the admin updates Beds successfully when inputting all fields
    Given the 'Bed' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Beds List' page
    And the admin is on 'Beds List' page
    And the admin clicks 'Add Bed' button on Beds List page
    And the admin inputs information on Beds Detail page following details:
      | name        | numberOfBedsForAdults | numberOfBedsForChild | uploadPicture |
      | Auto_Bed 02 |                     1 |                    2 |               |
    And the admin clicks 'Save' button on Beds Detail page
    And the admin is on 'Beds List' page
    And the admin clicks Edit button on the record just added on 'Beds List' page
    And the admin is on 'Edit Beds Detail' page
    When the admin inputs information on Beds Detail page following details:
      | name                | numberOfBedsForAdults | numberOfBedsForChild | uploadPicture  |
      | Auto_Bed 02 updated |                     2 |                    4 | bed/bed-02.jpg |
    And the admin clicks 'Save' button on Beds Detail page
    And the admin should see 'The nomenclature has been modified successfully' message on Confirmation popup
    And the admin is on 'Beds List' page
    And the admin clicks Edit button on the record just added on 'Beds List' page
    Then the admin is on 'Edit Beds Detail' page
    And the admin should see information on Beds Detail page

  @regression 
  Scenario: Verify the admin updates Beds successfully when inputting required fields
    Given the 'Bed' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Beds List' page
    And the admin is on 'Beds List' page
    And the admin clicks 'Add Bed' button on Beds List page
    And the admin inputs information on Beds Detail page following details:
      | name        | numberOfBedsForAdults | numberOfBedsForChild | uploadPicture |
      | Auto_Bed 03 |                     1 |                    2 |               |
    And the admin clicks 'Save' button on Beds Detail page
    And the admin is on 'Beds List' page
    And the admin clicks Edit button on the record just added on 'Beds List' page
    And the admin is on 'Edit Beds Detail' page
    When the admin inputs information on Beds Detail page following details:
      | name                | numberOfBedsForAdults | numberOfBedsForChild | uploadPicture |
      | Auto_Bed 03 updated |                     3 |                    6 |               |
    And the admin clicks 'Save' button on Beds Detail page
    And the admin should see 'The nomenclature has been modified successfully' message on Confirmation popup
    And the admin is on 'Beds List' page
    And the admin clicks Edit button on the record just added on 'Beds List' page
    Then the admin is on 'Edit Beds Detail' page
    And the admin should see information on Beds Detail page

  @regression @smoke 
  Scenario: Verify the admin deletes Beds successfully
    Given the 'Bed' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Beds List' page
    And the admin is on 'Beds List' page
    And the admin clicks 'Add Bed' button on Beds List page
    And the admin inputs information on Beds Detail page following details:
      | name        | numberOfBedsForAdults | numberOfBedsForChild | uploadPicture |
      | Auto_Bed 04 |                     1 |                    2 |               |
    And the admin clicks 'Save' button on Beds Detail page
    And the admin is on 'Beds List' page
    And the admin clicks Edit button on the record just added on 'Beds List' page
    And the admin is on 'Edit Beds Detail' page
    When the admin clicks 'Delete' button on Beds Detail page
    And the admin clicks 'Cancel' on Confirmation popup
    And the admin is on 'Edit Beds Detail' page
    And the admin clicks 'Delete' button on Beds Detail page
    And the admin clicks 'Delete anyway' on Confirmation popup
    Then the admin should see 'The nomenclature has been deleted successfully' message on Confirmation popup
    And the admin should not see the record just deleted on 'Beds List' page

  @regression 
  Scenario: Verify the admin restores Beds successfully
    Given the 'Bed' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Beds List' page
    And the admin is on 'Beds List' page
    And the admin clicks 'Add Bed' button on Beds List page
    And the admin inputs information on Beds Detail page following details:
      | name        | numberOfBedsForAdults | numberOfBedsForChild | uploadPicture |
      | Auto_Bed 05 |                     1 |                    2 |               |
    And the admin clicks 'Save' button on Beds Detail page
    And the admin is on 'Beds List' page
    And the admin clicks Edit button on the record just added on 'Beds List' page
    And the admin is on 'Edit Beds Detail' page
    And the admin clicks 'Delete' button on Beds Detail page
    And the admin clicks 'Delete anyway' on Confirmation popup
    And the admin should not see the record just deleted on 'Beds List' page
    And the admin goes to 'Beds Detail' page
    And the admin is on 'Edit Beds Detail' page
    And the admin clicks 'Restore' button on Beds Detail page
    And the admin should see 'The nomenclature has been restored successfully' message on Confirmation popup
    And the admin is on 'Beds List' page
    And the admin goes to 'Beds Detail' page
    Then the admin is on 'Edit Beds Detail' page
    And the admin should see information on Beds Detail page

  @regression 
  Scenario: Verify the admin updates an inactive Beds successfully when inputting all fields
    Given the 'Bed' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Beds List' page
    And the admin is on 'Beds List' page
    And the admin clicks 'Add Bed' button on Beds List page
    And the admin inputs information on Beds Detail page following details:
      | name        | numberOfBedsForAdults | numberOfBedsForChild | uploadPicture |
      | Auto_Bed 06 |                     1 |                    2 |               |
    And the admin clicks 'Save' button on Beds Detail page
    And the admin is on 'Beds List' page
    And the admin clicks Edit button on the record just added on 'Beds List' page
    And the admin is on 'Edit Beds Detail' page
    And the admin clicks 'Delete' button on Beds Detail page
    And the admin clicks 'Delete anyway' on Confirmation popup
    And the admin should not see the record just deleted on 'Beds List' page
    And the admin goes to 'Beds Detail' page
    And the admin is on 'Edit Beds Detail' page
    And the admin inputs information on Beds Detail page following details:
      | name                        | numberOfBedsForAdults | numberOfBedsForChild | uploadPicture |
      | Auto_Bed 06 update inactive |                     2 |                    4 |               |
    And the admin clicks 'Save' button on Beds Detail page
    And the admin should see 'The nomenclature has been modified successfully' message on Confirmation popup
    And the admin is on 'Beds List' page
    And the admin goes to 'Beds Detail' page
    Then the admin is on 'Edit Beds Detail' page
    And the admin should see information on Beds Detail page

  @regression
  Scenario: Verify the admin creates Beds unsuccessfully when no inputting required fields
    Given the 'Bed' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Beds List' page
    And the admin is on 'Beds List' page
    And the admin clicks 'Add Bed' button on Beds List page
    When the admin inputs information on Beds Detail page following details:
      | name | numberOfBedsForAdults | numberOfBedsForChild | uploadPicture |
      |      |                       |                      |               |
    Then the admin should see validation message on Beds Detail page following details:
      | name             | numberOfBedsForAdults                     | numberOfBedsForChild                     | uploadPicture |
      | Name is required | Number of beds for adults must be integer | Number of beds for child must be integer |               |

  @regression
  Scenario: Verify the admin updates Beds unsuccessfully when no inputting required fields
    Given the 'Bed' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Beds List' page
    And the admin is on 'Beds List' page
    And the admin clicks 'Add Bed' button on Beds List page
    And the admin inputs information on Beds Detail page following details:
      | name        | numberOfBedsForAdults | numberOfBedsForChild | uploadPicture |
      | Auto_Bed 07 |                     1 |                    2 |               |
    And the admin clicks 'Save' button on Beds Detail page
    And the admin is on 'Beds List' page
    And the admin clicks Edit button on the record just added on 'Beds List' page
    And the admin is on 'Edit Beds Detail' page
    When the admin inputs information on Beds Detail page following details:
      | name | numberOfBedsForAdults | numberOfBedsForChild | uploadPicture |
      |      |                       |                      |               |
    Then the admin should see validation message on Beds Detail page following details:
      | name             | numberOfBedsForAdults                     | numberOfBedsForChild                     | uploadPicture |
      | Name is required | Number of beds for adults must be integer | Number of beds for child must be integer |               |
