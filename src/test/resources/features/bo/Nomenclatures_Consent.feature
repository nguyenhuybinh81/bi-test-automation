@nomenclatures @consent
Feature: Consent

  Background: 
    Given the admin launches the web
    And the admin 'admin01' has been already existed in the system
    And the admin login to the system successfully

  @regression @ui 
  Scenario: Verify UI Consent list page
    When the admin goes to 'Consent List' page
    Then the admin is on 'Consent List' page
    And the admin should see breadscrumb 'Dashboard›Nomenclatures›Consent' on 'Consent List' page
    And the admin should see information on Consent List page following details:
      | component                   | text                               |
      | change display order button | Change display order on front page |
      | show delete items checkbox  | Show deleted items                 |
      | add new button              | Add Consent                        |
    And the admin should see pagination on 'Consent List' page
    And the admin should see table on 'Consent List' page following details:
      | columnIndex | columnName             |
      |           1 | Question               |
      |           2 | ID                     |
      |           3 | Last Modification Date |
      |           4 | Status                 |
      |           5 | Action                 |

  @regression @ui 
  Scenario: Verify the admin changes display order successfully on Consent list
    Given the admin goes to 'Consent List' page
    And the admin is on 'Consent List' page
    When the admin clicks 'Change display order on front page' button on Consent List page
    And the admin changes display order on 'Consent List' page
    Then the admin should see 'The nomenclature has been modified successfully' message on Confirmation popup
    And the admin should see new display order on 'Consent List' page

  @regression @smoke 
  Scenario: Verify the admin creates Consent successfully when inputting all fields
    Given the 'Consent' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Consent List' page
    And the admin is on 'Consent List' page
    And the admin clicks 'Add Consent' button on Consent List page
    When the admin inputs information on Consent Detail page following details:
      | question        |
      | Auto_Consent 01 |
    And the admin clicks 'Save' button on Consent Detail page
    And the admin should see 'The new nomenclature has been created successfully' message on Confirmation popup
    And the admin is on 'Consent List' page
    And the admin clicks Edit button on the record just added on 'Consent List' page
    Then the admin is on 'Edit Consent Detail' page
    And the admin should see information on Consent Detail page

  @regression @smoke 
  Scenario: Verify the admin updates Consent successfully when inputting all fields
    Given the 'Consent' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Consent List' page
    And the admin is on 'Consent List' page
    And the admin clicks 'Add Consent' button on Consent List page
    And the admin inputs information on Consent Detail page following details:
      | question        |
      | Auto_Consent 02 |
    And the admin clicks 'Save' button on Consent Detail page
    And the admin is on 'Consent List' page
    And the admin clicks Edit button on the record just added on 'Consent List' page
    And the admin is on 'Edit Consent Detail' page
    When the admin inputs information on Consent Detail page following details:
      | question                |
      | Auto_Consent 02 updated |
    And the admin clicks 'Save' button on Consent Detail page
    And the admin should see 'The nomenclature has been modified successfully' message on Confirmation popup
    And the admin is on 'Consent List' page
    And the admin clicks Edit button on the record just added on 'Consent List' page
    Then the admin is on 'Edit Consent Detail' page
    And the admin should see information on Consent Detail page

  @regression @smoke 
  Scenario: Verify the admin deletes Consent successfully
    Given the 'Consent' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Consent List' page
    And the admin is on 'Consent List' page
    And the admin clicks 'Add Consent' button on Consent List page
    And the admin inputs information on Consent Detail page following details:
      | question        |
      | Auto_Consent 03 |
    And the admin clicks 'Save' button on Consent Detail page
    And the admin is on 'Consent List' page
    And the admin clicks Edit button on the record just added on 'Consent List' page
    And the admin is on 'Edit Consent Detail' page
    When the admin clicks 'Delete' button on Consent Detail page
    And the admin clicks 'Cancel' on Confirmation popup
    And the admin is on 'Edit Consent Detail' page
    And the admin clicks 'Delete' button on Consent Detail page
    And the admin clicks 'Delete anyway' on Confirmation popup
    Then the admin should see 'The nomenclature has been deleted successfully' message on Confirmation popup
    And the admin should not see the record just deleted on 'Consent List' page

  @regression 
  Scenario: Verify the admin restores Consent successfully
    Given the 'Consent' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Consent List' page
    And the admin is on 'Consent List' page
    And the admin clicks 'Add Consent' button on Consent List page
    And the admin inputs information on Consent Detail page following details:
      | question        |
      | Auto_Consent 04 |
    And the admin clicks 'Save' button on Consent Detail page
    And the admin is on 'Consent List' page
    And the admin clicks Edit button on the record just added on 'Consent List' page
    And the admin is on 'Edit Consent Detail' page
    And the admin clicks 'Delete' button on Consent Detail page
    And the admin clicks 'Delete anyway' on Confirmation popup
    And the admin should not see the record just deleted on 'Consent List' page
    And the admin goes to 'Consent Detail' page
    And the admin is on 'Edit Consent Detail' page
    And the admin clicks 'Restore' button on Consent Detail page
    And the admin should see 'The nomenclature has been restored successfully' message on Confirmation popup
    And the admin is on 'Consent List' page
    And the admin goes to 'Consent Detail' page
    Then the admin is on 'Edit Consent Detail' page
    And the admin should see information on Consent Detail page

  @regression
  Scenario: Verify the admin updates an inactive Consent successfully when inputting all fields
    Given the 'Consent' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Consent List' page
    And the admin is on 'Consent List' page
    And the admin clicks 'Add Consent' button on Consent List page
    And the admin inputs information on Consent Detail page following details:
      | question        |
      | Auto_Consent 05 |
    And the admin clicks 'Save' button on Consent Detail page
    And the admin is on 'Consent List' page
    And the admin clicks Edit button on the record just added on 'Consent List' page
    And the admin is on 'Edit Consent Detail' page
    And the admin clicks 'Delete' button on Consent Detail page
    And the admin clicks 'Delete anyway' on Confirmation popup
    And the admin should not see the record just deleted on 'Consent List' page
    And the admin goes to 'Consent Detail' page
    And the admin is on 'Edit Consent Detail' page
    And the admin inputs information on Consent Detail page following details:
      | question                        |
      | Auto_Consent 05 update inactive |
    And the admin clicks 'Save' button on Consent Detail page
    And the admin should see 'The nomenclature has been modified successfully' message on Confirmation popup
    And the admin is on 'Consent List' page
    And the admin goes to 'Consent Detail' page
    Then the admin is on 'Edit Consent Detail' page
    And the admin should see information on Consent Detail page

  @regression 
  Scenario: Verify the admin creates Consent unsuccessfully when no inputting required fields
    Given the 'Consent' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Consent List' page
    And the admin is on 'Consent List' page
    And the admin clicks 'Add Consent' button on Consent List page
    When the admin inputs information on Consent Detail page following details:
      | question |
      |          |
    Then the admin should see validation message on Consent Detail page following details:
      | question             |
      | Question is required |

  @regression 
  Scenario: Verify the admin updates Consent unsuccessfully when no inputting required fields
    Given the 'Consent' entity has not been already existed any 'Auto_%' in the system
    And the admin goes to 'Consent List' page
    And the admin is on 'Consent List' page
    And the admin clicks 'Add Consent' button on Consent List page
    And the admin inputs information on Consent Detail page following details:
      | question        |
      | Auto_Consent 05 |
    And the admin clicks 'Save' button on Consent Detail page
    And the admin is on 'Consent List' page
    And the admin clicks Edit button on the record just added on 'Consent List' page
    And the admin is on 'Edit Consent Detail' page
    When the admin inputs information on Consent Detail page following details:
      | question |
      |          |
    Then the admin should see validation message on Consent Detail page following details:
      | question             |
      | Question is required |
