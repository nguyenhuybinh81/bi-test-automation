package com.dirox.bi.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "programs")
public class Program implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(name = "Denomination")
    private String denomination;
    
    private String deliveryDate;
    private String address;
    private String city;
    private String mainCityAround;
    private String mainCityAroundPostalCode;
    private String country;
    private String accountingUnit;
    private String accountingCenter;
    private String cleOperationCommerciale;
    private Localisation localisation;
    private ADV adv;
    private Commercial commercial;
    private Media media;
    private Log log;

    public Program() {
       
    }
    
    public Program(String denomination) {
        this.denomination = denomination;
    }
    
    public String toString() {
        return "Program{id=" + id + ", denomination='" + denomination + "', deliveryDate='" + deliveryDate + "', adddress='" + address + "', city='" + city
                        + "', mainCityAround='" + mainCityAround + "', mainCityAroundPostalCode='" + mainCityAroundPostalCode + "', country='" + country
                        + "', accountingUnit='" + accountingUnit + "', accountingCenter='" + accountingCenter + "', cleOperationCommerciale='"
                        + cleOperationCommerciale + "'}";
    }

}
