package com.dirox.bi.models;

import java.io.Serializable;

import lombok.Data;

@Data
public class ADV implements Serializable {
    private static final long serialVersionUID = 1L;
    private String iBANNumber;
    private String programNotarySocialName;
    private String programNotaryContactName;
    private String programNotaryPhoneNumber;
    private String notaryAddress;

    public String toString() {
        return "ADV{iBANNumber = '" + iBANNumber + "', programNotarySocialName='" + programNotarySocialName + "', programNotaryContactName='"
                        + programNotaryContactName + "', programNotaryPhoneNumber='" + programNotaryPhoneNumber + "', notaryAddress='" + notaryAddress + "'}";
    }

}
