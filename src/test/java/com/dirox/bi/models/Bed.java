package com.dirox.bi.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "beds")
public class Bed implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "name")
    private String name;
    private String numberOfBedsForAdults;
    private String numberOfBedsForChild;
    private String lastModificationDate;
    private String status;
    private String uploadPicture;
    private String isDeleted;

    public Bed() {
    }

    public Bed(String name) {
        this.name = name;
    }

    public String toString() {
        return "Bed{id = '" + id + "', name='" + name + "', numberOfBedsForAdults=" + numberOfBedsForAdults + ", numberOfBedsForChild=" + numberOfBedsForChild
                        + ", uploadPicture='" + uploadPicture + "', lastModificationDate='" + lastModificationDate + "', status='" + status + "', isDeleted='" + isDeleted + "'}";
    }
}
