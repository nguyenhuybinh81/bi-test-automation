package com.dirox.bi.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "commercializationchannel")
public class CommercializationChannel implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "name")
    private String name;
    private String lastModificationDate;
    private String status;
    private String isDeleted;

    public CommercializationChannel() {
    }

    public CommercializationChannel(String name) {
        this.name = name;
    }

    public String toString() {
        return "CommercializationChannel{id = '" + id + "', name='" + name + "', lastModificationDate='" + lastModificationDate + "', status='" + status
                        + "', isDeleted='" + isDeleted + "'}";
    }
}
