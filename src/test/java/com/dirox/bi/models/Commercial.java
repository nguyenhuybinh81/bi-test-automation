package com.dirox.bi.models;

import java.io.Serializable;

import lombok.Data;

@Data
public class Commercial implements Serializable {
    private static final long serialVersionUID = 1L;
    private String commercialProgramPhoneNumber;
    private String commercialOpeningDate;
    private String teasingDateStarts;
    private String teasingDateEnds;
    private String privateSellingCode;
    private String programCommercialPunchLine;
    private String programCommercialArgumentShortText;
    private String programCommercialArgumentLongText;
    private String programCommercialArgumentLongHTML;
    private String orbital3DModel;

    public String toString() {
        return "Commercial{commercialProgramPhoneNumber = '" + commercialProgramPhoneNumber + "', commercialOpeningDate='" + commercialOpeningDate
                        + "', teasingDateStarts='" + teasingDateStarts + "', teasingDateEnds='" + teasingDateEnds + "', privateSellingCode='"
                        + privateSellingCode + "', programCommercialPunchLine='" + programCommercialPunchLine + "', programCommercialArgumentShortText='"
                        + programCommercialArgumentShortText + "', programCommercialArgumentLongText='" + programCommercialArgumentLongText
                        + "', programCommercialArgumentLongHTML='" + programCommercialArgumentLongHTML + "', orbital3DModel='" + orbital3DModel + "'}";
    }

}
