package com.dirox.bi.models;

import java.io.Serializable;

import lombok.Data;

@Data
public class Media implements Serializable {
    private static final long serialVersionUID = 1L;
    private String immersive3DVisitURL;
    private String programSuburbVisit;
    private String programVideoURL;
    private String programLegalGeneralNotice;
    private String programExternalPerspective1;
    private String programExternalPerspective2;
    private String programInternalPerspective1;
    private String programInternalPerspective2;
    private String programExternalPerspective1Description;
    private String programExternalPerspective2Description;
    private String programInternalPerspective1Description;
    private String programInternalPerspective2Description;
    private String programGroundPlane;

    public String toString() {
        return "Media{Immersive3DVisitURL = '" + immersive3DVisitURL + "', programSuburbVisit='" + programSuburbVisit + "', programVideoURL='" + programVideoURL
                        + "', programLegalGeneralNotice='" + programLegalGeneralNotice + "', programExternalPerspective1='" + programExternalPerspective1
                        + "', programExternalPerspective2='" + programExternalPerspective2 + "', programInternalPerspective1='" + programInternalPerspective1
                        + "', programInternalPerspective2='" + programInternalPerspective2 + "', programExternalPerspective1Description='"
                        + programExternalPerspective1Description + "', programExternalPerspective2Description='" + programExternalPerspective2Description
                        + "', programInternalPerspective1Description='" + programInternalPerspective1Description + "', programInternalPerspective2Description='"
                        + programInternalPerspective2Description + "', programGroundPlane='" + programGroundPlane + "'}";
    }

}
