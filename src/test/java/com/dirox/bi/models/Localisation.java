package com.dirox.bi.models;

import java.io.Serializable;

import lombok.Data;

@Data
public class Localisation implements Serializable {
    private static final long serialVersionUID = 1L;
    private String complementAddress1;
    private String complementAddress2;
    private String complementAddress3;
    private String postalCode;
    private String longitude;
    private String latitude;

    public String toString() {
        return "Localisation{complementAddress1 = '" + complementAddress1 + "', complementAddress2='" + complementAddress2 + "', complementAddress3='"
                        + complementAddress3 + "', postalCode='" + postalCode + "', longitude='" + longitude + "', latitude='" + latitude + "'}";
    }

}
