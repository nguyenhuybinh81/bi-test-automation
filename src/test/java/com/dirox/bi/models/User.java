package com.dirox.bi.models;

import java.io.Serializable;

import lombok.Data;

@Data
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    private String username;
    private String email;
    private SMTP smtp;
    
    
    public String toString() {
        return "User{username = '" + username + "', email='" + email + "'}";
    }

}
