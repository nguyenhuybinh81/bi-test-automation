package com.dirox.bi.models;

import java.io.Serializable;

import lombok.Data;

@Data
public class Log implements Serializable {
    private static final long serialVersionUID = 1L;
    private String lastModificationDate;
    private String idAdminModified;
    private String creationDate;
    private String idAdminCreated;
    private String isDeleted;

    public String toString() {
        return "Log{lastModificationDate = '" + lastModificationDate + "', idAdminModified='" + idAdminModified + "', creationDate='"
                        + creationDate + "', idAdminCreated='" + idAdminCreated + "', isDeleted='" + isDeleted + "'}";
    }

}
