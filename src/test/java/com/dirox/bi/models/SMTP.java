package com.dirox.bi.models;

import java.io.Serializable;

import lombok.Data;

@Data
public class SMTP implements Serializable {
    private static final long serialVersionUID = 1L;
    private String smtpEmail;
    private String smtpPassword;

    public String toString() {
        return "SMTP{smtpEmail='" + smtpEmail + "', smtpPassword = '" + smtpPassword + "'}";
    }

}
