package com.dirox.bi.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "consents")
public class Consent implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "question")
    private String question;
    private String lastModificationDate;
    private String status;
    private String isDeleted;

    public Consent() {
    }

    public Consent(String question) {
        this.question = question;
    }

    public String toString() {
        return "Consent{id = '" + id + "', question='" + question + "', lastModificationDate='" + lastModificationDate + "', status='" + status
                        + "', isDeleted='" + isDeleted + "'}";
    }
}
