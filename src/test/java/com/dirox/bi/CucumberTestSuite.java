package com.dirox.bi;

import org.junit.runner.RunWith;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = "src/test/resources/features",
        //features = "src/test/resources/features/bo/Nomenclatures_Beds.feature",
        //features = "src/test/resources/features/bo/Nomenclatures_Commercialization_Channel.feature",
        //features = "src/test/resources/features/bo/Nomenclatures_Consent.feature",
        tags = "not @ignore"
)
public class CucumberTestSuite{
    
}
