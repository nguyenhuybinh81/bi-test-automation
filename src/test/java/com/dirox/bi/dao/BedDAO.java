package com.dirox.bi.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.dirox.bi.models.Bed;
import com.dirox.bi.utils.HibernateUtilFactory;

public class BedDAO extends BaseDAO implements IBaseDAO<Bed> {

    @Override
    public Bed get(Bed bed) {
        SessionFactory factory = null;
        Session session = null;
        try {
            factory = HibernateUtilFactory.getHibernateSessionFactory(dbEnv);
            session = factory.openSession();
            TypedQuery<Bed> query = session.createNativeQuery("SELECT * FROM beds WHERE is_deleted = false AND \"name\" = :name",
                            Bed.class);
            query.setParameter("name", bed.getName());
            return query.getSingleResult();
        } catch (Exception e) {
            LOGGER.error("getBedByName", e);
        } finally {
            if (session != null) {
                session.close();
            }
            if (factory != null) {
                factory.close();
            }
        }
        return null;
    }

    @Override
    public List<Bed> getAll() {
        SessionFactory factory = null;
        Session session = null;
        try {
            factory = HibernateUtilFactory.getHibernateSessionFactory(dbEnv);
            session = factory.openSession();
            TypedQuery<Bed> query = session.createNativeQuery("SELECT * FROM beds WHERE is_deleted = false", Bed.class);
            return query.getResultList();
        } catch (Exception e) {
            LOGGER.error("getAllBed", e);
        } finally {
            if (session != null) {
                session.close();
            }
            if (factory != null) {
                factory.close();
            }
        }
        return null;
    }

    @Override
    public int delete(Bed bed) {
        SessionFactory factory = null;
        Session session = null;
        int result = -1;
        try {
            factory = HibernateUtilFactory.getHibernateSessionFactory(dbEnv);
            session = factory.openSession();
            session.beginTransaction();
            TypedQuery<Bed> query = session.createNativeQuery("DELETE FROM beds WHERE \"name\" like :name", Bed.class);
            query.setParameter("name", bed.getName());
            result = query.executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            LOGGER.error("deleteBed", e);
        } finally {
            if (session != null) {
                session.close();
            }
            if (factory != null) {
                factory.close();
            }
        }
        return result;
    }
    
}
