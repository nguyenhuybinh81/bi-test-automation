package com.dirox.bi.dao;

import java.util.List;

public interface IBaseDAO<T> {
    T get(T t);
    List<T> getAll();
    int delete(T t);
}
