package com.dirox.bi.dao;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class BaseDAO {
    protected static final Logger LOGGER = (Logger) LoggerFactory.getLogger(BaseDAO.class);
    protected static String dbEnv;

    public BaseDAO() {
        EnvironmentVariables environmentVariables = SystemEnvironmentVariables.createEnvironmentVariables();
        dbEnv = EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("db.env");
    }
}
