package com.dirox.bi.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.dirox.bi.models.CommercializationChannel;
import com.dirox.bi.utils.HibernateUtilFactory;

public class CommercializationChannelDAO extends BaseDAO implements IBaseDAO<CommercializationChannel> {

    @Override
    public CommercializationChannel get(CommercializationChannel commercializationChannel) {
        SessionFactory factory = null;
        Session session = null;
        try {
            factory = HibernateUtilFactory.getHibernateSessionFactory(dbEnv);
            session = factory.openSession();
            TypedQuery<CommercializationChannel> query = session.createNativeQuery("SELECT * FROM commercializationchannel WHERE is_deleted = false AND \"name\" = :name",
                            CommercializationChannel.class);
            query.setParameter("name", commercializationChannel.getName());
            return query.getSingleResult();
        } catch (Exception e) {
            LOGGER.error("getCommercializationChannelByName", e);
        } finally {
            if (session != null) {
                session.close();
            }
            if (factory != null) {
                factory.close();
            }
        }
        return null;
    }

    @Override
    public List<CommercializationChannel> getAll() {
        SessionFactory factory = null;
        Session session = null;
        try {
            factory = HibernateUtilFactory.getHibernateSessionFactory(dbEnv);
            session = factory.openSession();
            TypedQuery<CommercializationChannel> query = session.createNativeQuery("SELECT * FROM commercializationchannel WHERE is_deleted = false", CommercializationChannel.class);
            return query.getResultList();
        } catch (Exception e) {
            LOGGER.error("getAllCommercializationChannel", e);
        } finally {
            if (session != null) {
                session.close();
            }
            if (factory != null) {
                factory.close();
            }
        }
        return null;
    }

    @Override
    public int delete(CommercializationChannel commercializationChannel) {
        SessionFactory factory = null;
        Session session = null;
        int result = -1;
        try {
            factory = HibernateUtilFactory.getHibernateSessionFactory(dbEnv);
            session = factory.openSession();
            session.beginTransaction();
            TypedQuery<CommercializationChannel> query = session.createNativeQuery("DELETE FROM commercializationchannel WHERE \"name\" like :name", CommercializationChannel.class);
            query.setParameter("name", commercializationChannel.getName());
            result = query.executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            LOGGER.error("deleteCommercializationChannel", e);
        } finally {
            if (session != null) {
                session.close();
            }
            if (factory != null) {
                factory.close();
            }
        }
        return result;
    }
}
