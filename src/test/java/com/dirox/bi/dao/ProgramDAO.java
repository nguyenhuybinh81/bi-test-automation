package com.dirox.bi.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.dirox.bi.models.Program;
import com.dirox.bi.utils.HibernateUtilFactory;

public class ProgramDAO extends BaseDAO implements IBaseDAO<Program> {

    @Override
    public Program get(Program program) {
        SessionFactory factory = null;
        Session session = null;
        try {
            factory = HibernateUtilFactory.getHibernateSessionFactory(dbEnv);
            session = factory.openSession();
            TypedQuery<Program> query = session.createNativeQuery("SELECT * FROM programs WHERE is_deleted = false AND \"Denomination\" = :denomination",
                            Program.class);
            query.setParameter("denomination", program.getDenomination());
            return query.getSingleResult();
        } catch (Exception e) {
            LOGGER.error("getProgramByDenomination", e);
        } finally {
            if (session != null) {
                session.close();
            }
            if (factory != null) {
                factory.close();
            }
        }
        return null;
    }

    @Override
    public List<Program> getAll() {
        SessionFactory factory = null;
        Session session = null;
        try {
            factory = HibernateUtilFactory.getHibernateSessionFactory(dbEnv);
            session = factory.openSession();
            TypedQuery<Program> query = session.createNativeQuery("SELECT * FROM programs WHERE is_deleted = false", Program.class);
            return query.getResultList();
        } catch (Exception e) {
            LOGGER.error("getAllProgram", e);
        } finally {
            if (session != null) {
                session.close();
            }
            if (factory != null) {
                factory.close();
            }
        }
        return null;
    }

    @Override
    public int delete(Program program) {
        SessionFactory factory = null;
        Session session = null;
        int result = -1;
        try {
            factory = HibernateUtilFactory.getHibernateSessionFactory(dbEnv);
            session = factory.openSession();
            session.beginTransaction();
            TypedQuery<Program> query = session.createNativeQuery("DELETE FROM programs WHERE \"Denomination\" like :denomination", Program.class);
            query.setParameter("denomination", program.getDenomination());
            result = query.executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            LOGGER.error("deleteProgram", e);
        } finally {
            if (session != null) {
                session.close();
            }
            if (factory != null) {
                factory.close();
            }
        }
        return result;
    }
    
}
