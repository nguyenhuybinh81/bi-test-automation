package com.dirox.bi.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.dirox.bi.models.Consent;
import com.dirox.bi.utils.HibernateUtilFactory;

public class ConsentDAO extends BaseDAO implements IBaseDAO<Consent> {

    @Override
    public Consent get(Consent consent) {
        SessionFactory factory = null;
        Session session = null;
        try {
            factory = HibernateUtilFactory.getHibernateSessionFactory(dbEnv);
            session = factory.openSession();
            TypedQuery<Consent> query = session.createNativeQuery("SELECT * FROM consents WHERE is_deleted = false AND \"question\" = :question", Consent.class);
            query.setParameter("question", consent.getQuestion());
            return query.getSingleResult();
        } catch (Exception e) {
            LOGGER.error("getConsentByName", e);
        } finally {
            if (session != null) {
                session.close();
            }
            if (factory != null) {
                factory.close();
            }
        }
        return null;
    }

    @Override
    public List<Consent> getAll() {
        SessionFactory factory = null;
        Session session = null;
        try {
            factory = HibernateUtilFactory.getHibernateSessionFactory(dbEnv);
            session = factory.openSession();
            TypedQuery<Consent> query = session.createNativeQuery("SELECT * FROM consents WHERE is_deleted = false", Consent.class);
            return query.getResultList();
        } catch (Exception e) {
            LOGGER.error("getAllConsent", e);
        } finally {
            if (session != null) {
                session.close();
            }
            if (factory != null) {
                factory.close();
            }
        }
        return null;
    }

    @Override
    public int delete(Consent consent) {
        SessionFactory factory = null;
        Session session = null;
        int result = -1;
        try {
            factory = HibernateUtilFactory.getHibernateSessionFactory(dbEnv);
            session = factory.openSession();
            session.beginTransaction();
            TypedQuery<Consent> query = session.createNativeQuery("DELETE FROM consents WHERE \"question\" like :question", Consent.class);
            query.setParameter("question", consent.getQuestion());
            result = query.executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            LOGGER.error("deleteConsent", e);
        } finally {
            if (session != null) {
                session.close();
            }
            if (factory != null) {
                factory.close();
            }
        }
        return result;
    }
}
