package com.dirox.bi.stepdefinitions;

import java.util.List;
import java.util.Map;

import com.dirox.bi.models.ADV;
import com.dirox.bi.models.Commercial;
import com.dirox.bi.models.Localisation;
import com.dirox.bi.models.Media;
import com.dirox.bi.models.Program;
import com.dirox.bi.serenity.OfferProgramDetailSteps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

public class OfferProgramDetailStepDefs {
    @Steps
    private OfferProgramDetailSteps offerProgramDetailSteps;

    @Then("the admin inputs information on Program Detail page for {string} tab following details:")
    public void the_admin_inputs_information_on_program_detail_page_for_tab_following_details(String tabName, DataTable table) {
        Program program = new Program();
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        for (Map<String, String> row : rows) {
            switch (tabName.trim()) {
                case "Required Info":
                    program.setDenomination(row.get("denomination"));
                    program.setDeliveryDate(row.get("deliveryDate"));
                    program.setAddress(row.get("address"));
                    program.setCity(row.get("city"));
                    program.setMainCityAround(row.get("mainCityAround"));
                    program.setMainCityAroundPostalCode(row.get("mainCityAroundPostalCode"));
                    program.setCountry(row.get("country"));
                    program.setAccountingUnit(row.get("accountingUnit"));
                    program.setAccountingCenter(row.get("accountingCenter"));
                    program.setCleOperationCommerciale(row.get("cleOperationCommerciale"));
                    Serenity.setSessionVariable("program").to(program);
                    offerProgramDetailSteps.input_data_on_required_info_tab(program);
                    break;
                case "Localisation":
                    Localisation localisation = new Localisation();
                    localisation.setComplementAddress1(row.get("complementAddress1"));
                    localisation.setComplementAddress2(row.get("complementAddress2"));
                    localisation.setComplementAddress3(row.get("complementAddress3"));
                    localisation.setPostalCode(row.get("postalCode"));
                    localisation.setLongitude(row.get("longitude"));
                    localisation.setLatitude(row.get("latitude"));
                    if(Serenity.sessionVariableCalled("program") != null) {
                        Program oldProgram = Serenity.sessionVariableCalled("program");
                        oldProgram.setLocalisation(localisation);
                        Program newProgram = oldProgram;
                        Serenity.setSessionVariable("program").to(newProgram);
                        program = Serenity.sessionVariableCalled("program");
                    }
                    offerProgramDetailSteps.input_data_on_localisation_tab(localisation);
                    break;
                case "ADV":
                    ADV adv = new ADV();
                    adv.setIBANNumber(row.get("iBANNumber"));
                    adv.setProgramNotarySocialName(row.get("programNotarySocialName"));
                    adv.setProgramNotaryContactName(row.get("programNotaryContactName"));
                    adv.setProgramNotaryPhoneNumber(row.get("programNotaryPhoneNumber"));
                    adv.setNotaryAddress(row.get("notaryAddress"));
                    if(Serenity.sessionVariableCalled("program") != null) {
                        Program oldProgram = Serenity.sessionVariableCalled("program");
                        oldProgram.setAdv(adv);
                        Program newProgram = oldProgram;
                        Serenity.setSessionVariable("program").to(newProgram);
                        program = Serenity.sessionVariableCalled("program");
                    }
                    offerProgramDetailSteps.input_data_on_adv_tab(adv);
                    break;
                case "Commercial":
                    Commercial commercial = new Commercial();
                    commercial.setCommercialProgramPhoneNumber(row.get("commercialProgramPhoneNumber"));
                    commercial.setCommercialOpeningDate(row.get("commercialOpeningDate"));
                    commercial.setTeasingDateStarts(row.get("teasingDateStarts"));
                    commercial.setTeasingDateEnds(row.get("teasingDateEnds"));
                    commercial.setPrivateSellingCode(row.get("privateSellingCode"));
                    commercial.setProgramCommercialPunchLine(row.get("programCommercialPunchLine"));
                    commercial.setProgramCommercialArgumentShortText(row.get("programCommercialArgumentShortText"));
                    commercial.setProgramCommercialArgumentLongText(row.get("programCommercialArgumentLongText"));
                    commercial.setProgramCommercialArgumentLongHTML(row.get("programCommercialArgumentLongHTML"));
                    commercial.setOrbital3DModel(row.get("orbital3DModel"));
                    if(Serenity.sessionVariableCalled("program") != null) {
                        Program oldProgram = Serenity.sessionVariableCalled("program");
                        oldProgram.setCommercial(commercial);
                        Program newProgram = oldProgram;
                        Serenity.setSessionVariable("program").to(newProgram);
                        program = Serenity.sessionVariableCalled("program");
                    }
                    offerProgramDetailSteps.input_data_on_commercial_tab(commercial);
                    break;
                case "Media":
                    Media media = new Media();
                    media.setImmersive3DVisitURL(row.get("immersive3DVisitURL"));
                    media.setProgramSuburbVisit(row.get("programSuburbVisit"));
                    media.setProgramVideoURL(row.get("programVideoURL"));
                    media.setProgramLegalGeneralNotice(row.get("programLegalGeneralNotice"));
                    media.setProgramExternalPerspective1(row.get("programExternalPerspective1"));
                    media.setProgramExternalPerspective2(row.get("programExternalPerspective2"));
                    media.setProgramInternalPerspective1(row.get("programInternalPerspective1"));
                    media.setProgramInternalPerspective2(row.get("programInternalPerspective2"));
                    media.setProgramExternalPerspective1Description(row.get("programExternalPerspective1Description"));
                    media.setProgramExternalPerspective2Description(row.get("programExternalPerspective2Description"));
                    media.setProgramInternalPerspective1Description(row.get("programInternalPerspective1Description"));
                    media.setProgramInternalPerspective2Description(row.get("programInternalPerspective2Description"));
                    media.setProgramGroundPlane(row.get("programGroundPlane"));
                    if(Serenity.sessionVariableCalled("program") != null) {
                        Program oldProgram = Serenity.sessionVariableCalled("program");
                        oldProgram.setMedia(media);
                        Program newProgram = oldProgram;
                        Serenity.setSessionVariable("program").to(newProgram);
                        program = Serenity.sessionVariableCalled("program");
                    }
                    offerProgramDetailSteps.input_data_on_media_tab(media);
                    break;
                default:
                    throw new IllegalArgumentException("The '" + tabName + "' tab is unsupported.");
            }
        }
    }

    @When("the admin clicks {string} button on Program Detail page")
    public void the_admin_clicks_button_on_program_detail_page(String button) {
        switch (button.trim()) {
            case "Save":
                offerProgramDetailSteps.click_save_button();
                break;
            case "Delete":
                offerProgramDetailSteps.click_delete_button();
                break;
            default:
                throw new IllegalArgumentException("The '" + button + "' button is unsupported.");
        }
    }

    @Then("the admin should see information on {string} tab on Program Detail page")
    public void the_admin_should_see_information_on_program_detail_page(String tabName) {
        Program program = Serenity.sessionVariableCalled("program");
        switch (tabName.trim()) {
          case "Required Info":
              offerProgramDetailSteps.see_information_on_required_info_tab(program);
              break;
          case "Localisation":
              offerProgramDetailSteps.see_information_on_localisation_tab(program.getLocalisation());
              break;
          case "ADV":
              offerProgramDetailSteps.see_information_on_adv_tab(program.getAdv());
              break;
          case "Commercial":
              offerProgramDetailSteps.see_information_on_commercial_tab(program.getCommercial());
              break;
          case "Media":
              offerProgramDetailSteps.see_information_on_media_tab(program.getMedia());
              break;
          case "Log":
              offerProgramDetailSteps.see_information_on_log_tab();
              break;
          default:
              throw new IllegalArgumentException("The '" + tabName + "' tab is unsupported.");
      }
    }
    
}