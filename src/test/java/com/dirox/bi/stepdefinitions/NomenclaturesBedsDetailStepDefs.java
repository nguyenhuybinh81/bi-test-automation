package com.dirox.bi.stepdefinitions;

import java.util.List;
import java.util.Map;

import com.dirox.bi.models.Bed;
import com.dirox.bi.serenity.NomenclaturesBedsDetailSteps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

public class NomenclaturesBedsDetailStepDefs {
    @Steps
    private NomenclaturesBedsDetailSteps nomenclaturesBedsDetailSteps;

    @When("the admin inputs information on Beds Detail page following details:")
    public void the_admin_should_see_information_on_beds_list_page_following_details(DataTable table) {
        Bed bed = new Bed();
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        for (Map<String, String> row : rows) {
            bed.setName(row.get("name"));
            bed.setNumberOfBedsForAdults(row.get("numberOfBedsForAdults"));
            bed.setNumberOfBedsForChild(row.get("numberOfBedsForChild"));
            bed.setUploadPicture(row.get("uploadPicture"));
            if(Serenity.sessionVariableCalled("isDeleted") != null) {
                bed.setIsDeleted("IsDeleted: Yes");
            }
            else {
                bed.setIsDeleted("IsDeleted: No");
            }
            Serenity.setSessionVariable("bed").to(bed);
            nomenclaturesBedsDetailSteps.input_data(bed);
        }
    }
    
    @When("the admin clicks {string} button on Beds Detail page")
    public void the_admin_clicks_button_on_beds_detail_page(String button) {
        switch (button.trim()) {
            case "Save":
                nomenclaturesBedsDetailSteps.click_save_button();
                break;
            case "Delete":
                nomenclaturesBedsDetailSteps.click_delete_button();
                break;
            case "Cancel":
                nomenclaturesBedsDetailSteps.click_cancel_button();
                break;
            case "Restore":
                nomenclaturesBedsDetailSteps.click_restore_button();
                break;
            default:
                throw new IllegalArgumentException("The '" + button + "' button is unsupported.");
        }
    }

    @Then("the admin should see information on Beds Detail page")
    public void the_admin_should_see_information_on_beds_detail_page() {
        Bed bed = Serenity.sessionVariableCalled("bed");
        nomenclaturesBedsDetailSteps.should_see_information(bed);
    }
    
    @Then("the admin should see validation message on Beds Detail page following details:")
    public void the_admin_should_see_validation_message_on_beds_detail_page_following_details(DataTable table) {
        Bed bed = new Bed();
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        for (Map<String, String> row : rows) {
            bed.setName(row.get("name"));
            bed.setNumberOfBedsForAdults(row.get("numberOfBedsForAdults"));
            bed.setNumberOfBedsForChild(row.get("numberOfBedsForChild"));
            bed.setUploadPicture(row.get("uploadPicture"));
            nomenclaturesBedsDetailSteps.should_see_validation_message(bed);
        }
    }
    
}