package com.dirox.bi.stepdefinitions;

import com.dirox.bi.serenity.OfferProgramListSteps;

import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class OfferProgramListPageStepDefs {
    @Steps
    private OfferProgramListSteps offerProgramListSteps;

    @When("the admin clicks {string} button on Programs List page")
    public void the_admin_clicks_menus_on_program_list_page(String option) {
        switch (option) {
            case "Add New Item":
                offerProgramListSteps.click_add_program_button();
                break;
            case "Show deleted items":
                // @TBD
                break;
            default:
                throw new IllegalArgumentException("The '" + option + "' menu is unsupported.");
        }
    }

}