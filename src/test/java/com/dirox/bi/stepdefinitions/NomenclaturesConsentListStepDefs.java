package com.dirox.bi.stepdefinitions;

import java.util.List;
import java.util.Map;

import com.dirox.bi.serenity.NomenclaturesConsentListSteps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class NomenclaturesConsentListStepDefs {
    @Steps
    private NomenclaturesConsentListSteps nomenclaturesConsentlListSteps;

    @When("the admin clicks {string} button on Consent List page")
    public void the_admin_clicks_menus_on_consent_list_page(String option) {
        switch (option) {
            case "Add Consent":
                nomenclaturesConsentlListSteps.click_add_consent_button();
                break;
            case "Show deleted items":
                nomenclaturesConsentlListSteps.click_show_deleted_items_checkbox();
                break;
            case "Change display order on front page":
                nomenclaturesConsentlListSteps.click_change_display_order_on_front_page();
                break;
            case "Done":
                nomenclaturesConsentlListSteps.click_done_button();
                break;
            default:
                throw new IllegalArgumentException("The '" + option + "' button is unsupported.");
        }
    }

    @Then("the admin should see information on Consent List page following details:")
    public void the_admin_should_see_information_on_consent_list_page_following_details(DataTable table) {
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        for (Map<String, String> row : rows) {
            switch (row.get("component").trim()) {
                case "change display order button":
                    nomenclaturesConsentlListSteps.is_display_button(row.get("text").trim());
                    break;
                case "show delete items checkbox":
                    nomenclaturesConsentlListSteps.is_display_checkbox(row.get("text").trim());
                    break;
                case "add new button":
                    nomenclaturesConsentlListSteps.is_display_button(row.get("text").trim());
                    break;
                default:
                    throw new IllegalArgumentException("The '" + row.get("component") + "' component is unsupported.");
            }
        }
    }

}