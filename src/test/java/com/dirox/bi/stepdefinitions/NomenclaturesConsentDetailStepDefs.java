package com.dirox.bi.stepdefinitions;

import java.util.List;
import java.util.Map;

import com.dirox.bi.models.Consent;
import com.dirox.bi.serenity.NomenclaturesConsentDetailSteps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

public class NomenclaturesConsentDetailStepDefs {
    @Steps
    private NomenclaturesConsentDetailSteps nomenclaturesConsentDetailSteps;

    @When("the admin inputs information on Consent Detail page following details:")
    public void the_admin_should_see_information_on_consent_list_page_following_details(DataTable table) {
        Consent consent = new Consent();
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        for (Map<String, String> row : rows) {
            consent.setQuestion(row.get("question"));
            if(Serenity.sessionVariableCalled("isDeleted") != null) {
                consent.setIsDeleted("IsDeleted: Yes");
            }
            else {
                consent.setIsDeleted("IsDeleted: No");
            }
            Serenity.setSessionVariable("consent").to(consent);
            nomenclaturesConsentDetailSteps.input_data(consent);
        }
    }
    
    @When("the admin clicks {string} button on Consent Detail page")
    public void the_admin_clicks_button_on_consent_detail_page(String button) {
        switch (button.trim()) {
            case "Save":
                nomenclaturesConsentDetailSteps.click_save_button();
                break;
            case "Delete":
                nomenclaturesConsentDetailSteps.click_delete_button();
                break;
            case "Cancel":
                nomenclaturesConsentDetailSteps.click_cancel_button();
                break;
            case "Restore":
                nomenclaturesConsentDetailSteps.click_restore_button();
                break;
            default:
                throw new IllegalArgumentException("The '" + button + "' button is unsupported.");
        }
    }

    @Then("the admin should see information on Consent Detail page")
    public void the_admin_should_see_information_on_consent_detail_page() {
        Consent consent = Serenity.sessionVariableCalled("consent");
        nomenclaturesConsentDetailSteps.should_see_information(consent);
    }
    
    @Then("the admin should see validation message on Consent Detail page following details:")
    public void the_admin_should_see_validation_message_on_consent_detail_page_following_details(DataTable table) {
        Consent consent = new Consent();
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        for (Map<String, String> row : rows) {
            consent.setQuestion(row.get("question"));
            nomenclaturesConsentDetailSteps.should_see_validation_message(consent);
        }
    }
    
}