package com.dirox.bi.stepdefinitions;

import java.util.List;
import java.util.Map;

import com.dirox.bi.serenity.NomenclaturesBedsListSteps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class NomenclaturesBedsListPageStepDefs {
    @Steps
    private NomenclaturesBedsListSteps nomenclaturesBedsListSteps;

    @When("the admin clicks {string} button on Beds List page")
    public void the_admin_clicks_menus_on_beds_list_page(String option) {
        switch (option) {
            case "Add Bed":
                nomenclaturesBedsListSteps.click_add_beds_button();
                break;
            case "Show deleted items":
                nomenclaturesBedsListSteps.click_show_deleted_items_checkbox();
                break;
            case "Change display order on front page":
                nomenclaturesBedsListSteps.click_change_display_order_on_front_page();
                break;
            case "Done":
                nomenclaturesBedsListSteps.click_done_button();
                break;
            default:
                throw new IllegalArgumentException("The '" + option + "' button is unsupported.");
        }
    }

    @Then("the admin should see information on Beds List page following details:")
    public void the_admin_should_see_information_on_beds_list_page_following_details(DataTable table) {
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        for (Map<String, String> row : rows) {
            switch (row.get("component").trim()) {
                case "change display order button":
                    nomenclaturesBedsListSteps.is_display_button(row.get("text").trim());
                    break;
                case "show delete items checkbox":
                    nomenclaturesBedsListSteps.is_display_checkbox(row.get("text").trim());
                    break;
                case "add new button":
                    nomenclaturesBedsListSteps.is_display_button(row.get("text").trim());
                    break;
                default:
                    throw new IllegalArgumentException("The '" + row.get("component") + "' component is unsupported.");
            }
        }
    }

}