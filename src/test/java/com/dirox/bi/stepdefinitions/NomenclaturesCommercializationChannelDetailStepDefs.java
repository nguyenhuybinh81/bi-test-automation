package com.dirox.bi.stepdefinitions;

import java.util.List;
import java.util.Map;

import com.dirox.bi.models.CommercializationChannel;
import com.dirox.bi.serenity.NomenclaturesCommercializationChannelDetailSteps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

public class NomenclaturesCommercializationChannelDetailStepDefs {
    @Steps
    private NomenclaturesCommercializationChannelDetailSteps nomenclaturesCommercializationChannelDetailSteps;

    @When("the admin inputs information on Commercialization Channel Detail page following details:")
    public void the_admin_should_see_information_on_commercialization_channel_list_page_following_details(DataTable table) {
        CommercializationChannel commercializationChannel = new CommercializationChannel();
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        for (Map<String, String> row : rows) {
            commercializationChannel.setName(row.get("name"));
            if(Serenity.sessionVariableCalled("isDeleted") != null) {
                commercializationChannel.setIsDeleted("IsDeleted: Yes");
            }
            else {
                commercializationChannel.setIsDeleted("IsDeleted: No");
            }
            Serenity.setSessionVariable("commercializationChannel").to(commercializationChannel);
            nomenclaturesCommercializationChannelDetailSteps.input_data(commercializationChannel);
        }
    }
    
    @When("the admin clicks {string} button on Commercialization Channel Detail page")
    public void the_admin_clicks_button_on_commercialization_channel_detail_page(String button) {
        switch (button.trim()) {
            case "Save":
                nomenclaturesCommercializationChannelDetailSteps.click_save_button();
                break;
            case "Delete":
                nomenclaturesCommercializationChannelDetailSteps.click_delete_button();
                break;
            case "Cancel":
                nomenclaturesCommercializationChannelDetailSteps.click_cancel_button();
                break;
            case "Restore":
                nomenclaturesCommercializationChannelDetailSteps.click_restore_button();
                break;
            default:
                throw new IllegalArgumentException("The '" + button + "' button is unsupported.");
        }
    }

    @Then("the admin should see information on Commercialization Channel Detail page")
    public void the_admin_should_see_information_on_commercialization_channel_detail_page() {
        CommercializationChannel commercializationChannel = Serenity.sessionVariableCalled("commercializationChannel");
        nomenclaturesCommercializationChannelDetailSteps.should_see_information(commercializationChannel);
    }
    
    @Then("the admin should see validation message on Commercialization Channel Detail page following details:")
    public void the_admin_should_see_validation_message_on_commercialization_channel_detail_page_following_details(DataTable table) {
        CommercializationChannel commercializationChannel = new CommercializationChannel();
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        for (Map<String, String> row : rows) {
            commercializationChannel.setName(row.get("name"));
            nomenclaturesCommercializationChannelDetailSteps.should_see_validation_message(commercializationChannel);
        }
    }
    
}