package com.dirox.bi.stepdefinitions;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;

import com.dirox.bi.constants.TestConst;
import com.dirox.bi.dao.BedDAO;
import com.dirox.bi.dao.CommercializationChannelDAO;
import com.dirox.bi.dao.ConsentDAO;
import com.dirox.bi.dao.IBaseDAO;
import com.dirox.bi.dao.ProgramDAO;
import com.dirox.bi.models.Bed;
import com.dirox.bi.models.CommercializationChannel;
import com.dirox.bi.models.Consent;
import com.dirox.bi.models.Program;
import com.dirox.bi.models.User;
import com.dirox.bi.serenity.LoginSteps;
import com.dirox.bi.serenity.NomenclaturesBedsDetailSteps;
import com.dirox.bi.serenity.NomenclaturesBedsListSteps;
import com.dirox.bi.serenity.NomenclaturesCommercializationChannelDetailSteps;
import com.dirox.bi.serenity.NomenclaturesCommercializationChannelListSteps;
import com.dirox.bi.serenity.NomenclaturesConsentDetailSteps;
import com.dirox.bi.serenity.NomenclaturesConsentListSteps;
import com.dirox.bi.serenity.OfferProgramDetailSteps;
import com.dirox.bi.serenity.OfferProgramListSteps;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.qos.logback.classic.Logger;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

public class BaseStepDefs {
    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(BaseStepDefs.class);

    @Steps
    private LoginSteps loginSteps;

    @Steps
    private OfferProgramListSteps offerProgramListSteps;

    @Steps
    private OfferProgramDetailSteps offerAddNewProgramSteps;

    @Steps
    private NomenclaturesBedsListSteps nomenclaturesBedsListSteps;

    @Steps
    private NomenclaturesBedsDetailSteps nomenclaturesBedsDetailSteps;

    @Steps
    private NomenclaturesCommercializationChannelListSteps nomenclaturesCommercializationChannelListSteps;

    @Steps
    private NomenclaturesCommercializationChannelDetailSteps nomenclaturesCommercializationChannelDetailSteps;

    @Steps
    private NomenclaturesConsentListSteps nomenclaturesConsentListSteps;

    @Steps
    private NomenclaturesConsentDetailSteps nomenclaturesConsentDetailSteps;

    @Given("the admin launches the web")
    public void the_admin_launches_the_web() {
        loginSteps.opens_login_page();
    }

    @Given("the admin {string} has been already existed in the system")
    public void the_admin_has_been_already_existed_in_the_system(String username) throws JsonParseException, JsonMappingException, IOException {
        // JSON file to Java object
        ObjectMapper mapper = new ObjectMapper();
        String filePath = TestConst.DATA_PATH + "users.json";
        List<User> admins = mapper.readValue(new File(filePath), new TypeReference<List<User>>() {});
        LOGGER.info("User list:" + admins.toString());
        User admin = admins.stream().filter(x -> x.getUsername().equalsIgnoreCase(username)).findAny().get();
        if (admin == null) {
            throw new RuntimeException("The '" + username + "' user is not found. Please, check your test data.");
        }
        Serenity.setSessionVariable("admin").to(admin);
    }

    @Given("the {string} entity has not been already existed any {string} in the system")
    public void the_entity_has_not_been_already_existed_in_the_system(String entity, String dataPattern) {
        int result = 0;
        switch (entity.trim()) {
            case "Program":
                IBaseDAO<Program> iProgramDAO = new ProgramDAO();
                result = iProgramDAO.delete(new Program(dataPattern));
                break;
            case "Product":
                // @TBD
                break;
            case "Bed":
                IBaseDAO<Bed> iBedODAO = new BedDAO();
                result = iBedODAO.delete(new Bed(dataPattern));
                break;
            case "Commercialization Channel":
                IBaseDAO<CommercializationChannel> iCommercializationChannelODAO = new CommercializationChannelDAO();
                result = iCommercializationChannelODAO.delete(new CommercializationChannel(dataPattern));
                break;
            case "Consent":
                IBaseDAO<Consent> iConsentODAO = new ConsentDAO();
                result = iConsentODAO.delete(new Consent(dataPattern));
                break;
            default:
                throw new IllegalArgumentException("The '" + entity + "' entity is unsupported.");
        }
        LOGGER.info("Total record(s) are deleted in '" + entity + "' is " + result);
    }

    @When("the admin goes to {string} page")
    public void the_admin_goes_to_page(String page) {
        String id = null;
        switch (page.trim()) {
            case "Programs List":
                offerProgramListSteps.open_program_list_page();
                break;
            case "Product Family List":
                // @TBD
                break;
            case "Beds List":
                nomenclaturesBedsListSteps.open_beds_list_page();
                break;
            case "Beds Detail":
                id = Serenity.sessionVariableCalled("selectedId");
                nomenclaturesBedsDetailSteps.go_to_detail_page(id);
                break;
            case "Commercialization Channel List":
                nomenclaturesCommercializationChannelListSteps.open_commercialization_channel_list_page();
                break;
            case "Commercialization Channel Detail":
                id = Serenity.sessionVariableCalled("selectedId");
                nomenclaturesCommercializationChannelDetailSteps.go_to_detail_page(id);
                break;
            case "Consent List":
                nomenclaturesConsentListSteps.open_consent_list_page();
                break;
            case "Consent Detail":
                id = Serenity.sessionVariableCalled("selectedId");
                nomenclaturesConsentDetailSteps.go_to_detail_page(id);
                break;
            default:
                throw new IllegalArgumentException("The '" + page + "' page is unsupported.");
        }
    }

    @Then("the admin is on {string} page")
    public void the_admin_is_on_page(String page) {
        switch (page.trim()) {
            case "Programs List":
                offerProgramListSteps.is_on_offer_program_list_page();
                break;
            case "Add New Program Detail":
                offerAddNewProgramSteps.is_on_add_new_offer_program_page();
                break;
            case "Edit Program Detail":
                offerAddNewProgramSteps.is_on_edit_offer_program_page();
                break;
            case "Beds List":
                nomenclaturesBedsListSteps.is_on_beds_list_page();
                break;
            case "Edit Beds Detail":
                nomenclaturesBedsDetailSteps.is_on_edit_beds_page();
                break;
            case "Commercialization Channel List":
                nomenclaturesCommercializationChannelListSteps.is_on_nomenclatures_commercialization_channel_list_page();
                break;
            case "Edit Commercialization Channel Detail":
                nomenclaturesCommercializationChannelDetailSteps.is_on_edit_commercialization_channel_page();
                break;
            case "Consent List":
                nomenclaturesConsentListSteps.is_on_nomenclatures_consent_list_page();
                break;
            case "Edit Consent Detail":
                nomenclaturesConsentDetailSteps.is_on_edit_consent_page();
                break;
            default:
                throw new IllegalArgumentException("The '" + page + "' page is unsupported.");
        }
    }

    @When("the admin clicks Edit button on the record just added on {string} page")
    public void the_admin_clicks_edit_button_on_the_record_just_added_on_page(String page) {
        switch (page.trim()) {
            case "Programs List":
                Program program = Serenity.sessionVariableCalled("program");
                offerProgramListSteps.click_edit_program_button(program.getDenomination());
                break;
            case "Product Family":
                // @TBD
                break;
            case "Beds List":
                Bed bed = Serenity.sessionVariableCalled("bed");
                nomenclaturesBedsListSteps.click_edit_beds_button(bed.getName());
                break;
            case "Commercialization Channel List":
                CommercializationChannel commercializationChannel = Serenity.sessionVariableCalled("commercializationChannel");
                nomenclaturesCommercializationChannelListSteps.click_edit_commercialization_channel_button(commercializationChannel.getName());
                break;
            case "Consent List":
                Consent consent = Serenity.sessionVariableCalled("consent");
                nomenclaturesConsentListSteps.click_edit_consent_button(consent.getQuestion());
                break;
            default:
                throw new IllegalArgumentException("The '" + page + "' page is unsupported.");
        }
    }

    @Then("the admin should not see the record just deleted on {string} page")
    public void the_admin_should_not_see_the_record_just_deleted_on_page(String page) {
        switch (page.trim()) {
            case "Programs List":
                Program program = Serenity.sessionVariableCalled("program");
                offerProgramListSteps.should_not_see_the_record_just_deleted_on_program_list_page(program.getDenomination());
                break;
            case "Product Family List":
                // @TODO
                break;
            case "Beds List":
                Bed bed = Serenity.sessionVariableCalled("bed");
                nomenclaturesBedsListSteps.should_not_see_the_record_just_deleted_on_beds_list_page(bed.getName());
                break;
            case "Commercialization Channel List":
                CommercializationChannel commercializationChannel = Serenity.sessionVariableCalled("commercializationChannel");
                nomenclaturesCommercializationChannelListSteps
                                .should_not_see_the_record_just_deleted_on_commercialization_channel_list_page(commercializationChannel.getName());
                break;
            case "Consent List":
                Consent consent = Serenity.sessionVariableCalled("consent");
                nomenclaturesConsentListSteps.should_not_see_the_record_just_deleted_on_consent_list_page(consent.getQuestion());
                break;
            default:
                throw new IllegalArgumentException("The '" + page + "' page is unsupported.");
        }
    }

    @Then("the admin should see breadscrumb {string} on {string} page")
    public void the_admin_should_see_breadscrumb_on_page(String breadScrumb, String page) {
        switch (page.trim()) {
            case "Programs List":
                // @TODO
                break;
            case "Product Family List":
                // @TODO
                break;
            case "Beds List":
                nomenclaturesBedsListSteps.should_see_bread_scrumb(breadScrumb);
                break;
            case "Commercialization Channel List":
                nomenclaturesCommercializationChannelListSteps.should_see_bread_scrumb(breadScrumb);
                break;
            case "Consent List":
                nomenclaturesConsentListSteps.should_see_bread_scrumb(breadScrumb);
                break;
            default:
                throw new IllegalArgumentException("The '" + page + "' page is unsupported.");
        }
    }

    @When("the admin changes display order on {string} page")
    public void the_admin_changes_display_order_on_page(String page) {
        switch (page.trim()) {
            case "Beds List":
                nomenclaturesBedsListSteps.store_data_before_order();
                Bed bed1 = Serenity.sessionVariableCalled("beforeOrderBed1");
                Bed bed2 = Serenity.sessionVariableCalled("beforeOrderBed2");
                LOGGER.info("Before order: Bed1=" + bed1.getName() + " and Bed2=" + bed2.getName());
                nomenclaturesBedsListSteps.change_order_descending();
                nomenclaturesBedsListSteps.store_data_after_order();
                bed1 = Serenity.sessionVariableCalled("afterOrderBed1");
                bed2 = Serenity.sessionVariableCalled("afterOrderBed2");
                LOGGER.info("After order: Bed1=" + bed1.getName() + " and Bed2=" + bed2.getName());
                break;
            case "Commercialization Channel List":
                nomenclaturesCommercializationChannelListSteps.store_data_before_order();
                CommercializationChannel commercializationChannel1 = Serenity.sessionVariableCalled("beforeOrderCommercializationChannel1");
                CommercializationChannel commercializationChannel2 = Serenity.sessionVariableCalled("beforeOrderCommercializationChannel2");
                LOGGER.info("Before order: CommercializationChannel1=" + commercializationChannel1.getName() + " and CommercializationChannel2="
                                + commercializationChannel2.getName());
                nomenclaturesCommercializationChannelListSteps.change_order_descending();
                nomenclaturesCommercializationChannelListSteps.store_data_after_order();
                commercializationChannel1 = Serenity.sessionVariableCalled("afterOrderCommercializationChannel1");
                commercializationChannel2 = Serenity.sessionVariableCalled("afterOrderCommercializationChannel2");
                LOGGER.info("After order: CommercializationChannel1=" + commercializationChannel1.getName() + " and CommercializationChanne2="
                                + commercializationChannel2.getName());
                break;
            case "Consent List":
                nomenclaturesConsentListSteps.store_data_before_order();
                Consent consent1 = Serenity.sessionVariableCalled("beforeOrderConsent1");
                Consent consent2 = Serenity.sessionVariableCalled("beforeOrderConsent2");
                LOGGER.info("Before order: Consent1=" + consent1.getQuestion() + " and Consent2=" + consent2.getQuestion());
                nomenclaturesConsentListSteps.change_order_descending();
                nomenclaturesConsentListSteps.store_data_after_order();
                consent1 = Serenity.sessionVariableCalled("afterOrderConsent1");
                consent2 = Serenity.sessionVariableCalled("afterOrderConsent2");
                LOGGER.info("After order: Consent1=" + consent1.getQuestion() + " and Consent2=" + consent2.getQuestion());
                break;
            default:
                throw new IllegalArgumentException("The '" + page + "' button is unsupported.");
        }
    }

    @When("the admin should see new display order on {string} page")
    public void the_admin_should_see_new_display_order_on_list_page(String page) {
        switch (page.trim()) {
            case "Programs List":
                // @TODO
                break;
            case "Product Family List":
                // @TODO
                break;
            case "Beds List":
                nomenclaturesBedsListSteps.should_see_order_on_bed_list_page();
                break;
            case "Commercialization Channel List":
                nomenclaturesCommercializationChannelListSteps.should_see_order_on_commercialization_channel_list_page();
                break;
            case "Consent List":
                nomenclaturesConsentListSteps.should_see_order_on_consent_list_page();
                break;
            default:
                throw new IllegalArgumentException("The '" + page + "' page is unsupported.");
        }
    }

    @When("the admin should see table on {string} page following details:")
    public void the_admin_should_see_table_on_Beds_List_page_following_details(String page, DataTable table) {
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        for (Map<String, String> row : rows) {
            switch (page.trim()) {
                case "Programs List":
                    // @TODO
                    break;
                case "Product Family List":
                    // @TODO
                    break;
                case "Beds List":
                    nomenclaturesBedsListSteps.should_see_column_name(Integer.parseInt(row.get("columnIndex")), row.get("columnName"));
                    break;
                case "Commercialization Channel List":
                    nomenclaturesCommercializationChannelListSteps.should_see_column_name(Integer.parseInt(row.get("columnIndex")), row.get("columnName"));
                    break;
                case "Consent List":
                    nomenclaturesConsentListSteps.should_see_column_name(Integer.parseInt(row.get("columnIndex")), row.get("columnName"));
                    break;
                default:
                    throw new IllegalArgumentException("The '" + page + "' page is unsupported.");
            }
        }
    }

    @Then("the admin should see pagination on {string} page")
    public void the_admin_should_see_pagination_on_page(String page) {
        switch (page.trim()) {
            case "Programs List":
                // @TODO
                break;
            case "Product Family List":
                // @TODO
                break;
            case "Beds List":
                IBaseDAO<Bed> iBedODAO = new BedDAO();
                List<Bed> beds = iBedODAO.getAll();
                if (beds.size() > 10) {
                    nomenclaturesBedsListSteps.is_display_pagination();
                }
                break;
            case "Commercialization Channel List":
                IBaseDAO<CommercializationChannel> iCommercializationChannelODAO = new CommercializationChannelDAO();
                List<CommercializationChannel> commercializationChannel = iCommercializationChannelODAO.getAll();
                if (commercializationChannel.size() > 10) {
                    nomenclaturesCommercializationChannelListSteps.is_display_pagination();
                }
                break;
            case "Consent List":
                IBaseDAO<Consent> iConsentDAO = new ConsentDAO();
                List<Consent> consents = iConsentDAO.getAll();
                if (consents.size() > 10) {
                    nomenclaturesBedsListSteps.is_display_pagination();
                }
                break;
            default:
                throw new IllegalArgumentException("The '" + page + "' page is unsupported.");
        }
    }

}
