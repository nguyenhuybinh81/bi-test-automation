package com.dirox.bi.stepdefinitions;

import com.dirox.bi.serenity.ConfirmationSteps;

import io.cucumber.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class ConfirmationStepDefs {
    @Steps
    private ConfirmationSteps confirmationSteps;

    @Given("the admin clicks {string} on Confirmation popup")
    public void the_admin_login_to_the_system_successfully(String button) {
        switch (button.trim()) {
            case "Delete anyway":
                confirmationSteps.click_delete_anyway_button_on_popup();
                break;
            case "Cancel":
                confirmationSteps.click_cancel_button_on_popup();
                break;
            default:
                throw new IllegalArgumentException("The '" + button + "' button is unsupported.");
        }
    }
    
    @Given("the admin should see {string} message on Confirmation popup")
    public void the_admin_should_see_message_on_confirmation_popup(String message) {
        confirmationSteps.see_confirmation_message_on_popup(message);
    }
}
