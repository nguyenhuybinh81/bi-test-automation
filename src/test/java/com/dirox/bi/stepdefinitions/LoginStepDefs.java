package com.dirox.bi.stepdefinitions;

import com.dirox.bi.models.User;
import com.dirox.bi.serenity.LoginSteps;

import io.cucumber.java.en.Given;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

public class LoginStepDefs {
    @Steps
    private LoginSteps loginSteps;

    @Given("the admin login to the system successfully")
    public void the_admin_login_to_the_system_successfully() {
        User admin = Serenity.sessionVariableCalled("admin");
        loginSteps.login_to_the_sytem_successfully(admin);
    }
}
