package com.dirox.bi.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.dirox.bi.utils.ElementHelper;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("/admin/offer/programs")
public class OfferProgramListPage extends PageObject {
    private WebDriver driver; 
    @FindBy(xpath = "//span[contains(@class, 'breadcrumb')]//a[text()='Programs Management']")
    private WebElementFacade lblPageHeader;
    
    @FindBy(xpath = "TBD")
    private WebElementFacade chkShowDeletedItems;
    
    
    @FindBy(xpath= "//button[contains(., 'Add Program')]")
    private WebElementFacade btnAddProgram;
    
    public OfferProgramListPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public boolean isCurrentPageAt() {
        lblPageHeader.waitUntilVisible();
        return lblPageHeader.isDisplayed();
    }

    public OfferProgramDetailPage addProgram() {
        btnAddProgram.waitUntilVisible();
        btnAddProgram.click();
        return new OfferProgramDetailPage(driver);
    }
    
    public OfferProgramDetailPage editProgram(String denomination) {
        By by = By.xpath("//tbody//div[contains(., '" + denomination + "')]/../following-sibling::td//i[@title='Edit']");
        WebElement selectedItem = ElementHelper.findElement(driver, by, getWaitForTimeout().getSeconds());
        selectedItem.click();
        return new OfferProgramDetailPage(driver);
    }
    
    public boolean isDisplayedProgram(String denomination) {
        By by = By.xpath("//tbody//div[contains(., '" + denomination + "')]/../following-sibling::td//i[@title='Edit']");
        try {
            return ElementHelper.findElement(driver, by, getWaitForTimeout().getSeconds()).isDisplayed();
        }
        catch(WebDriverException e) {
            return false;
        }
    }
    
}
