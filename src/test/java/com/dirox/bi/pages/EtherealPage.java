package com.dirox.bi.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class EtherealPage extends PageObject {
    private WebDriver driver; 
    
    @FindBy(xpath = "//a[contains(., 'Ethereal')]")
    private WebElementFacade lblPageHeader;

    @FindBy(name = "address")
    private WebElementFacade txtEmail;
    
    @FindBy(name = "password")
    private WebElementFacade txtPassword;
    
    @FindBy(xpath = "//button[contains(., 'Log in')]")
    private WebElementFacade btnSubmit;
    
    @FindBy(xpath = "//li/a[text()='Messages']")
    private WebElementFacade lnkMessage;
    
    @FindBy(xpath = "(//a[text()='Your link to Majorelle’s Admin portal'])[1]")
    private WebElementFacade lnkEmailSubject;
    
    @FindBy(xpath = "//body/a[text()='Web link']")
    private WebElementFacade lnkWeb;
    
    public EtherealPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }
    
    public boolean isCurrentPageAt() {
        lblPageHeader.waitUntilVisible();
        return lblPageHeader.isDisplayed();
    }

    public EtherealPage inputAccount(String email, String password) {
        txtEmail.waitUntilVisible();
        txtPassword.waitUntilVisible();
        txtEmail.sendKeys(email);
        txtPassword.sendKeys(password);
        btnSubmit.waitUntilVisible();
        btnSubmit.click();
        return this;
    }
    
    public EtherealPage clickMessage() {
        lnkMessage.waitUntilVisible();
        lnkMessage.click();
        return this;
    }
    
    public EtherealPage clickEmailSubject() {
        lnkEmailSubject.waitUntilVisible();
        lnkEmailSubject.click();
        return this;
    }
    
    public EtherealPage clickWebLink() {
        driver.switchTo().frame(0);
        lnkWeb.waitUntilVisible();
        lnkWeb.click();
        return this;
    }
    
    public AdminDashBoardPage openAuthentication(String email, String password) {
        inputAccount(email, password);
        clickMessage();
        clickEmailSubject();
        clickWebLink();
        return new AdminDashBoardPage(driver);
    }
    
}
