package com.dirox.bi.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.WebElementFacade;

public class NomenclaturesCommercializationChannelDetailPage extends BaseDetailPage {
    @FindBy(xpath = "//div[contains(@class, 'header')]//b[text()='Add new commercialization channel']")
    private WebElementFacade lblPageHeader;

    @FindBy(xpath = "//label[text()='Name']/following-sibling::div//input")
    private WebElementFacade txtName;

    @FindBy(xpath = "//label[text()='Name']/following-sibling::div//div[contains(@class, 'error')]")
    private WebElementFacade lblErrorName;

    // Edit mode
    @FindBy(xpath = "//h3[contains(@class, 'title')]//div[text()='Edit Commercialization Channel']")
    private WebElementFacade lblPageHeader2;

    @FindBy(xpath = "(//h3[contains(@class, 'title')])[2]")
    private WebElementFacade lblId;

    public NomenclaturesCommercializationChannelDetailPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }
    
    public void openDetail(String id) {
        driver.navigate().to(getBOUrl() + "/admin/nomenclatures/commercialization-channel/" + id);
    }

    public boolean isAtAddNewPage() {
        lblPageHeader.waitUntilVisible();
        return lblPageHeader.isDisplayed();
    }

    public boolean isAtEditPage() {
        lblPageHeader2.waitUntilVisible();
        return lblPageHeader2.isDisplayed();
    }

    public String getErrorNameMessage() {
        lblErrorName.waitUntilVisible();
        return lblErrorName.getText().trim();
    }

    public NomenclaturesCommercializationChannelDetailPage inputName(String text) {
        txtName.waitUntilVisible();
        txtName.clear();
        if (text != null) {
            txtName.sendKeys(text);
        }
        txtName.sendKeys(Keys.TAB);
        return this;
    }

    public NomenclaturesCommercializationChannelDetailPage clickSave() {
        btnSave.waitUntilVisible();
        btnSave.click();
        return this;
    }

    public NomenclaturesCommercializationChannelDetailPage clickDelete() {
        btnDelete.waitUntilVisible();
        btnDelete.click();
        return this;
    }

    public NomenclaturesCommercializationChannelDetailPage clickCancel() {
        btnCancel.waitUntilVisible();
        btnCancel.click();
        return this;
    }

    public NomenclaturesCommercializationChannelDetailPage clickRestore() {
        btnRestore.waitUntilVisible();
        btnRestore.click();
        return this;
    }

    // Edit mode
    
    public boolean isDisplayId() {
        lblId.waitUntilVisible();
        return lblId.isDisplayed();
    }
    
    public String getId() {
        lblId.waitUntilVisible();
        return lblId.getText().trim();
    }

    public String getName() {
        txtName.waitUntilVisible();
        return txtName.getAttribute("value").trim();
    }
}
