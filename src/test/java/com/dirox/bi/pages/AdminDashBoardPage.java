package com.dirox.bi.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class AdminDashBoardPage extends PageObject {
    @FindBy(xpath = "//span[text()='Admin Dashboard']")
    private WebElementFacade lblPageHeader;
    

    public AdminDashBoardPage(WebDriver driver) {
        super(driver);
    }
    
    public boolean isCurrentPageAt() {
        lblPageHeader.waitUntilVisible();
        return lblPageHeader.isDisplayed();
    }

    
}
