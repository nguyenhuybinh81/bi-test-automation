package com.dirox.bi.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.WebElementFacade;

public class NomenclaturesConsentDetailPage extends BaseDetailPage {
    @FindBy(xpath = "//div[contains(@class, 'header')]//b[text()='Add new consent']")
    private WebElementFacade lblPageHeader;

    @FindBy(xpath = "//label[text()='Question']/following-sibling::div//textarea")
    private WebElementFacade txtQuestion;

    @FindBy(xpath = "//label[text()='Question']/following-sibling::div//div[contains(@class, 'error')]")
    private WebElementFacade lblErrorQuestion;

    // Edit mode
    @FindBy(xpath = "//h3[contains(@class, 'title')]//div[text()='Edit Consent']")
    private WebElementFacade lblPageHeader2;

    @FindBy(xpath = "(//h3[contains(@class, 'title')])[2]")
    private WebElementFacade lblId;

    public NomenclaturesConsentDetailPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }
    
    public void openDetail(String id) {
        driver.navigate().to(getBOUrl() + "/admin/nomenclatures/consent/" + id);
    }
    
    public boolean isAtAddNewPage() {
        lblPageHeader.waitUntilVisible();
        return lblPageHeader.isDisplayed();
    }

    public boolean isAtEditPage() {
        lblPageHeader2.waitUntilVisible();
        return lblPageHeader2.isDisplayed();
    }

    public String getErrorNameMessage() {
        lblErrorQuestion.waitUntilVisible();
        return lblErrorQuestion.getText().trim();
    }

    public NomenclaturesConsentDetailPage inputQuestion(String text) {
        txtQuestion.waitUntilVisible();
        txtQuestion.clear();
        if (text != null) {
            txtQuestion.sendKeys(text);
        }
        txtQuestion.sendKeys(Keys.TAB);
        return this;
    }

    public NomenclaturesConsentDetailPage clickSave() {
        btnSave.waitUntilVisible();
        btnSave.click();
        return this;
    }

    public NomenclaturesConsentDetailPage clickDelete() {
        btnDelete.waitUntilVisible();
        btnDelete.click();
        return this;
    }

    public NomenclaturesConsentDetailPage clickCancel() {
        btnCancel.waitUntilVisible();
        btnCancel.click();
        return this;
    }

    public NomenclaturesConsentDetailPage clickRestore() {
        btnRestore.waitUntilVisible();
        btnRestore.click();
        return this;
    }

    // Edit mode
    
    public boolean isDisplayId() {
        lblId.waitUntilVisible();
        return lblId.isDisplayed();
    }
    
    public String getId() {
        lblId.waitUntilVisible();
        return lblId.getText().trim();
    }

    public String getName() {
        txtQuestion.waitUntilVisible();
        return txtQuestion.getAttribute("value").trim();
    }
}
