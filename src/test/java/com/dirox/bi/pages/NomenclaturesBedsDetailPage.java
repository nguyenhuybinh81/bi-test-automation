package com.dirox.bi.pages;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import com.dirox.bi.constants.TestConst;
import com.dirox.bi.utils.ElementHelper;

import net.serenitybdd.core.pages.WebElementFacade;

public class NomenclaturesBedsDetailPage extends BaseDetailPage {
    @FindBy(xpath = "//div[contains(@class, 'header')]//b[text()='Add new bed']")
    private WebElementFacade lblPageHeader;

    @FindBy(xpath = "//label[text()='Name']/following-sibling::div//input")
    private WebElementFacade txtName;

    @FindBy(xpath = "//label[text()='Number of beds for adults']/following-sibling::div//input")
    private WebElementFacade txtNumberOfBedsForAdults;

    @FindBy(xpath = "//label[text()='Number of beds for adults']/following-sibling::div//i[contains(@class, 'arrow-up')]")
    private WebElementFacade btnArrowUpForAdults;

    @FindBy(xpath = "//label[text()='Number of beds for adults']/following-sibling::div//i[contains(@class, 'arrow-down')]")
    private WebElementFacade btnArrowDownForAdults;

    @FindBy(xpath = "//label[text()='Number of beds for child']/following-sibling::div//input")
    private WebElementFacade txtNumberOfBedsForChild;

    @FindBy(xpath = "//label[text()='Number of beds for child']/following-sibling::div//i[contains(@class, 'arrow-up')]")
    private WebElementFacade btnArrowUpForChild;

    @FindBy(xpath = "//label[text()='Number of beds for child']/following-sibling::div//i[contains(@class, 'arrow-down')]")
    private WebElementFacade btnArrowDownForChild;

    @FindBy(xpath = "//label[text()='Name']/following-sibling::div//div[contains(@class, 'error')]")
    private WebElementFacade lblErrorName;
    
    @FindBy(xpath = "//label[text()='Number of beds for adults']/following-sibling::div//div[contains(@class, 'error')]")
    private WebElementFacade lblErrornumberOfBedsForAdults;
    
    @FindBy(xpath = "//label[text()='Number of beds for child']/following-sibling::div//div[contains(@class, 'error')]")
    private WebElementFacade lblErrornumberOfBedsForChild;
    

    // Edit mode
    @FindBy(xpath = "//h3[contains(@class, 'title')]//div[text()='Edit bed']")
    private WebElementFacade lblPageHeader2;

    @FindBy(xpath = "//h3[contains(@class, 'title')]/div/div[2]")
    private WebElementFacade lblId;

    @FindBy(xpath = "//div[contains(@class, 'upload')]/img")
    private WebElementFacade imgUpload;

    public NomenclaturesBedsDetailPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public void openDetail(String id) {
        driver.navigate().to(getBOUrl() + "/admin/nomenclatures/beds/" + id);
    }
    
    public boolean isAtAddNewPage() {
        lblPageHeader.waitUntilVisible();
        return lblPageHeader.isDisplayed();
    }

    public boolean isAtEditPage() {
        lblPageHeader2.waitUntilVisible();
        return lblPageHeader2.isDisplayed();
    }

    public String getErrorNameMessage() {
        lblErrorName.waitUntilVisible();
        return lblErrorName.getText().trim();
    }
    
    public String getErrorNumberOfBedsForAdultsMessage() {
        lblErrornumberOfBedsForAdults.waitUntilVisible();
        return lblErrornumberOfBedsForAdults.getText().trim();
    }
    
    public String getErrorNumberOfBedsForChildMessage() {
        lblErrornumberOfBedsForChild.waitUntilVisible();
        return lblErrornumberOfBedsForChild.getText().trim();
    }

    public NomenclaturesBedsDetailPage inputName(String text) {
        txtName.waitUntilVisible();
        txtName.clear();
        if (text != null) {
            txtName.sendKeys(text);
        }
        txtName.sendKeys(Keys.TAB);
        return this;
    }

    public NomenclaturesBedsDetailPage inputNumberOfBedsForAdults(String text) {
        txtNumberOfBedsForAdults.waitUntilVisible();
        txtNumberOfBedsForAdults.clear();
        if (text != null) {
            txtNumberOfBedsForAdults.sendKeys(text);
        }
        txtNumberOfBedsForAdults.sendKeys(Keys.TAB);
        return this;
    }

    public NomenclaturesBedsDetailPage inputNumberOfBedsForChild(String text) {
        txtNumberOfBedsForChild.waitUntilVisible();
        txtNumberOfBedsForChild.clear();
        if (text != null) {
            txtNumberOfBedsForChild.sendKeys(text);
        }
        txtNumberOfBedsForChild.sendKeys(Keys.TAB);
        return this;
    }

    public NomenclaturesBedsDetailPage uploadImage(String filename) {
        if (filename != null) {
            String s = TestConst.IMAGES_PATH + filename;
            // String s = "images/bed/bed-01.jpg";
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource(s).getFile());
            ElementHelper.uploadFile(driver, By.xpath("//div[contains(@class, 'upload')]/input[@type='file']"), file.toString(),
                            getWaitForTimeout().getSeconds());
        }
        return this;
    }

    public NomenclaturesBedsDetailPage clickSave() {
        btnSave.waitUntilVisible();
        btnSave.click();
        return this;
    }

    public NomenclaturesBedsDetailPage clickDelete() {
        btnDelete.waitUntilVisible();
        btnDelete.waitUntilClickable();
        btnDelete.click();
        return this;
    }

    public NomenclaturesBedsDetailPage clickCancel() {
        btnCancel.waitUntilVisible();
        btnCancel.click();
        return this;
    }

    public NomenclaturesBedsDetailPage clickRestore() {
        btnRestore.waitUntilVisible();
        btnRestore.waitUntilClickable();
        btnRestore.click();
        return this;
    }

    // Edit mode
    
    public boolean isDisplayId() {
        lblId.waitUntilVisible();
        return lblId.isDisplayed();
    }
    
    public String getId() {
        lblId.waitUntilVisible();
        return lblId.getText().trim();
    }

    public String getName() {
        txtName.waitUntilVisible();
        return txtName.getAttribute("value").trim();
    }

    public String getNumberOfBedsForAdults() {
        txtNumberOfBedsForAdults.waitUntilVisible();
        return txtNumberOfBedsForAdults.getAttribute("value").trim();
    }

    public String getNumberOfBedsForChild() {
        txtNumberOfBedsForChild.waitUntilVisible();
        return txtNumberOfBedsForChild.getAttribute("value").trim();
    }

    public String getUploadedImage() {
        txtNumberOfBedsForChild.waitUntilVisible();
        return txtNumberOfBedsForChild.getAttribute("src").trim();
    }
}
