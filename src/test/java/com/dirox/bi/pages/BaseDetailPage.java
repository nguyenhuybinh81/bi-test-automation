package com.dirox.bi.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class BaseDetailPage extends PageObject {
    protected WebDriver driver;

    @FindBy(xpath = "//button/span[contains(., 'Save')]")
    protected WebElementFacade btnSave;
    
    @FindBy(xpath = "//button/span[contains(., 'Cancel')]")
    protected WebElementFacade btnCancel;
    
    @FindBy(xpath = "//button/span[contains(., 'Delete')]")
    protected WebElementFacade btnDelete;
    
    @FindBy(xpath = "//button/span[contains(., 'Restore')]")
    protected WebElementFacade btnRestore;
    
    @FindBy(xpath = "//div[contains(@class, 'log-info')]/div[1]")
    protected WebElementFacade lblLastModificationDate;
    
    @FindBy(xpath = "//div[contains(@class, 'log-info')]/div[2]")
    protected WebElementFacade lblIdAdminModified;
    
    @FindBy(xpath = "//div[contains(@class, 'log-info')]/div[3]")
    protected WebElementFacade lblCreationDate;
    
    @FindBy(xpath = "//div[contains(@class, 'log-info')]/div[4]")
    protected WebElementFacade lblIdAdminCreated;
    
    @FindBy(xpath = "//div[contains(@class, 'log-info')]/div[5]")
    protected WebElementFacade lblIsDeleted;

    public BaseDetailPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }
    
    public String getBOUrl() {
      EnvironmentVariables environmentVariables = SystemEnvironmentVariables.createEnvironmentVariables();
      String url = EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("webdriver.base.url");
      return url.trim();
    }

    public String getLastModificationDate() {
        lblLastModificationDate.waitUntilVisible();
        return lblLastModificationDate.getText().trim();
    }
    
    public String getIdAdminModified() {
        lblIdAdminModified.waitUntilVisible();
        return lblIdAdminModified.getText().trim();
    }
    
    public String getCreationDate() {
        lblCreationDate.waitUntilVisible();
        return lblCreationDate.getText().trim();
    }
    
    public String getIdAdminCreated() {
        lblIdAdminCreated.waitUntilVisible();
        return lblIdAdminCreated.getText().trim();
    }
    
    public String getIsDeleted() {
        lblIsDeleted.waitUntilVisible();
        return lblIsDeleted.getText().trim();
    }

}
