package com.dirox.bi.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class ConfirmationPopup extends PageObject {
    protected WebDriver driver; 
    @FindBy(xpath = "//div[contains(@class, 'message-box')]/div[contains(@class, 'header')]//span")
    private WebElementFacade lblPageHeader;

    @FindBy(xpath = "//div[contains(@class, 'message-box')]/p")
    private WebElementFacade lblMessage;

    @FindBy(xpath = "//div[contains(@class, 'message-box')]/button[contains(., 'Delete anyway')]")
    private WebElementFacade btnDeleteAnyway;
    
    @FindBy(xpath = "//div[contains(@class, 'message-box')]/button[contains(., 'Cancel')]")
    private WebElementFacade btnCancel;
    
    @FindBy(xpath = "//div[@role='alert']//div[contains(@class, 'content')]/p")
    private WebElementFacade lblConfirmationMessage;
    
    @FindBy(xpath = "//div[contains(@class, 'message-box')]/p")
    private WebElementFacade lblSuccessMessage;

    public ConfirmationPopup(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public boolean isCurrentPageAt() {
        lblPageHeader.waitUntilVisible();
        return lblPageHeader.isDisplayed();
    }

    public String getMessage() {
        lblMessage.waitUntilVisible();
        return lblMessage.getText().trim();
    }

    public void clickDeleteAnyway() {
        btnDeleteAnyway.waitUntilVisible();
        btnDeleteAnyway.click();
    }
    
    public void clickCancel() {
        btnCancel.waitUntilVisible();
        btnCancel.click();
    }
    
    public String getSuccessMessage() {
        lblConfirmationMessage.waitUntilVisible();
        return lblConfirmationMessage.getText().trim();
    }

}
