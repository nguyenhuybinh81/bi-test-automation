package com.dirox.bi.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import com.dirox.bi.constants.TestConst;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("/admin/login")
public class LoginPage extends PageObject {
    private WebDriver driver; 
    
    @FindBy(xpath = "//h2[contains(text(),'Majorelle’s Admin portal')]")
    private WebElementFacade lblPageHeader;

    @FindBy(name = "email")
    private WebElementFacade txtEmail;
    
    @FindBy(xpath = "//button[@type='submit']")
    private WebElementFacade btnSubmit;
    
    @FindBy(xpath = "//h1[text()='Perfect!']")
    private WebElementFacade lblStatus;
    
    public LoginPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }
    
    public EtherealPage goToEthereal() {
        getDriver().get(TestConst.SMTP_HOST);
        return new EtherealPage(driver);
    }
    
    public boolean isCurrentPageAt() {
        lblPageHeader.waitUntilVisible();
        return lblPageHeader.isDisplayed();
    }

    public LoginPage inputEmail(String email) {
        txtEmail.waitUntilVisible();
        txtEmail.sendKeys(email);
        return this;
    }
    
    public LoginPage clickSubmit() {
        btnSubmit.waitUntilVisible();
        btnSubmit.click();
        return this;
    }
    
    public boolean isPerfect() {
        lblStatus.waitUntilVisible();
        return lblStatus.isDisplayed();
    }
    
}
