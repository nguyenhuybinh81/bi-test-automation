package com.dirox.bi.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("/admin/nomenclatures/consent")
public class NomenclaturesConsentlListPage extends BaseListPage {
    @FindBy(xpath = "//div/h3[text()='Consent']")
    private WebElementFacade lblPageHeader;

    @FindBy(xpath = "(//table/tbody/tr/td[2])[1]")
    private WebElementFacade lblDisplayOrder1;
    
    @FindBy(xpath = "(//table/tbody/tr/td[2])[2]")
    private WebElementFacade lblDisplayOrder2;

    @FindBy(xpath = "(//table/tbody/tr/td[3])[1]")
    private WebElementFacade lblName1;
    
    @FindBy(xpath = "(//table/tbody/tr/td[3])[2]")
    private WebElementFacade lblName2;

    private static final String ADD_NEW_BUTTON = "Add Consent";
    private static final int DESCENDING_BUTTON_INDEX = 1;

    public NomenclaturesConsentlListPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public boolean isCurrentPageAt() {
        lblPageHeader.waitUntilVisible();
        return lblPageHeader.isDisplayed();
    }

    public NomenclaturesConsentlListPage changeOrderOnFrontPage() {
        WebElement element = getButton(CHANGE_DISPLAY_ORDER_ON_FRONT_END);
        element.click();
        return this;
    }

    public NomenclaturesConsentlListPage changeOrderDesc() {
        WebElement element = getDescButton(DESCENDING_BUTTON_INDEX);
        element.click();
        return this;
    }

    public String getDisplayOrder1() {
        lblDisplayOrder1.waitUntilVisible();
        return lblDisplayOrder1.getText().trim();
    }
    
    public String getDisplayOrder2() {
        lblDisplayOrder2.waitUntilVisible();
        return lblDisplayOrder2.getText().trim();
    }

    public String getName1() {
        lblName1.waitUntilVisible();
        return lblName1.getText().trim();
    }
    
    public String getName2() {
        lblName2.waitUntilVisible();
        return lblName2.getText().trim();
    }

    public NomenclaturesConsentlListPage clickDone() {
        WebElement element = getButton(DONE);
        element.click();
        return this;
    }

    public NomenclaturesConsentDetailPage clickAddNew() {
        WebElement element = getButton(ADD_NEW_BUTTON);
        element.click();
        return new NomenclaturesConsentDetailPage(driver);
    }
    
    public NomenclaturesConsentlListPage clickShowDeletedItems() {
        WebElement element = getCheckbox(SHOW_DELETED_ITEMS);
        element.click();
        return this;
    }

    public NomenclaturesConsentDetailPage clickEdit(String bed) {
        WebElement selectedItem = getEditButton(bed);
        selectedItem.click();
        return new NomenclaturesConsentDetailPage(driver);
    }
}
