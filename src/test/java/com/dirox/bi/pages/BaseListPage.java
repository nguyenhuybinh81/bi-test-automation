package com.dirox.bi.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import com.dirox.bi.utils.ElementHelper;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class BaseListPage extends PageObject {
    protected WebDriver driver;

    @FindBy(xpath = "//div[@aria-label='Breadcrumb']")
    private WebElementFacade lblBreadScrumb;

    @FindBy(xpath = "//div[@class='pagination-container']//span[contains(@class, 'total')]")
    private WebElementFacade lblTotalRecord;

    @FindBy(xpath = "//i[contains(@class, 'arrow-left')]/../preceding-sibling::span[1]//i[contains(@class, 'arrow-up')]")
    private WebElementFacade btnNarrowup;

    @FindBy(xpath = "//i[contains(@class, 'arrow-left')]")
    private WebElementFacade btnNarrowLeft;

    @FindBy(xpath = "//ul[contains(@class, 'pager')]/li")
    private WebElementFacade lblCurrentPage;

    @FindBy(xpath = "//i[contains(@class, 'arrow-right')]")
    private WebElementFacade btnNarrowRight;

    @FindBy(xpath = "//span[contains(@class, 'jump') and contains(., 'Go to')]")
    private WebElementFacade lblGoto;

    @FindBy(xpath = "//span[contains(@class, 'jump')]//input[@min='1']")
    private WebElementFacade txtJumpToPage;
    
    protected static final String CHANGE_DISPLAY_ORDER_ON_FRONT_END = "Change display order on front page";
    protected static final String DONE = "Done";
    protected static final String SHOW_DELETED_ITEMS = "Show deleted items";

    public BaseListPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public String getBreadScrumb() {
        lblBreadScrumb.waitUntilVisible();
        return lblBreadScrumb.getText().trim();
    }

    public WebElement getDescButton(int index) {
        By by = By.xpath("(//table/tbody/tr/td[" + index + "])[1]//span/i[contains(@class, 'descending')]");
        return ElementHelper.findElement(driver, by, getWaitForTimeout().getSeconds());
    }

    public String getColumnName(int index) {
        try {
            By by = By.xpath("(//table/thead/tr/th/div)[" + index + "]");
            WebElement element = ElementHelper.findElement(driver, by, 3);
            return element.getText().trim();
        } catch (Exception e) {
            return null;
        }
    }

    public boolean isExistedRecordPerPage(String text) {
        By by = By.xpath("//ul[contains(@class, 'select-dropdown')]/li/span[text()='" + text + "']");
        WebElement element = ElementHelper.findElement(driver, by, getWaitForTimeout().getSeconds());
        return element.isDisplayed();
    }

    public String getTotalRecord() {
        lblTotalRecord.waitUntilVisible();
        return lblTotalRecord.getText().trim();
    }

    public boolean isExistedNarrowLeft() {
        btnNarrowLeft.waitUntilVisible();
        return btnNarrowLeft.isDisplayed();
    }

    public String getCurrentPage() {
        lblCurrentPage.waitUntilVisible();
        return lblCurrentPage.getText().trim();
    }

    public boolean isExistedNarrowRight() {
        btnNarrowRight.waitUntilVisible();
        return btnNarrowRight.isDisplayed();
    }

    public boolean isExistedGoto() {
        lblGoto.waitUntilVisible();
        return lblGoto.isDisplayed();
    }

    public boolean isExistedJumpToPage() {
        txtJumpToPage.waitUntilVisible();
        return txtJumpToPage.isDisplayed();
    }

    public BaseListPage openDopdownSelectRecordPerPage() {
        btnNarrowup.waitUntilVisible();
        btnNarrowup.click();
        return this;
    }

    public boolean isDisplayButton(String text) {
        By by = By.xpath("//button/span[contains(., '" + text + "')]");
        try {
            return ElementHelper.findElement(driver, by, getWaitForTimeout().getSeconds()).isDisplayed();
        } catch (WebDriverException e) {
            return false;
        }
    }

    public boolean isDisplayCheckboxCaption(String text) {
        By by = By.xpath("//label/span[contains(., '" + text + "')]");
        try {
            return ElementHelper.findElement(driver, by, getWaitForTimeout().getSeconds()).isDisplayed();
        } catch (WebDriverException e) {
            return false;
        }
    }

    public boolean isDisplayCheckbox(String text) {
        By by = By.xpath("//label/span[contains(., '" + text + "')]/preceding-sibling::span");
        try {
            return ElementHelper.findElement(driver, by, getWaitForTimeout().getSeconds()).isDisplayed();
        } catch (WebDriverException e) {
            return false;
        }
    }

    public boolean isDisplayedItem(String text) {
        By by = By.xpath("//tbody//div[contains(., '" + text + "')]/../following-sibling::td//i[@title='Edit']");
        try {
            return ElementHelper.findElement(driver, by, getWaitForTimeout().getSeconds()).isDisplayed();
        } catch (WebDriverException e) {
            return false;
        }
    }

    public WebElement getButton(String text) {
        By by = By.xpath("//button/span[contains(., '" + text + "')]");
        return ElementHelper.findElement(driver, by, getWaitForTimeout().getSeconds());
    }

    public WebElement getCheckbox(String text) {
        By by = By.xpath("//label/span[contains(., '" + text + "')]/preceding-sibling::span");
        return ElementHelper.findElement(driver, by, getWaitForTimeout().getSeconds());
    }

    public WebElement getEditButton(String text) {
        By by = By.xpath("//tbody//div[contains(., '" + text + "')]/../following-sibling::td//i[@title='Edit']");
        return ElementHelper.findElement(driver, by, getWaitForTimeout().getSeconds());
    }

}
