package com.dirox.bi.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.dirox.bi.utils.ElementHelper;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class OfferProgramDetailPage extends PageObject {
    protected WebDriver driver; 
    @FindBy(xpath = "//div[contains(@class, 'header')]//b[text()='Add a new program']")
    protected WebElementFacade lblPageHeader;
    
    @FindBy(xpath = "//label[text()='Denomination']/following-sibling::div//input")
    protected WebElementFacade txtDenomination;
    
    @FindBy(xpath = "//label[text()='DeliveryDate']/following-sibling::div//input")
    protected WebElementFacade txtDeliveryDate;
    
    @FindBy(xpath = "//label[text()='Address']/following-sibling::div//input")
    protected WebElementFacade txtAddress;
    
    @FindBy(xpath = "//label[text()='City']/following-sibling::div//input")
    protected WebElementFacade cboCity;
    
    @FindBy(xpath = "//label[text()='MainCityAround']/following-sibling::div//input")
    protected WebElementFacade txtMainCityAround;
    
    @FindBy(xpath = "//label[text()='MainCityAroundPostalCode']/following-sibling::div//input")
    protected WebElementFacade txtMainCityAroundPostalCode;
    
    @FindBy(xpath = "//label[text()='Country']/following-sibling::div//input")
    protected WebElementFacade cboCountry;
    
    @FindBy(xpath = "//label[text()='AccountingUnit']/following-sibling::div//input")
    protected WebElementFacade txtAccountingUnit;
    
    @FindBy(xpath = "//label[text()='AccountingCenter']/following-sibling::div//input")
    protected WebElementFacade txtAccountingCenter;
    
    @FindBy(xpath = "//label[text()='CleOperationCommerciale']/following-sibling::div//input")
    protected WebElementFacade txtCleOperationCommerciale;
    
    @FindBy(id = "tab-Localisation")
    protected WebElementFacade tabLocalisation;
    
    @FindBy(id = "tab-ADV")
    protected WebElementFacade tabAdv;
    
    @FindBy(id = "tab-Commercial")
    protected WebElementFacade tabCommercial;
    
    @FindBy(id = "tab-Media")
    protected WebElementFacade tabMedia;
    
    @FindBy(id = "tab-Log")
    protected WebElementFacade tabLog;
    
    protected OfferProgramDetailLocalisationTab offerAddNewProgramLocalisationTab;
    protected OfferProgramDetailAdvTab offerAddNewProgramAdvTab;
    protected OfferProgramDetailCommercialTab offerAddNewProgramCommercialTab;
    protected OfferProgramDetailMediaTab offerAddNewProgramMediaTab;
    protected OfferProgramDetailLogTab offerAddNewProgramLogTab;
    
    @FindBy(xpath = "//div[contains(@class, 'group-btn')]//button/span[contains(., 'Save')]")
    protected WebElementFacade btnSave;
    
    @FindBy(xpath = "//div[contains(@class, 'group-btn')]//button/span[contains(., 'Delete')]")
    protected WebElementFacade btnDelete;
    
    //Edit mode
    @FindBy(xpath = "//div/h3[text()='Edit Program']")
    private WebElementFacade lblPageHeader2;
    
    @FindBy(xpath = "//label[text()='IdProgram']/following-sibling::div//span")
    protected WebElementFacade lblIdProgram;
    
    public OfferProgramDetailPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public boolean isAtAddNewPage() {
        lblPageHeader.waitUntilVisible();
        return lblPageHeader.isDisplayed();
    }
    
    public boolean isAtEditPage() {
        lblPageHeader2.waitUntilVisible();
        return lblPageHeader2.isDisplayed();
    }
    
    public OfferProgramDetailLocalisationTab openOfferAddNewProgramLocalisationTab() {
        tabLocalisation.waitUntilVisible();
        tabLocalisation.click();
        return offerAddNewProgramLocalisationTab;
    }
    
    public OfferProgramDetailAdvTab openOfferAddNewProgramAdvTab() {
        tabAdv.waitUntilVisible();
        tabAdv.click();
        return offerAddNewProgramAdvTab;
    }
    
    public OfferProgramDetailCommercialTab openOfferAddNewProgramCommercialTab() {
        tabCommercial.waitUntilVisible();
        tabCommercial.click();
        return offerAddNewProgramCommercialTab;
    }
    
    public OfferProgramDetailMediaTab openOfferAddNewProgramMediaTab() {
        tabMedia.waitUntilVisible();
        tabMedia.click();
        return offerAddNewProgramMediaTab;
    }
    
    public OfferProgramDetailLogTab openOfferAddNewProgramLogTab() {
        tabLog.waitUntilVisible();
        tabLog.click();
        return offerAddNewProgramLogTab;
    }
    
    public OfferProgramDetailPage inputDenomination(String text) {
        txtDenomination.waitUntilVisible();
        txtDenomination.clear();
        txtDenomination.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailPage inputDeliveryDate(String text) {
        txtDeliveryDate.waitUntilVisible();
        txtDeliveryDate.clear();
        txtDeliveryDate.sendKeys(text);
        txtDeliveryDate.sendKeys(Keys.TAB);
        return this;
        
    }
    
    public OfferProgramDetailPage inputAddress(String text) {
        txtAddress.waitUntilVisible();
        txtAddress.clear();
        txtAddress.sendKeys(text);
        return this;
        
    }
    
    public OfferProgramDetailPage selectCity(String text) {
        cboCity.waitUntilVisible();
        cboCity.click();
        waitABit(1000);
        By by = By.xpath("//ul[contains(@class, 'dropdown')]/li/span[text()='" + text + "']");
        WebElement selectedItem = ElementHelper.findElement(driver, by, getWaitForTimeout().getSeconds());
        selectedItem.click();
        return this;
    }
    
    public OfferProgramDetailPage inputMainCityAround(String text) {
        txtMainCityAround.waitUntilVisible();
        txtMainCityAround.clear();
        txtMainCityAround.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailPage inputMainCityAroundPostalCode(String text) {
        txtMainCityAroundPostalCode.waitUntilVisible();
        txtMainCityAroundPostalCode.clear();
        txtMainCityAroundPostalCode.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailPage selectCountry(String text) {
        cboCountry.waitUntilVisible();
        cboCountry.click();
        waitABit(1000);
        By by = By.xpath("//ul[contains(@class, 'dropdown')]/li/span[text()='" + text + "']");
        WebElement selectedItem = ElementHelper.findElement(driver, by, getWaitForTimeout().getSeconds());
        selectedItem.click();
        return this;
    }
    
    public OfferProgramDetailPage inputAccountingUnit(String text) {
        txtAccountingUnit.waitUntilVisible();
        txtAccountingUnit.clear();
        txtAccountingUnit.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailPage inputAccountingCenter(String text) {
        txtAccountingCenter.waitUntilVisible();
        txtAccountingCenter.clear();
        txtAccountingCenter.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailPage inputCleOperationCommerciale(String text) {
        txtCleOperationCommerciale.waitUntilVisible();
        txtCleOperationCommerciale.clear();
        txtCleOperationCommerciale.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailPage clickSave() {
        btnSave.waitUntilVisible();
        btnSave.click();
        return this;
    }
    
    public OfferProgramDetailPage clickDelete() {
        btnDelete.waitUntilVisible();
        btnDelete.click();
        return this;
    }
    
    public String getDenomination() {
        txtDenomination.waitUntilVisible();
        return txtDenomination.getAttribute("value").trim();
    }
    
    public String getDeliveryDate() {
        txtDeliveryDate.waitUntilVisible();
        return txtDeliveryDate.getAttribute("value").trim();
    }
    
    public String getAddress() {
        txtAddress.waitUntilVisible();
        return txtAddress.getAttribute("value").trim();
    }
    
    public String getCity() {
        cboCity.waitUntilVisible();
        return cboCity.getAttribute("value").trim();
    }
    
    public String getMainCityAround() {
        txtMainCityAround.waitUntilVisible();
        return txtMainCityAround.getAttribute("value").trim();
    }
    
    public String getMainCityAroundPostalCode() {
        txtMainCityAroundPostalCode.waitUntilVisible();
        return txtMainCityAroundPostalCode.getAttribute("value").trim();
    }
    
    public String getCountry() {
        cboCountry.waitUntilVisible();
        return cboCountry.getAttribute("value").trim();
    }
    
    public String getAccountingUnit() {
        txtAccountingUnit.waitUntilVisible();
        return txtAccountingUnit.getAttribute("value").trim();
    }
    
    public String getAccountingCenter() {
        txtAccountingCenter.waitUntilVisible();
        return txtAccountingCenter.getAttribute("value").trim();
    }
    
    public String getCleOperationCommerciale() {
        txtCleOperationCommerciale.waitUntilVisible();
        return txtCleOperationCommerciale.getAttribute("value").trim();
    }
    
    public String getIdProgram() {
        lblIdProgram.waitUntilVisible();
        return lblIdProgram.getText().trim();
    }
    
}
