package com.dirox.bi.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class OfferProgramDetailLogTab extends PageObject {
    @FindBy(xpath = "//div[contains(@class,'title-box')]/b[text()='Log']")
    private WebElementFacade lblTabHeader;
    
    @FindBy(xpath = "//div[contains(@class, 'log-info')]/div[1]")
    private WebElementFacade lblLastModificationDate;
    
    @FindBy(xpath = "//div[contains(@class, 'log-info')]/div[2]")
    private WebElementFacade lblIdAdminModified;
    
    @FindBy(xpath = "//div[contains(@class, 'log-info')]/div[3]")
    private WebElementFacade lblCreationDate;
    
    @FindBy(xpath = "//div[contains(@class, 'log-info')]/div[4]")
    private WebElementFacade lblIdAdminCreated;
    
    @FindBy(xpath = "//div[contains(@class, 'log-info')]/div[5]")
    private WebElementFacade lblIsDeleted;
    
    public OfferProgramDetailLogTab(WebDriver driver) {
        super(driver);
    }

    public boolean isCurrentPageAt() {
        lblTabHeader.waitUntilVisible();
        return lblTabHeader.isDisplayed();
    }

    public String getLastModificationDate() {
        lblLastModificationDate.waitUntilVisible();
        return lblLastModificationDate.getText().trim();
    }
    
    public String getIdAdminModified() {
        lblIdAdminModified.waitUntilVisible();
        return lblIdAdminModified.getText().trim();
    }
    
    public String getCreationDate() {
        lblCreationDate.waitUntilVisible();
        return lblCreationDate.getText().trim();
    }
    
    public String getIdAdminCreated() {
        lblIdAdminCreated.waitUntilVisible();
        return lblIdAdminCreated.getText().trim();
    }
    
    public String getIsDeleted() {
        lblIsDeleted.waitUntilVisible();
        return lblIsDeleted.getText().trim();
    }
    
}
