package com.dirox.bi.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class OfferProgramDetailLocalisationTab extends PageObject {
    @FindBy(xpath = "//div[@class='title-box']/b[text()='Localisation']")
    private WebElementFacade lblTabHeader;
    
    @FindBy(xpath = "//label[text()='ComplementAddress1']/following-sibling::div//input")
    private WebElementFacade txtComplementAddress1;
    
    @FindBy(xpath = "//label[text()='ComplementAddress2']/following-sibling::div//input")
    private WebElementFacade txtComplementAddress2;
    
    @FindBy(xpath = "//label[text()='ComplementAddress3']/following-sibling::div//input")
    private WebElementFacade txtComplementAddress3;
    
    @FindBy(xpath = "//label[text()='PostalCode']/following-sibling::div//input")
    private WebElementFacade txtPostalCode;
    
    @FindBy(xpath = "//label[text()='Longitude']/following-sibling::div//input")
    private WebElementFacade txtLongitude;
    
    @FindBy(xpath = "//label[text()='Latitude']/following-sibling::div//input")
    private WebElementFacade txtLatitude;
    
    public OfferProgramDetailLocalisationTab(WebDriver driver) {
        super(driver);
    }

    public boolean isCurrentPageAt() {
        lblTabHeader.waitUntilVisible();
        return lblTabHeader.isDisplayed();
    }

    public OfferProgramDetailLocalisationTab inputComplementAddress1(String text) {
        txtComplementAddress1.waitUntilVisible();
        txtComplementAddress1.clear();
        txtComplementAddress1.sendKeys(text);
        return this;
        
    }
    
    public OfferProgramDetailLocalisationTab inputComplementAddress2(String text) {
        txtComplementAddress2.waitUntilVisible();
        txtComplementAddress2.clear();
        txtComplementAddress2.sendKeys(text);
        return this;
        
    }
    
    public OfferProgramDetailLocalisationTab inputComplementAddress3(String text) {
        txtComplementAddress3.waitUntilVisible();
        txtComplementAddress3.clear();
        txtComplementAddress3.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailLocalisationTab inputPostalCode(String text) {
        txtPostalCode.waitUntilVisible();
        txtPostalCode.clear();
        txtPostalCode.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailLocalisationTab inputLongitude(String text) {
        txtLongitude.waitUntilVisible();
        txtLongitude.clear();
        txtLongitude.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailLocalisationTab inputLatitude(String text) {
        txtLatitude.waitUntilVisible();
        txtLatitude.clear();
        txtLatitude.sendKeys(text);
        return this;
    }
    
    public String getComplementAddress1() {
        txtComplementAddress1.waitUntilVisible();
        return txtComplementAddress1.getAttribute("value").trim();
    }
    
    public String getComplementAddress2() {
        txtComplementAddress2.waitUntilVisible();
        return txtComplementAddress2.getAttribute("value").trim();
        
    }
    
    public String getComplementAddress3() {
        txtComplementAddress3.waitUntilVisible();
        return txtComplementAddress3.getAttribute("value").trim();
    }
    
    public String getPostalCode() {
        txtPostalCode.waitUntilVisible();
        return txtPostalCode.getAttribute("value").trim();
    }
    
    public String getLongitude() {
        txtLongitude.waitUntilVisible();
        return txtLongitude.getAttribute("value").trim();
    }
    
    public String getLatitude() {
        txtLatitude.waitUntilVisible();
        return txtLatitude.getAttribute("value").trim();
    }
    
}
