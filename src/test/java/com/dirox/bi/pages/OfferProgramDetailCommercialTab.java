package com.dirox.bi.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import com.dirox.bi.utils.ElementHelper;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class OfferProgramDetailCommercialTab extends PageObject {
    private WebDriver driver; 
    @FindBy(xpath = "//div[@class='title-box']/b[text()='Commercial']")
    private WebElementFacade lblTabHeader;
    
    @FindBy(xpath = "//label[text()='CommercialProgramPhoneNumber']/following-sibling::div//input")
    private WebElementFacade txtCommercialProgramPhoneNumber;
    
    @FindBy(xpath = "//label[text()='CommercialOpeningDate']/following-sibling::div//input")
    protected WebElementFacade txtCommercialOpeningDate;
    
    @FindBy(xpath = "//label[text()='TeasingDateStarts']/following-sibling::div//input")
    protected WebElementFacade txtTeasingDateStarts;
    
    @FindBy(xpath = "//label[text()='TeasingDateEnds']/following-sibling::div//input")
    protected WebElementFacade txtTeasingDateEnds;
    
    @FindBy(xpath = "//label[text()='PrivateSellingCode']/following-sibling::div//input")
    private WebElementFacade txtPrivateSellingCode;
    
    @FindBy(xpath = "//label[text()='ProgramCommercialPunchLine']/following-sibling::div//input")
    private WebElementFacade txtProgramCommercialPunchLine;
    
    @FindBy(xpath = "//label[text()='ProgramCommercialArgumentShortText']/following-sibling::div//input")
    private WebElementFacade txtProgramCommercialArgumentShortText;
    
    @FindBy(xpath = "//label[text()='ProgramCommercialArgumentLongText']/following-sibling::div//input")
    private WebElementFacade txtProgramCommercialArgumentLongText;
    
    @FindBy(xpath = "//html/body")
    private WebElementFacade txtProgramCommercialArgumentLongHTML;
    
    @FindBy(xpath = "//label[text()='Orbital3DModel']/following-sibling::div//input")
    private WebElementFacade txtOrbital3DModel;
    
    public OfferProgramDetailCommercialTab(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public boolean isCurrentPageAt() {
        lblTabHeader.waitUntilVisible();
        return lblTabHeader.isDisplayed();
    }

    public OfferProgramDetailCommercialTab inputCommercialProgramPhoneNumber(String text) {
        txtCommercialProgramPhoneNumber.waitUntilVisible();
        txtCommercialProgramPhoneNumber.clear();
        txtCommercialProgramPhoneNumber.sendKeys(text);
        return this;
        
    }
    
    public OfferProgramDetailCommercialTab inputCommercialOpeningDate(String text) {
        txtCommercialOpeningDate.waitUntilVisible();
        txtCommercialOpeningDate.clear();
        txtCommercialOpeningDate.sendKeys(text);
        txtCommercialOpeningDate.sendKeys(Keys.TAB);
        return this;
        
    }
    
    public OfferProgramDetailCommercialTab inputTeasingDateStarts(String text) {
        txtTeasingDateStarts.waitUntilVisible();
        txtTeasingDateStarts.clear();
        txtTeasingDateStarts.sendKeys(text);
        txtTeasingDateStarts.sendKeys(Keys.TAB);
        return this;
    }
    
    public OfferProgramDetailCommercialTab inputTeasingDateEnds(String text) {
        txtTeasingDateEnds.waitUntilVisible();
        txtTeasingDateEnds.clear();
        txtTeasingDateEnds.sendKeys(text);
        txtTeasingDateEnds.sendKeys(Keys.TAB);
        return this;
    }
    
    public OfferProgramDetailCommercialTab inputPrivateSellingCode(String text) {
        txtPrivateSellingCode.waitUntilVisible();
        txtPrivateSellingCode.clear();
        txtPrivateSellingCode.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailCommercialTab inputProgramCommercialPunchLine(String text) {
        txtProgramCommercialPunchLine.waitUntilVisible();
        txtProgramCommercialPunchLine.clear();
        txtProgramCommercialPunchLine.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailCommercialTab inputProgramCommercialArgumentShortText(String text) {
        txtProgramCommercialArgumentShortText.waitUntilVisible();
        txtProgramCommercialArgumentShortText.clear();
        txtProgramCommercialArgumentShortText.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailCommercialTab inputProgramCommercialArgumentLongText(String text) {
        txtProgramCommercialArgumentLongText.waitUntilVisible();
        txtProgramCommercialArgumentLongText.clear();
        txtProgramCommercialArgumentLongText.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailCommercialTab inputProgramCommercialArgumentLongHTML(String text) {
        driver.switchTo().frame(0);
        txtProgramCommercialArgumentLongHTML.waitUntilVisible();
        txtProgramCommercialArgumentLongHTML.clear();
        txtProgramCommercialArgumentLongHTML.sendKeys(text);
        driver.switchTo().defaultContent();
        return this;
    }
    
    public OfferProgramDetailCommercialTab inputOrbital3DModel(String text) {
        txtOrbital3DModel.waitUntilVisible();
        txtOrbital3DModel.clear();
        txtOrbital3DModel.sendKeys(text);
        return this;
    }
    
    public String getCommercialProgramPhoneNumber() {
        txtCommercialProgramPhoneNumber.waitUntilVisible();
        return txtCommercialProgramPhoneNumber.getAttribute("value").trim();
    }
    
    public String getCommercialOpeningDate() {
        txtCommercialOpeningDate.waitUntilVisible();
        return txtCommercialOpeningDate.getAttribute("value").trim();
    }
    
    public String getTeasingDateStarts() {
        txtTeasingDateStarts.waitUntilVisible();
        return txtTeasingDateStarts.getAttribute("value").trim();
    }
    
    public String getTeasingDateEnds() {
        txtTeasingDateEnds.waitUntilVisible();
        return txtTeasingDateEnds.getAttribute("value").trim();
    }
    
    public String getPrivateSellingCode() {
        txtPrivateSellingCode.waitUntilVisible();
        return txtPrivateSellingCode.getAttribute("value").trim();
    }
    
    public String getProgramCommercialPunchLine() {
        txtProgramCommercialPunchLine.waitUntilVisible();
        return txtProgramCommercialPunchLine.getAttribute("value").trim();
    }
    
    public String getProgramCommercialArgumentShortText() {
        txtProgramCommercialArgumentShortText.waitUntilVisible();
        return txtProgramCommercialArgumentShortText.getAttribute("value").trim();
    }
    
    public String getProgramCommercialArgumentLongText() {
        txtProgramCommercialArgumentLongText.waitUntilVisible();
        return txtProgramCommercialArgumentLongText.getAttribute("value").trim();
    }
    
    public Object scrollTo(String text) {
        return ElementHelper.scrollToText(driver, text, getWaitForTimeout().getSeconds());
    }
    
    public String getProgramCommercialArgumentLongHTML() {
        //waitABit(2000);
        driver.switchTo().frame(0);
        //waitABit(1000);
        //txtProgramCommercialArgumentLongHTML.waitUntilVisible();
        String text = txtProgramCommercialArgumentLongHTML.getText().trim();
        driver.switchTo().defaultContent();
        return text;
    }
    
    public String getOrbital3DModel() {
        txtOrbital3DModel.waitUntilVisible();
        return txtOrbital3DModel.getAttribute("value").trim();
    }
    
}
