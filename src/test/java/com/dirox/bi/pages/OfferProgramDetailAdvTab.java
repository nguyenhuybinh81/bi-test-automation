package com.dirox.bi.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class OfferProgramDetailAdvTab extends PageObject {
    @FindBy(xpath = "//div[@class='title-box']/b[text()='ADV']")
    private WebElementFacade lblTabHeader;
    
    @FindBy(xpath = "//label[text()='IBANNumber']/following-sibling::div//input")
    private WebElementFacade txtIBANNumber;
    
    @FindBy(xpath = "//label[text()='ProgramNotarySocialName']/following-sibling::div//input")
    private WebElementFacade txtProgramNotarySocialName;
    
    @FindBy(xpath = "//label[text()='ProgramNotaryContactName']/following-sibling::div//input")
    private WebElementFacade txtProgramNotaryContactName;
    
    @FindBy(xpath = "//label[text()='ProgramNotaryPhoneNumber']/following-sibling::div//input")
    private WebElementFacade txtProgramNotaryPhoneNumber;
    
    @FindBy(xpath = "//label[text()='NotaryAddress']/following-sibling::div//input")
    private WebElementFacade txtNotaryAddress;
    
    
    public OfferProgramDetailAdvTab(WebDriver driver) {
        super(driver);
    }

    public boolean isCurrentPageAt() {
        lblTabHeader.waitUntilVisible();
        return lblTabHeader.isDisplayed();
    }

    public OfferProgramDetailAdvTab inputIBANNumber(String text) {
        txtIBANNumber.waitUntilVisible();
        txtIBANNumber.clear();
        txtIBANNumber.sendKeys(text);
        return this;
        
    }
    
    public OfferProgramDetailAdvTab inputProgramNotarySocialName(String text) {
        txtProgramNotarySocialName.waitUntilVisible();
        txtProgramNotarySocialName.clear();
        txtProgramNotarySocialName.sendKeys(text);
        return this;
        
    }
    
    public OfferProgramDetailAdvTab inputProgramNotaryContactName(String text) {
        txtProgramNotaryContactName.waitUntilVisible();
        txtProgramNotaryContactName.clear();
        txtProgramNotaryContactName.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailAdvTab inputProgramNotaryPhoneNumber(String text) {
        txtProgramNotaryPhoneNumber.waitUntilVisible();
        txtProgramNotaryPhoneNumber.clear();
        txtProgramNotaryPhoneNumber.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailAdvTab inputNotaryAddress(String text) {
        txtNotaryAddress.waitUntilVisible();
        txtNotaryAddress.clear();
        txtNotaryAddress.sendKeys(text);
        return this;
    }
    
    public String getIBANNumber() {
        txtIBANNumber.waitUntilVisible();
        return txtIBANNumber.getAttribute("value").trim();
    }
    
    public String getProgramNotarySocialName() {
        txtProgramNotarySocialName.waitUntilVisible();
        return txtProgramNotarySocialName.getAttribute("value").trim();
    }
    
    public String getProgramNotaryContactName() {
        txtProgramNotaryContactName.waitUntilVisible();
        return txtProgramNotaryContactName.getAttribute("value").trim();
    }
    
    public String getProgramNotaryPhoneNumber() {
        txtProgramNotaryPhoneNumber.waitUntilVisible();
        return txtProgramNotaryPhoneNumber.getAttribute("value").trim();
    }
    
    public String getNotaryAddress() {
        txtNotaryAddress.waitUntilVisible();
        return txtNotaryAddress.getAttribute("value").trim();
    }
    
}
