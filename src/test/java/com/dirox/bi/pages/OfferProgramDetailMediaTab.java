package com.dirox.bi.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class OfferProgramDetailMediaTab extends PageObject {
    @FindBy(xpath = "//div[@class='title-box']/b[text()='Media']")
    private WebElementFacade lblTabHeader;
    
    @FindBy(xpath = "//label[text()='Immersive3DVisitURL']/following-sibling::div//input")
    private WebElementFacade txtImmersive3DVisitURL;
    
    @FindBy(xpath = "//label[text()='ProgramSuburbVisit']/following-sibling::div//input")
    private WebElementFacade txtProgramSuburbVisit;
    
    @FindBy(xpath = "//label[text()='ProgramVideoURL']/following-sibling::div//input")
    private WebElementFacade txtProgramVideoURL;
    
    @FindBy(xpath = "//label[text()='ProgramLegalGeneralNotice']/following-sibling::div//input")
    private WebElementFacade txtProgramLegalGeneralNotice;
    
    @FindBy(xpath = "//label[text()='ProgramExternalPerspective1']/following-sibling::div//input")
    private WebElementFacade txtProgramExternalPerspective1;
    
    @FindBy(xpath = "//label[text()='ProgramExternalPerspective2']/following-sibling::div//input")
    private WebElementFacade txtProgramExternalPerspective2;
    
    @FindBy(xpath = "//label[text()='ProgramInternalPerspective1']/following-sibling::div//input")
    private WebElementFacade txtProgramInternalPerspective1;
    
    @FindBy(xpath = "//label[text()='ProgramInternalPerspective2']/following-sibling::div//input")
    private WebElementFacade txtProgramInternalPerspective2;
    
    @FindBy(xpath = "//label[text()='ProgramExternalPerspective1Description']/following-sibling::div//input")
    private WebElementFacade txtProgramExternalPerspective1Description;
    
    @FindBy(xpath = "//label[text()='ProgramExternalPerspective2Description']/following-sibling::div//input")
    private WebElementFacade txtProgramExternalPerspective2Description;
    
    @FindBy(xpath = "//label[text()='ProgramInternalPerspective1Description']/following-sibling::div//input")
    private WebElementFacade txtProgramInternalPerspective1Description;
    
    @FindBy(xpath = "//label[text()='ProgramInternalPerspective2Description']/following-sibling::div//input")
    private WebElementFacade txtProgramInternalPerspective2Description;
    
    @FindBy(xpath = "//label[text()='ProgramGroundPlane']/following-sibling::div//input")
    private WebElementFacade txtProgramGroundPlane;
    
    public OfferProgramDetailMediaTab(WebDriver driver) {
        super(driver);
    }

    public boolean isCurrentPageAt() {
        lblTabHeader.waitUntilVisible();
        return lblTabHeader.isDisplayed();
    }

    public OfferProgramDetailMediaTab inputImmersive3DVisitURL(String text) {
        txtImmersive3DVisitURL.waitUntilVisible();
        txtImmersive3DVisitURL.clear();
        txtImmersive3DVisitURL.sendKeys(text);
        return this;
        
    }
    
    public OfferProgramDetailMediaTab inputProgramSuburbVisit(String text) {
        txtProgramSuburbVisit.waitUntilVisible();
        txtProgramSuburbVisit.clear();
        txtProgramSuburbVisit.sendKeys(text);
        return this;
        
    }
    
    public OfferProgramDetailMediaTab inputProgramVideoURL(String text) {
        txtProgramVideoURL.waitUntilVisible();
        txtProgramVideoURL.clear();
        txtProgramVideoURL.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailMediaTab inputProgramLegalGeneralNotice(String text) {
        txtProgramLegalGeneralNotice.waitUntilVisible();
        txtProgramLegalGeneralNotice.clear();
        txtProgramLegalGeneralNotice.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailMediaTab inputProgramExternalPerspective1(String text) {
        txtProgramExternalPerspective1.waitUntilVisible();
        txtProgramExternalPerspective1.clear();
        txtProgramExternalPerspective1.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailMediaTab inputProgramInternalPerspective1(String text) {
        txtProgramInternalPerspective1.waitUntilVisible();
        txtProgramInternalPerspective1.clear();
        txtProgramInternalPerspective1.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailMediaTab inputProgramInternalPerspective2(String text) {
        txtProgramInternalPerspective2.waitUntilVisible();
        txtProgramInternalPerspective2.clear();
        txtProgramInternalPerspective2.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailMediaTab inputProgramExternalPerspective1Description(String text) {
        txtProgramExternalPerspective1Description.waitUntilVisible();
        txtProgramExternalPerspective1Description.clear();
        txtProgramExternalPerspective1Description.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailMediaTab inputProgramExternalPerspective2(String text) {
        txtProgramExternalPerspective2.waitUntilVisible();
        txtProgramExternalPerspective2.clear();
        txtProgramExternalPerspective2.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailMediaTab inputProgramExternalPerspective2Description(String text) {
        txtProgramExternalPerspective2Description.waitUntilVisible();
        txtProgramExternalPerspective2Description.clear();
        txtProgramExternalPerspective2Description.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailMediaTab inputProgramInternalPerspective1Description(String text) {
        txtProgramInternalPerspective1Description.waitUntilVisible();
        txtProgramInternalPerspective1Description.clear();
        txtProgramInternalPerspective1Description.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailMediaTab inputProgramInternalPerspective2Description(String text) {
        txtProgramInternalPerspective2Description.waitUntilVisible();
        txtProgramInternalPerspective2Description.clear();
        txtProgramInternalPerspective2Description.sendKeys(text);
        return this;
    }
    
    public OfferProgramDetailMediaTab inputProgramGroundPlane(String text) {
        txtProgramGroundPlane.waitUntilVisible();
        txtProgramGroundPlane.clear();
        txtProgramGroundPlane.sendKeys(text);
        return this;
    }
    
    public String getImmersive3DVisitURL() {
        txtImmersive3DVisitURL.waitUntilVisible();
        return txtImmersive3DVisitURL.getAttribute("value").trim();
    }
    
    public String getProgramSuburbVisit() {
        txtProgramSuburbVisit.waitUntilVisible();
        return txtProgramSuburbVisit.getAttribute("value").trim();
    }
    
    public String getProgramVideoURL() {
        txtProgramVideoURL.waitUntilVisible();
        return txtProgramVideoURL.getAttribute("value").trim();
    }
    
    public String getProgramLegalGeneralNotice() {
        txtProgramLegalGeneralNotice.waitUntilVisible();
        return txtProgramLegalGeneralNotice.getAttribute("value").trim();
    }
    
    public String getProgramExternalPerspective1() {
        txtProgramExternalPerspective1.waitUntilVisible();
        return txtProgramExternalPerspective1.getAttribute("value").trim();
    }
    
    public String getProgramExternalPerspective2() {
        txtProgramExternalPerspective2.waitUntilVisible();
        return txtProgramExternalPerspective2.getAttribute("value").trim();
    }
    
    public String getProgramInternalPerspective1() {
        txtProgramInternalPerspective1.waitUntilVisible();
        return txtProgramInternalPerspective1.getAttribute("value").trim();
    }
    
    public String getProgramInternalPerspective2() {
        txtProgramInternalPerspective2.waitUntilVisible();
        return txtProgramInternalPerspective2.getAttribute("value").trim();
    }
    
    public String getProgramExternalPerspective1Description() {
        txtProgramExternalPerspective1Description.waitUntilVisible();
        return txtProgramExternalPerspective1Description.getAttribute("value").trim();
    }
    
    public String getProgramExternalPerspective2Description() {
        txtProgramExternalPerspective2Description.waitUntilVisible();
        return txtProgramExternalPerspective2Description.getAttribute("value").trim();
    }
    
    public String getProgramInternalPerspective1Description() {
        txtProgramInternalPerspective1Description.waitUntilVisible();
        return txtProgramInternalPerspective1Description.getAttribute("value").trim();
    }
    
    public String getProgramInternalPerspective2Description() {
        txtProgramInternalPerspective2Description.waitUntilVisible();
        return txtProgramInternalPerspective2Description.getAttribute("value").trim();
    }
    
    public String getProgramGroundPlane() {
        txtProgramGroundPlane.waitUntilVisible();
        return txtProgramGroundPlane.getAttribute("value").trim();
    }
    
}
