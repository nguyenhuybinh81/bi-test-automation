package com.dirox.bi.pages;

import org.openqa.selenium.WebDriver;

public class ConfirmationPopupFactory {

    public static ConfirmationPopup getConfirmationPopup(WebDriver driver) {
        return new ConfirmationPopup(driver);
    }
}