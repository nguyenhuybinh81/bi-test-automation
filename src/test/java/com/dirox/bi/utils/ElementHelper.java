package com.dirox.bi.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

public class ElementHelper {
    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(ElementHelper.class);

    public static WebElement findElement(WebDriver driver, By by, long timeOutInSeconds) {
        LOGGER.info("Finding element '" + by.toString() + "' in " + timeOutInSeconds + " second(s)");
        WebElement element = driver.findElement(by);
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        return wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static Object scrollToText(WebDriver driver, String text, long timeOutInSeconds) {
        LOGGER.info("Scrolling to text '" + text + "' in " + timeOutInSeconds + " second(s)");
        WebElement element = findElement(driver, By.xpath("//*[contains(text(), '" + text + "')]"), timeOutInSeconds);
        return ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public static void uploadFile(WebDriver driver, By by, String filePath, long timeOutInSeconds) {
        LOGGER.info("Uploading the file '" + filePath + "' in " + timeOutInSeconds + " second(s)");
        WebElement upload = driver.findElement(by);
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
        upload.sendKeys(filePath);
    }

}
