package com.dirox.bi.utils;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.slf4j.LoggerFactory;

import com.dirox.bi.dao.ProgramDAO;

import ch.qos.logback.classic.Logger;

public class HibernateUtil {
    private static final Logger logger = (Logger) LoggerFactory.getLogger(ProgramDAO.class);
    
    public static SessionFactory buildSessionFactory(String cfgFile) {
        try {
            StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure(cfgFile).build();
            Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
            return meta.getSessionFactoryBuilder().build();
        } catch (Exception e) {
            logger.error("Initial SessionFactory creation failed.", e);
            throw new ExceptionInInitializerError(e);
        }
    }
}