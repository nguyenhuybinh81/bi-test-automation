package com.dirox.bi.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringHelper {

    public static String extractId(String text) {
        Pattern p = Pattern.compile("ID:(.+)");
        Matcher m = p.matcher(text);
        while (m.find()) {
            return m.group(1).trim();
        }
        return "";
    }
    
}
