package com.dirox.bi.utils;

import org.hibernate.SessionFactory;

public class HibernateUtilFactory {
    
    
    public static SessionFactory getHibernateSessionFactory(String environment) {
        switch (environment) {
            case "dev":
                return HibernateUtil.buildSessionFactory("hibernate.cfg-dev.xml");
            case "staging":
                return HibernateUtil.buildSessionFactory("hibernate.cfg-staging.xml");
            default:
                throw new IllegalArgumentException("The '" + environment + "' environment is unsupported.");
        }
    }
}

