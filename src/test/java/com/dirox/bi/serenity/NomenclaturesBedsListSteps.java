package com.dirox.bi.serenity;

import static org.assertj.core.api.Assertions.assertThat;

import com.dirox.bi.models.Bed;
import com.dirox.bi.pages.ConfirmationPopupFactory;
import com.dirox.bi.pages.NomenclaturesBedsDetailPage;
import com.dirox.bi.pages.NomenclaturesBedsListPage;
import com.dirox.bi.utils.StringHelper;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class NomenclaturesBedsListSteps extends ScenarioSteps {
    private static final long serialVersionUID = 1L;
    private NomenclaturesBedsListPage nomenclaturesBedsListPage;

    @Step
    public void open_beds_list_page() {
        nomenclaturesBedsListPage.open();
    }

    @Step
    public void is_on_beds_list_page() {
        assertThat(nomenclaturesBedsListPage.isCurrentPageAt()).isTrue();
    }

    @Step
    public void click_done_button() {
        nomenclaturesBedsListPage.clickDone();
    }
    
    @Step
    public void store_data_before_order() {
        //String order = nomenclaturesBedsListPage.getDisplayOrder1();
        String name = nomenclaturesBedsListPage.getName1();
        //Bed bed1 = new Bed(Integer.parseInt(order), name);
        Bed bed1 = new Bed(name);
        //order = nomenclaturesBedsListPage.getDisplayOrder2();
        name = nomenclaturesBedsListPage.getName2();
        //Bed bed2 = new Bed(Integer.parseInt(order), name);
        Bed bed2 = new Bed(name);
        
        Serenity.setSessionVariable("beforeOrderBed1").to(bed1);
        Serenity.setSessionVariable("beforeOrderBed2").to(bed2);
    }
    
    @Step
    public void change_order_descending() {
        nomenclaturesBedsListPage.changeOrderDesc();
        String msg = ConfirmationPopupFactory.getConfirmationPopup(getDriver()).getSuccessMessage();
        Serenity.setSessionVariable("confirmationMessage").to(msg);
    }
    
    @Step
    public void store_data_after_order() {
        //String order = nomenclaturesBedsListPage.getDisplayOrder1();
        String name = nomenclaturesBedsListPage.getName1();
        //Bed bed1 = new Bed(Integer.parseInt(order), name);
        Bed bed1 = new Bed(name);
        //order = nomenclaturesBedsListPage.getDisplayOrder2();
        name = nomenclaturesBedsListPage.getName2();
        //Bed bed2 = new Bed(Integer.parseInt(order), name);
        Bed bed2 = new Bed(name);
        
        Serenity.setSessionVariable("afterOrderBed1").to(bed1);
        Serenity.setSessionVariable("afterOrderBed2").to(bed2);
    }
    
    @Step
    public void click_add_beds_button() {
        nomenclaturesBedsListPage.clickAddNew();
    }
    
    @Step
    public void click_show_deleted_items_checkbox() {
        nomenclaturesBedsListPage.clickShowDeletedItems();
    }
    
    @Step
    public void click_edit_beds_button(String text) {
        NomenclaturesBedsDetailPage nomenclaturesBedsDetailPage = nomenclaturesBedsListPage.clickEdit(text);
        //Store id to using for go to detail page with id
        assertThat(nomenclaturesBedsDetailPage.isAtEditPage()).isTrue();
        assertThat(nomenclaturesBedsDetailPage.isDisplayId()).isTrue();
        String id = StringHelper.extractId(nomenclaturesBedsDetailPage.getId());
        System.out.println("SELECTED ID: " + id);
        Serenity.setSessionVariable("selectedId").to(id);
    }

    @Step
    public void is_display_button(String text) {
        assertThat(nomenclaturesBedsListPage.isDisplayButton(text)).isTrue();
    }

    @Step
    public void is_display_checkbox(String text) {
        assertThat(nomenclaturesBedsListPage.isDisplayCheckboxCaption(text)).isTrue();
        assertThat(nomenclaturesBedsListPage.isDisplayCheckbox(text)).isTrue();
    }
    
    @Step
    public void should_see_order_on_bed_list_page() {
        Bed bed1 = Serenity.sessionVariableCalled("afterOrderBed1");
        Bed bed2 = Serenity.sessionVariableCalled("afterOrderBed2");
        assertThat(nomenclaturesBedsListPage.getName1()).isEqualTo(bed1.getName());
        assertThat(nomenclaturesBedsListPage.getName2()).isEqualTo(bed2.getName());
    }

    @Step
    public void should_not_see_the_record_just_deleted_on_beds_list_page(String name) {
        waitABit(1000);
        assertThat(nomenclaturesBedsListPage.isDisplayedItem(name)).isFalse();
    }

    @Step
    public void should_see_bread_scrumb(String breadScrumb) {
        assertThat(nomenclaturesBedsListPage.getBreadScrumb().replace("\n", "")).isEqualTo(breadScrumb);
    }

    @Step
    public void should_see_column_name(int columnIndex, String columnName) {
        assertThat(nomenclaturesBedsListPage.getColumnName(columnIndex)).isEqualTo(columnName);
    }

    @Step
    public void click_change_display_order_on_front_page() {
        nomenclaturesBedsListPage.changeOrderOnFrontPage();
    }
    
    @Step
    public void is_display_pagination() {
        assertThat(nomenclaturesBedsListPage.getTotalRecord()).isNotNull();
        nomenclaturesBedsListPage.openDopdownSelectRecordPerPage();
        assertThat(nomenclaturesBedsListPage.isExistedRecordPerPage("10/page")).isTrue();
        assertThat(nomenclaturesBedsListPage.isExistedRecordPerPage("20/page")).isTrue();
        assertThat(nomenclaturesBedsListPage.isExistedRecordPerPage("30/page")).isTrue();
        assertThat(nomenclaturesBedsListPage.isExistedRecordPerPage("50/page")).isTrue();
        assertThat(nomenclaturesBedsListPage.isExistedNarrowLeft()).isTrue();
        assertThat(nomenclaturesBedsListPage.getCurrentPage()).isNotNull();
        assertThat(nomenclaturesBedsListPage.isExistedNarrowRight()).isTrue();
        assertThat(nomenclaturesBedsListPage.isExistedGoto()).isTrue();
        assertThat(nomenclaturesBedsListPage.isExistedJumpToPage()).isTrue();
    }
}
