package com.dirox.bi.serenity;

import static org.assertj.core.api.Assertions.assertThat;

import com.dirox.bi.models.ADV;
import com.dirox.bi.models.Commercial;
import com.dirox.bi.models.Localisation;
import com.dirox.bi.models.Media;
import com.dirox.bi.models.Program;
import com.dirox.bi.pages.ConfirmationPopupFactory;
import com.dirox.bi.pages.OfferProgramDetailAdvTab;
import com.dirox.bi.pages.OfferProgramDetailCommercialTab;
import com.dirox.bi.pages.OfferProgramDetailLocalisationTab;
import com.dirox.bi.pages.OfferProgramDetailLogTab;
import com.dirox.bi.pages.OfferProgramDetailMediaTab;
import com.dirox.bi.pages.OfferProgramDetailPage;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class OfferProgramDetailSteps extends ScenarioSteps {
    private static final long serialVersionUID = 1L;
    private OfferProgramDetailPage offerProgramDetailPage;

    @Step
    public void is_on_add_new_offer_program_page() {
        assertThat(offerProgramDetailPage.isAtAddNewPage()).isTrue();
    }

    @Step
    public void input_data_on_required_info_tab(Program program) {
        if(program == null) {
            throw new RuntimeException("The inputted data for 'Program' is null. Please, re-check it.");
        }
        offerProgramDetailPage.inputDenomination(program.getDenomination()).inputDeliveryDate(program.getDeliveryDate()).inputAddress(program.getAddress())
                        .selectCity(program.getCity()).inputMainCityAround(program.getMainCityAround())
                        .inputMainCityAroundPostalCode(program.getMainCityAroundPostalCode()).selectCountry(program.getCountry())
                        .inputAccountingUnit(program.getAccountingUnit()).inputAccountingCenter(program.getAccountingCenter())
                        .inputCleOperationCommerciale(program.getCleOperationCommerciale());
    }

    @Step
    public void input_data_on_localisation_tab(Localisation localisation) {
        if(localisation == null) {
            throw new RuntimeException("The inputted data for 'Localisation' is null. Please, re-check it.");
        }
        OfferProgramDetailLocalisationTab localisationTab = offerProgramDetailPage.openOfferAddNewProgramLocalisationTab();
        assertThat(localisationTab.isCurrentPageAt()).isTrue();
        localisationTab.inputComplementAddress1(localisation.getComplementAddress1())
                        .inputComplementAddress2(localisation.getComplementAddress2()).inputComplementAddress3(localisation.getComplementAddress3())
                        .inputPostalCode(localisation.getPostalCode()).inputLongitude(localisation.getLongitude()).inputLatitude(localisation.getLatitude());
    }

    @Step
    public void input_data_on_adv_tab(ADV adv) {
        if(adv == null) {
            throw new RuntimeException("The inputted data for 'ADV' is null. Please, re-check it.");
        }
        OfferProgramDetailAdvTab advTab = offerProgramDetailPage.openOfferAddNewProgramAdvTab();
        assertThat(advTab.isCurrentPageAt()).isTrue();
        advTab.inputIBANNumber(adv.getIBANNumber()).inputProgramNotarySocialName(adv.getProgramNotarySocialName())
                        .inputProgramNotaryContactName(adv.getProgramNotaryContactName()).inputProgramNotaryPhoneNumber(adv.getProgramNotaryPhoneNumber())
                        .inputNotaryAddress(adv.getNotaryAddress());
    }

    @Step
    public void input_data_on_commercial_tab(Commercial commercial) {
        if(commercial == null) {
            throw new RuntimeException("The inputted data for 'Commercial' is null. Please, re-check it.");
        }
        OfferProgramDetailCommercialTab commercialTab = offerProgramDetailPage.openOfferAddNewProgramCommercialTab();
        assertThat(commercialTab.isCurrentPageAt()).isTrue();
        commercialTab.inputCommercialProgramPhoneNumber(commercial.getCommercialProgramPhoneNumber())
                        .inputCommercialOpeningDate(commercial.getCommercialOpeningDate()).inputTeasingDateStarts(commercial.getTeasingDateStarts())
                        .inputTeasingDateEnds(commercial.getTeasingDateEnds()).inputPrivateSellingCode(commercial.getPrivateSellingCode())
                        .inputProgramCommercialPunchLine(commercial.getProgramCommercialPunchLine())
                        .inputProgramCommercialArgumentShortText(commercial.getProgramCommercialArgumentShortText())
                        .inputProgramCommercialArgumentLongText(commercial.getProgramCommercialArgumentLongText())
                        .inputProgramCommercialArgumentLongHTML(commercial.getProgramCommercialArgumentLongHTML())
                        .inputOrbital3DModel(commercial.getOrbital3DModel());
    }

    @Step
    public void input_data_on_media_tab(Media media) {
        if(media == null) {
            throw new RuntimeException("The inputted data for 'Media' is null. Please, re-check it.");
        }
        OfferProgramDetailMediaTab mediaTab = offerProgramDetailPage.openOfferAddNewProgramMediaTab();
        assertThat(mediaTab.isCurrentPageAt()).isTrue();
        mediaTab.inputImmersive3DVisitURL(media.getImmersive3DVisitURL()).inputProgramSuburbVisit(media.getProgramSuburbVisit())
                        .inputProgramVideoURL(media.getProgramVideoURL()).inputProgramLegalGeneralNotice(media.getProgramLegalGeneralNotice())
                        .inputProgramExternalPerspective1(media.getProgramExternalPerspective1())
                        .inputProgramExternalPerspective2(media.getProgramExternalPerspective2())
                        .inputProgramInternalPerspective1(media.getProgramInternalPerspective1())
                        .inputProgramInternalPerspective2(media.getProgramInternalPerspective2())
                        .inputProgramExternalPerspective1Description(media.getProgramExternalPerspective1Description())
                        .inputProgramExternalPerspective2Description(media.getProgramExternalPerspective2Description())
                        .inputProgramInternalPerspective1Description(media.getProgramInternalPerspective1Description())
                        .inputProgramInternalPerspective2Description(media.getProgramInternalPerspective2Description())
                        .inputProgramGroundPlane(media.getProgramGroundPlane());
    }

    @Step
    public void click_save_button() {
        offerProgramDetailPage.clickSave();
        String msg = ConfirmationPopupFactory.getConfirmationPopup(getDriver()).getSuccessMessage();
        Serenity.setSessionVariable("confirmationMessage").to(msg);
        waitABit(5000); // delay time for data storing in DB
    }
    
    @Step
    public void click_delete_button() {
        offerProgramDetailPage.clickDelete();
    }
    
    //Edit mode
    @Step
    public void is_on_edit_offer_program_page() {
        assertThat(offerProgramDetailPage.isAtEditPage()).isTrue();
    }

    @Step
    public void see_information_on_required_info_tab(Program program) {
        if(program == null) {
            throw new RuntimeException("The expected result of 'Program' is null. Please, re-check it.");
        }
        waitABit(1000);
        assertThat(offerProgramDetailPage.getIdProgram()).isNotNull();
        assertThat(offerProgramDetailPage.getDenomination()).isEqualTo(program.getDenomination());
        assertThat(offerProgramDetailPage.getDeliveryDate()).isEqualTo(program.getDeliveryDate());
        assertThat(offerProgramDetailPage.getAddress()).isEqualTo(program.getAddress());
        assertThat(offerProgramDetailPage.getCity()).isEqualTo(program.getCity());
        assertThat(offerProgramDetailPage.getMainCityAround()).isEqualTo(program.getMainCityAround());
        assertThat(offerProgramDetailPage.getMainCityAroundPostalCode()).isEqualTo(program.getMainCityAroundPostalCode());
        assertThat(offerProgramDetailPage.getCountry()).isEqualTo(program.getCountry());
        assertThat(offerProgramDetailPage.getAccountingUnit()).isEqualTo(program.getAccountingUnit());
        assertThat(offerProgramDetailPage.getAccountingCenter()).isEqualTo(program.getAccountingCenter());
        assertThat(offerProgramDetailPage.getCleOperationCommerciale()).isEqualTo(program.getCleOperationCommerciale());
    }

    @Step
    public void see_information_on_localisation_tab(Localisation localisation) {
        if(localisation == null) {
            throw new RuntimeException("The expected result of 'Localisation' is null. Please, re-check it.");
        }
        OfferProgramDetailLocalisationTab localisationTab = offerProgramDetailPage.openOfferAddNewProgramLocalisationTab();
        assertThat(localisationTab.isCurrentPageAt()).isTrue();
        assertThat(localisationTab.getComplementAddress1()).isEqualTo(localisation.getComplementAddress1());
        assertThat(localisationTab.getComplementAddress2()).isEqualTo(localisation.getComplementAddress2());
        assertThat(localisationTab.getComplementAddress3()).isEqualTo(localisation.getComplementAddress3());
        assertThat(localisationTab.getPostalCode()).isEqualTo(localisation.getPostalCode());
        assertThat(localisationTab.getLongitude()).isEqualTo(localisation.getLongitude());
        assertThat(localisationTab.getLatitude()).isEqualTo(localisation.getLatitude());
    }

    @Step
    public void see_information_on_adv_tab(ADV adv) {
        if(adv == null) {
            throw new RuntimeException("The expected result of 'ADV' is null. Please, re-check it.");
        }
        OfferProgramDetailAdvTab advTab = offerProgramDetailPage.openOfferAddNewProgramAdvTab();
        assertThat(advTab.isCurrentPageAt()).isTrue();
        assertThat(advTab.getIBANNumber()).isEqualTo(adv.getIBANNumber());
        assertThat(advTab.getProgramNotarySocialName()).isEqualTo(adv.getProgramNotarySocialName());
        assertThat(advTab.getProgramNotaryContactName()).isEqualTo(adv.getProgramNotaryContactName());
        assertThat(advTab.getProgramNotaryPhoneNumber()).isEqualTo(adv.getProgramNotaryPhoneNumber());
        assertThat(advTab.getNotaryAddress()).isEqualTo(adv.getNotaryAddress());
    }

    @Step
    public void see_information_on_commercial_tab(Commercial commercial) {
        if(commercial == null) {
            throw new RuntimeException("The expected result of 'Commercial' is null. Please, re-check it.");
        }
        OfferProgramDetailCommercialTab commercialTab = offerProgramDetailPage.openOfferAddNewProgramCommercialTab();
        assertThat(commercialTab.isCurrentPageAt()).isTrue();
        assertThat(commercialTab.getCommercialProgramPhoneNumber()).isEqualTo(commercial.getCommercialProgramPhoneNumber());
        assertThat(commercialTab.getCommercialOpeningDate()).isEqualTo(commercial.getCommercialOpeningDate());
        assertThat(commercialTab.getTeasingDateStarts()).isEqualTo(commercial.getTeasingDateStarts());
        assertThat(commercialTab.getTeasingDateEnds()).isEqualTo(commercial.getTeasingDateEnds());
        assertThat(commercialTab.getPrivateSellingCode()).isEqualTo(commercial.getPrivateSellingCode());
        assertThat(commercialTab.getProgramCommercialPunchLine()).isEqualTo(commercial.getProgramCommercialPunchLine());
        assertThat(commercialTab.getProgramCommercialArgumentShortText()).isEqualTo(commercial.getProgramCommercialArgumentShortText());
        assertThat(commercialTab.getProgramCommercialArgumentLongText()).isEqualTo(commercial.getProgramCommercialArgumentLongText());
        //Scroll to text 
        commercialTab.scrollTo("Orbital3DModel");
        assertThat(commercialTab.getProgramCommercialArgumentLongHTML()).isEqualTo(commercial.getProgramCommercialArgumentLongHTML());
        assertThat(commercialTab.getOrbital3DModel()).isEqualTo(commercial.getOrbital3DModel());
    }
    
    @Step
    public void see_information_on_media_tab(Media media) {
        if(media == null) {
            throw new RuntimeException("The expected result of 'Media' is null. Please, re-check it.");
        }
        OfferProgramDetailMediaTab mediaTab = offerProgramDetailPage.openOfferAddNewProgramMediaTab();
        assertThat(mediaTab.isCurrentPageAt()).isTrue();
        assertThat(mediaTab.getImmersive3DVisitURL()).isEqualTo(media.getImmersive3DVisitURL());
        assertThat(mediaTab.getProgramSuburbVisit()).isEqualTo(media.getProgramSuburbVisit());
        assertThat(mediaTab.getProgramVideoURL()).isEqualTo(media.getProgramVideoURL());
        assertThat(mediaTab.getProgramLegalGeneralNotice()).isEqualTo(media.getProgramLegalGeneralNotice());
        assertThat(mediaTab.getProgramExternalPerspective1()).isEqualTo(media.getProgramExternalPerspective1());
        assertThat(mediaTab.getProgramExternalPerspective2()).isEqualTo(media.getProgramExternalPerspective2());
        assertThat(mediaTab.getProgramInternalPerspective1()).isEqualTo(media.getProgramInternalPerspective1());
        assertThat(mediaTab.getProgramInternalPerspective2()).isEqualTo(media.getProgramInternalPerspective2());
        assertThat(mediaTab.getProgramExternalPerspective1Description()).isEqualTo(media.getProgramExternalPerspective1Description());
        assertThat(mediaTab.getProgramExternalPerspective2Description()).isEqualTo(media.getProgramExternalPerspective2Description());
        assertThat(mediaTab.getProgramInternalPerspective1Description()).isEqualTo(media.getProgramInternalPerspective1Description());
        assertThat(mediaTab.getProgramInternalPerspective2Description()).isEqualTo(media.getProgramInternalPerspective2Description());
        assertThat(mediaTab.getProgramGroundPlane()).isEqualTo(media.getProgramGroundPlane());
    }
    
    @Step
    public void see_information_on_log_tab() {
        OfferProgramDetailLogTab logTab = offerProgramDetailPage.openOfferAddNewProgramLogTab();
        assertThat(logTab.isCurrentPageAt()).isTrue();
        assertThat(logTab.getLastModificationDate()).isNotNull();
        assertThat(logTab.getIdAdminModified()).isNotNull();
        assertThat(logTab.getCreationDate()).isNotNull();
        assertThat(logTab.getIdAdminCreated()).isNotNull();
        assertThat(logTab.getIsDeleted()).isNotNull();
    }

}
