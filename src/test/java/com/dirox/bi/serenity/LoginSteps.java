package com.dirox.bi.serenity;

import static org.assertj.core.api.Assertions.assertThat;

import com.dirox.bi.models.SMTP;
import com.dirox.bi.models.User;
import com.dirox.bi.pages.AdminDashBoardPage;
import com.dirox.bi.pages.EtherealPage;
import com.dirox.bi.pages.LoginPage;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class LoginSteps extends ScenarioSteps{
    private static final long serialVersionUID = 1L;
    private LoginPage loginPage;
    
    @Step
    public void opens_login_page() {
        loginPage.open();
    }
    
    @Step
    public void login_to_the_sytem_successfully(User user) {
        assertThat(loginPage.isCurrentPageAt()).isEqualTo(true);
        loginPage.inputEmail(user.getEmail()).clickSubmit();
        assertThat(loginPage.isPerfect()).isEqualTo(true);
        SMTP smtp = user.getSmtp();
        EtherealPage etherealPage = loginPage.goToEthereal();
        AdminDashBoardPage adminDashBoardPage = etherealPage.openAuthentication(smtp.getSmtpEmail(), smtp.getSmtpPassword());
        assertThat(adminDashBoardPage.isCurrentPageAt()).isEqualTo(true);
    }
    
}
