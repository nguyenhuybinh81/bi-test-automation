package com.dirox.bi.serenity;

import static org.assertj.core.api.Assertions.assertThat;

import com.dirox.bi.models.CommercializationChannel;
import com.dirox.bi.pages.ConfirmationPopupFactory;
import com.dirox.bi.pages.NomenclaturesCommercializationChannelDetailPage;
import com.dirox.bi.pages.NomenclaturesCommercializationChannelListPage;
import com.dirox.bi.utils.StringHelper;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class NomenclaturesCommercializationChannelListSteps extends ScenarioSteps {
    private static final long serialVersionUID = 1L;
    private NomenclaturesCommercializationChannelListPage nomenclaturesCommercializationChannelListPage;

    @Step
    public void open_commercialization_channel_list_page() {
        nomenclaturesCommercializationChannelListPage.open();
    }

    @Step
    public void is_on_nomenclatures_commercialization_channel_list_page() {
        assertThat(nomenclaturesCommercializationChannelListPage.isCurrentPageAt()).isTrue();
    }

    @Step
    public void click_done_button() {
        nomenclaturesCommercializationChannelListPage.clickDone();
    }
    
    @Step
    public void store_data_before_order() {
        String name = nomenclaturesCommercializationChannelListPage.getName1();
        CommercializationChannel commercializationChannel1 = new CommercializationChannel(name);
        name = nomenclaturesCommercializationChannelListPage.getName2();
        CommercializationChannel commercializationChannel2 = new CommercializationChannel(name);
        
        Serenity.setSessionVariable("beforeOrderCommercializationChannel1").to(commercializationChannel1);
        Serenity.setSessionVariable("beforeOrderCommercializationChannel2").to(commercializationChannel2);
    }
    
    @Step
    public void change_order_descending() {
        nomenclaturesCommercializationChannelListPage.changeOrderDesc();
        String msg = ConfirmationPopupFactory.getConfirmationPopup(getDriver()).getSuccessMessage();
        Serenity.setSessionVariable("confirmationMessage").to(msg);
    }
    
    @Step
    public void store_data_after_order() {
        String name = nomenclaturesCommercializationChannelListPage.getName1();
        CommercializationChannel commercializationChannel1 = new CommercializationChannel(name);
        name = nomenclaturesCommercializationChannelListPage.getName2();
        CommercializationChannel commercializationChannel2 = new CommercializationChannel(name);
        
        Serenity.setSessionVariable("afterOrderCommercializationChannel1").to(commercializationChannel1);
        Serenity.setSessionVariable("afterOrderCommercializationChannel2").to(commercializationChannel2);
    }
    
    @Step
    public void click_add_commercialization_channel_button() {
        nomenclaturesCommercializationChannelListPage.clickAddNew();
    }
    
    @Step
    public void click_show_deleted_items_checkbox() {
        nomenclaturesCommercializationChannelListPage.clickShowDeletedItems();
    }
    
    @Step
    public void click_edit_commercialization_channel_button(String text) {
        NomenclaturesCommercializationChannelDetailPage nomenclaturesCommercializationChannelDetailPage = nomenclaturesCommercializationChannelListPage.clickEdit(text);
        //Store id to using for go to detail page with id
        assertThat(nomenclaturesCommercializationChannelDetailPage.isAtEditPage()).isTrue();
        assertThat(nomenclaturesCommercializationChannelDetailPage.isDisplayId()).isTrue();
        String id = StringHelper.extractId(nomenclaturesCommercializationChannelDetailPage.getId());
        System.out.println("SELECTED ID: " + id);
        Serenity.setSessionVariable("selectedId").to(id);
    }

    @Step
    public void is_display_button(String text) {
        assertThat(nomenclaturesCommercializationChannelListPage.isDisplayButton(text)).isTrue();
    }

    @Step
    public void is_display_checkbox(String text) {
        assertThat(nomenclaturesCommercializationChannelListPage.isDisplayCheckboxCaption(text)).isTrue();
        assertThat(nomenclaturesCommercializationChannelListPage.isDisplayCheckbox(text)).isTrue();
    }
    
    @Step
    public void should_see_order_on_commercialization_channel_list_page() {
        CommercializationChannel commercializationChannel1 = Serenity.sessionVariableCalled("afterOrderCommercializationChannel1");
        CommercializationChannel commercializationChannel2 = Serenity.sessionVariableCalled("afterOrderCommercializationChannel2");
        assertThat(nomenclaturesCommercializationChannelListPage.getName1()).isEqualTo(commercializationChannel1.getName());
        assertThat(nomenclaturesCommercializationChannelListPage.getName2()).isEqualTo(commercializationChannel2.getName());
    }

    @Step
    public void should_not_see_the_record_just_deleted_on_commercialization_channel_list_page(String name) {
        waitABit(1000);
        assertThat(nomenclaturesCommercializationChannelListPage.isDisplayedItem(name)).isFalse();
    }

    @Step
    public void should_see_bread_scrumb(String breadScrumb) {
        assertThat(nomenclaturesCommercializationChannelListPage.getBreadScrumb().replace("\n", "")).isEqualTo(breadScrumb);
    }

    @Step
    public void should_see_column_name(int columnIndex, String columnName) {
        assertThat(nomenclaturesCommercializationChannelListPage.getColumnName(columnIndex)).isEqualTo(columnName);
    }

    @Step
    public void click_change_display_order_on_front_page() {
        nomenclaturesCommercializationChannelListPage.changeOrderOnFrontPage();
    }
    
    @Step
    public void is_display_pagination() {
        assertThat(nomenclaturesCommercializationChannelListPage.getTotalRecord()).isNotNull();
        nomenclaturesCommercializationChannelListPage.openDopdownSelectRecordPerPage();
        assertThat(nomenclaturesCommercializationChannelListPage.isExistedRecordPerPage("10/page")).isTrue();
        assertThat(nomenclaturesCommercializationChannelListPage.isExistedRecordPerPage("20/page")).isTrue();
        assertThat(nomenclaturesCommercializationChannelListPage.isExistedRecordPerPage("30/page")).isTrue();
        assertThat(nomenclaturesCommercializationChannelListPage.isExistedRecordPerPage("50/page")).isTrue();
        assertThat(nomenclaturesCommercializationChannelListPage.isExistedNarrowLeft()).isTrue();
        assertThat(nomenclaturesCommercializationChannelListPage.getCurrentPage()).isNotNull();
        assertThat(nomenclaturesCommercializationChannelListPage.isExistedNarrowRight()).isTrue();
        assertThat(nomenclaturesCommercializationChannelListPage.isExistedGoto()).isTrue();
        assertThat(nomenclaturesCommercializationChannelListPage.isExistedJumpToPage()).isTrue();
    }
}
