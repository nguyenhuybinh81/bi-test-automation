package com.dirox.bi.serenity;

import static org.assertj.core.api.Assertions.assertThat;

import com.dirox.bi.models.Consent;
import com.dirox.bi.pages.ConfirmationPopupFactory;
import com.dirox.bi.pages.NomenclaturesConsentDetailPage;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class NomenclaturesConsentDetailSteps extends ScenarioSteps {
    private static final long serialVersionUID = 1L;
    private NomenclaturesConsentDetailPage nomenclaturesConsentDetailPage;

    @Step
    public void is_on_add_new_consent_page() {
        assertThat(nomenclaturesConsentDetailPage.isAtAddNewPage()).isTrue();
    }

    @Step
    public void input_data(Consent consent) {
        if (consent == null) {
            throw new RuntimeException("The inputted data for 'Consent' is null. Please, re-check it.");
        }
        waitABit(1000);
        nomenclaturesConsentDetailPage.inputQuestion(consent.getQuestion());
    }

    @Step
    public void click_save_button() {
        nomenclaturesConsentDetailPage.clickSave();
        String msg = ConfirmationPopupFactory.getConfirmationPopup(getDriver()).getSuccessMessage();
        Serenity.setSessionVariable("confirmationMessage").to(msg);
        waitABit(5000); // delay time for data storing in DB
    }

    @Step
    public void click_delete_button() {
        nomenclaturesConsentDetailPage.clickDelete();
    }

    @Step
    public void click_cancel_button() {
        nomenclaturesConsentDetailPage.clickCancel();
    }

    @Step
    public void click_restore_button() {
        nomenclaturesConsentDetailPage.clickRestore();
        String msg = ConfirmationPopupFactory.getConfirmationPopup(getDriver()).getSuccessMessage();
        Serenity.setSessionVariable("confirmationMessage").to(msg);
    }

    // Edit mode
    @Step
    public void is_on_edit_consent_page() {
        assertThat(nomenclaturesConsentDetailPage.isAtEditPage()).isTrue();
    }

    @Step
    public void go_to_detail_page(String id) {
        nomenclaturesConsentDetailPage.openDetail(id);
    }
    
    @Step
    public void should_see_information(Consent consent) {
        if (consent == null) {
            throw new RuntimeException("The expected result of 'Consent' is null. Please, re-check it.");
        }
        waitABit(1000);
        assertThat(nomenclaturesConsentDetailPage.getId()).isNotNull();
        assertThat(nomenclaturesConsentDetailPage.getName()).isEqualTo(consent.getQuestion());
        assertThat(nomenclaturesConsentDetailPage.getLastModificationDate()).isNotNull();
        assertThat(nomenclaturesConsentDetailPage.getIdAdminModified()).isNotNull();
        assertThat(nomenclaturesConsentDetailPage.getCreationDate()).isNotNull();
        assertThat(nomenclaturesConsentDetailPage.getIdAdminCreated()).isNotNull();
        assertThat(nomenclaturesConsentDetailPage.getIsDeleted()).isEqualTo(consent.getIsDeleted());
    }

    @Step
    public void should_see_validation_message(Consent consent) {
        if (consent == null) {
            throw new RuntimeException("The expected result of 'Consent' is null. Please, re-check it.");
        }
        assertThat(nomenclaturesConsentDetailPage.getErrorNameMessage()).isEqualTo(consent.getQuestion());
    }
}
