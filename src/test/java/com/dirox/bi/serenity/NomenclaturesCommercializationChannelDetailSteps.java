package com.dirox.bi.serenity;

import static org.assertj.core.api.Assertions.assertThat;

import com.dirox.bi.models.CommercializationChannel;
import com.dirox.bi.pages.ConfirmationPopupFactory;
import com.dirox.bi.pages.NomenclaturesCommercializationChannelDetailPage;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class NomenclaturesCommercializationChannelDetailSteps extends ScenarioSteps {
    private static final long serialVersionUID = 1L;
    private NomenclaturesCommercializationChannelDetailPage nomenclaturesCommercializationChannelDetailPage;

    @Step
    public void is_on_add_new_commercialization_channel_page() {
        assertThat(nomenclaturesCommercializationChannelDetailPage.isAtAddNewPage()).isTrue();
    }

    @Step
    public void input_data(CommercializationChannel commercializationChannel) {
        if (commercializationChannel == null) {
            throw new RuntimeException("The inputted data for 'Commercialization Channel' is null. Please, re-check it.");
        }
        nomenclaturesCommercializationChannelDetailPage.inputName(commercializationChannel.getName());
    }

    @Step
    public void click_save_button() {
        nomenclaturesCommercializationChannelDetailPage.clickSave();
        String msg = ConfirmationPopupFactory.getConfirmationPopup(getDriver()).getSuccessMessage();
        Serenity.setSessionVariable("confirmationMessage").to(msg);
        waitABit(5000); // delay time for data storing in DB
    }

    @Step
    public void click_delete_button() {
        nomenclaturesCommercializationChannelDetailPage.clickDelete();
    }

    @Step
    public void click_cancel_button() {
        nomenclaturesCommercializationChannelDetailPage.clickCancel();
    }

    @Step
    public void click_restore_button() {
        nomenclaturesCommercializationChannelDetailPage.clickRestore();
        String msg = ConfirmationPopupFactory.getConfirmationPopup(getDriver()).getSuccessMessage();
        Serenity.setSessionVariable("confirmationMessage").to(msg);
    }

    // Edit mode
    @Step
    public void is_on_edit_commercialization_channel_page() {
        assertThat(nomenclaturesCommercializationChannelDetailPage.isAtEditPage()).isTrue();
    }

    @Step
    public void go_to_detail_page(String id) {
        nomenclaturesCommercializationChannelDetailPage.openDetail(id);
    }
    
    @Step
    public void should_see_information(CommercializationChannel commercializationChannel) {
        if (commercializationChannel == null) {
            throw new RuntimeException("The expected result of 'Program' is null. Please, re-check it.");
        }
        waitABit(1000);
        assertThat(nomenclaturesCommercializationChannelDetailPage.getId()).isNotNull();
        assertThat(nomenclaturesCommercializationChannelDetailPage.getName()).isEqualTo(commercializationChannel.getName());
        assertThat(nomenclaturesCommercializationChannelDetailPage.getLastModificationDate()).isNotNull();
        assertThat(nomenclaturesCommercializationChannelDetailPage.getIdAdminModified()).isNotNull();
        assertThat(nomenclaturesCommercializationChannelDetailPage.getCreationDate()).isNotNull();
        assertThat(nomenclaturesCommercializationChannelDetailPage.getIdAdminCreated()).isNotNull();
        assertThat(nomenclaturesCommercializationChannelDetailPage.getIsDeleted()).isEqualTo(commercializationChannel.getIsDeleted());
    }

    @Step
    public void should_see_validation_message(CommercializationChannel commercializationChannel) {
        if (commercializationChannel == null) {
            throw new RuntimeException("The expected result of 'Commercialization Channel' is null. Please, re-check it.");
        }
        assertThat(nomenclaturesCommercializationChannelDetailPage.getErrorNameMessage()).isEqualTo(commercializationChannel.getName());
    }
}
