package com.dirox.bi.serenity;

import static org.assertj.core.api.Assertions.assertThat;

import com.dirox.bi.models.Consent;
import com.dirox.bi.pages.ConfirmationPopupFactory;
import com.dirox.bi.pages.NomenclaturesConsentDetailPage;
import com.dirox.bi.pages.NomenclaturesConsentlListPage;
import com.dirox.bi.utils.StringHelper;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class NomenclaturesConsentListSteps extends ScenarioSteps {
    private static final long serialVersionUID = 1L;
    private NomenclaturesConsentlListPage nomenclaturesConsentlListPage;

    @Step
    public void open_consent_list_page() {
        nomenclaturesConsentlListPage.open();
    }

    @Step
    public void is_on_nomenclatures_consent_list_page() {
        assertThat(nomenclaturesConsentlListPage.isCurrentPageAt()).isTrue();
    }

    @Step
    public void click_done_button() {
        nomenclaturesConsentlListPage.clickDone();
    }
    
    @Step
    public void store_data_before_order() {
        String name = nomenclaturesConsentlListPage.getName1();
        Consent consent1 = new Consent(name);
        name = nomenclaturesConsentlListPage.getName2();
        Consent consent2 = new Consent(name);
        
        Serenity.setSessionVariable("beforeOrderConsent1").to(consent1);
        Serenity.setSessionVariable("beforeOrderConsent2").to(consent2);
    }
    
    @Step
    public void change_order_descending() {
        nomenclaturesConsentlListPage.changeOrderDesc();
        String msg = ConfirmationPopupFactory.getConfirmationPopup(getDriver()).getSuccessMessage();
        Serenity.setSessionVariable("confirmationMessage").to(msg);
    }
    
    @Step
    public void store_data_after_order() {
        String name = nomenclaturesConsentlListPage.getName1();
        Consent consent1 = new Consent(name);
        name = nomenclaturesConsentlListPage.getName2();
        Consent consent2 = new Consent(name);
        
        Serenity.setSessionVariable("afterOrderConsent1").to(consent1);
        Serenity.setSessionVariable("afterOrderConsent2").to(consent2);
    }
    
    @Step
    public void click_add_consent_button() {
        nomenclaturesConsentlListPage.clickAddNew();
    }
    
    @Step
    public void click_show_deleted_items_checkbox() {
        nomenclaturesConsentlListPage.clickShowDeletedItems();
    }
    
    @Step
    public void click_edit_consent_button(String text) {
        NomenclaturesConsentDetailPage nomenclaturesConsentDetailPage = nomenclaturesConsentlListPage.clickEdit(text);
        //Store id to using for go to detail page with id
        assertThat(nomenclaturesConsentDetailPage.isAtEditPage()).isTrue();
        assertThat(nomenclaturesConsentDetailPage.isDisplayId()).isTrue();
        String id = StringHelper.extractId(nomenclaturesConsentDetailPage.getId());
        System.out.println("SELECTED ID: " + id);
        Serenity.setSessionVariable("selectedId").to(id);
    }

    @Step
    public void is_display_button(String text) {
        assertThat(nomenclaturesConsentlListPage.isDisplayButton(text)).isTrue();
    }

    @Step
    public void is_display_checkbox(String text) {
        assertThat(nomenclaturesConsentlListPage.isDisplayCheckboxCaption(text)).isTrue();
        assertThat(nomenclaturesConsentlListPage.isDisplayCheckbox(text)).isTrue();
    }
    
    @Step
    public void should_see_order_on_consent_list_page() {
        Consent consent1 = Serenity.sessionVariableCalled("afterOrderConsent1");
        Consent consent2 = Serenity.sessionVariableCalled("afterOrderConsent2");
        assertThat(nomenclaturesConsentlListPage.getName1()).isEqualTo(consent1.getQuestion());
        assertThat(nomenclaturesConsentlListPage.getName2()).isEqualTo(consent2.getQuestion());
    }

    @Step
    public void should_not_see_the_record_just_deleted_on_consent_list_page(String name) {
        waitABit(1000);
        assertThat(nomenclaturesConsentlListPage.isDisplayedItem(name)).isFalse();
    }

    @Step
    public void should_see_bread_scrumb(String breadScrumb) {
        assertThat(nomenclaturesConsentlListPage.getBreadScrumb().replace("\n", "")).isEqualTo(breadScrumb);
    }

    @Step
    public void should_see_column_name(int columnIndex, String columnName) {
        assertThat(nomenclaturesConsentlListPage.getColumnName(columnIndex)).isEqualTo(columnName);
    }

    @Step
    public void click_change_display_order_on_front_page() {
        nomenclaturesConsentlListPage.changeOrderOnFrontPage();
    }
    
    @Step
    public void is_display_pagination() {
        assertThat(nomenclaturesConsentlListPage.getTotalRecord()).isNotNull();
        nomenclaturesConsentlListPage.openDopdownSelectRecordPerPage();
        assertThat(nomenclaturesConsentlListPage.isExistedRecordPerPage("10/page")).isTrue();
        assertThat(nomenclaturesConsentlListPage.isExistedRecordPerPage("20/page")).isTrue();
        assertThat(nomenclaturesConsentlListPage.isExistedRecordPerPage("30/page")).isTrue();
        assertThat(nomenclaturesConsentlListPage.isExistedRecordPerPage("50/page")).isTrue();
        assertThat(nomenclaturesConsentlListPage.isExistedNarrowLeft()).isTrue();
        assertThat(nomenclaturesConsentlListPage.getCurrentPage()).isNotNull();
        assertThat(nomenclaturesConsentlListPage.isExistedNarrowRight()).isTrue();
        assertThat(nomenclaturesConsentlListPage.isExistedGoto()).isTrue();
        assertThat(nomenclaturesConsentlListPage.isExistedJumpToPage()).isTrue();
    }
}
