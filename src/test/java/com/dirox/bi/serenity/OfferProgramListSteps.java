package com.dirox.bi.serenity;

import static org.assertj.core.api.Assertions.assertThat;

import com.dirox.bi.pages.OfferProgramListPage;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class OfferProgramListSteps extends ScenarioSteps{
    private static final long serialVersionUID = 1L;
    private OfferProgramListPage offerProgramListPage;
    
    @Step
    public void open_program_list_page() {
        offerProgramListPage.open();
    }
    
    @Step
    public void is_on_offer_program_list_page() {
        assertThat(offerProgramListPage.isCurrentPageAt()).isTrue();
    }
    
    @Step
    public void click_add_program_button() {
        offerProgramListPage.addProgram();
    }
    
    @Step
    public void click_edit_program_button(String denomination) {
        offerProgramListPage.editProgram(denomination);
    }
    
    @Step
    public void should_not_see_the_record_just_deleted_on_program_list_page(String denomination) {
        waitABit(1000);
        assertThat(offerProgramListPage.isDisplayedProgram(denomination)).isFalse();
    }
}
