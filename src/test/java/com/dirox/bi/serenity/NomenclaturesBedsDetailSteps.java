package com.dirox.bi.serenity;

import static org.assertj.core.api.Assertions.assertThat;

import com.dirox.bi.models.Bed;
import com.dirox.bi.pages.ConfirmationPopupFactory;
import com.dirox.bi.pages.NomenclaturesBedsDetailPage;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class NomenclaturesBedsDetailSteps extends ScenarioSteps {
    private static final long serialVersionUID = 1L;
    private NomenclaturesBedsDetailPage nomenclaturesBedsDetailPage;

    @Step
    public void is_on_add_new_beds_page() {
        assertThat(nomenclaturesBedsDetailPage.isAtAddNewPage()).isTrue();
    }

    @Step
    public void input_data(Bed bed) {
        if (bed == null) {
            throw new RuntimeException("The inputted data for 'Bed' is null. Please, re-check it.");
        }
        nomenclaturesBedsDetailPage.inputName(bed.getName()).inputNumberOfBedsForAdults(bed.getNumberOfBedsForAdults())
                        .inputNumberOfBedsForChild(bed.getNumberOfBedsForChild()).uploadImage(bed.getUploadPicture());
    }

    @Step
    public void click_save_button() {
        nomenclaturesBedsDetailPage.clickSave();
        String msg = ConfirmationPopupFactory.getConfirmationPopup(getDriver()).getSuccessMessage();
        Serenity.setSessionVariable("confirmationMessage").to(msg);
        waitABit(5000); // delay time for data storing in DB
    }

    @Step
    public void click_delete_button() {
        nomenclaturesBedsDetailPage.clickDelete();
    }

    @Step
    public void click_cancel_button() {
        nomenclaturesBedsDetailPage.clickCancel();
    }

    @Step
    public void click_restore_button() {
        nomenclaturesBedsDetailPage.clickRestore();
        String msg = ConfirmationPopupFactory.getConfirmationPopup(getDriver()).getSuccessMessage();
        Serenity.setSessionVariable("confirmationMessage").to(msg);
    }

    // Edit mode
    @Step
    public void is_on_edit_beds_page() {
        assertThat(nomenclaturesBedsDetailPage.isAtEditPage()).isTrue();
    }

    @Step
    public void go_to_detail_page(String id) {
        nomenclaturesBedsDetailPage.openDetail(id);
    }
    
    @Step
    public void should_see_information(Bed bed) {
        if (bed == null) {
            throw new RuntimeException("The expected result of 'Bed' is null. Please, re-check it.");
        }
        waitABit(1000);
        assertThat(nomenclaturesBedsDetailPage.getId()).isNotNull();
        assertThat(nomenclaturesBedsDetailPage.getName()).isEqualTo(bed.getName());
        assertThat(nomenclaturesBedsDetailPage.getNumberOfBedsForAdults()).isEqualTo(bed.getNumberOfBedsForAdults());
        assertThat(nomenclaturesBedsDetailPage.getNumberOfBedsForChild()).isEqualTo(bed.getNumberOfBedsForChild());
        if (bed.getUploadPicture() == null) {
            assertThat(nomenclaturesBedsDetailPage.getUploadedImage()).isEqualTo("");
        } else {
            assertThat(nomenclaturesBedsDetailPage.getUploadedImage()).isNotNull();
        }
        assertThat(nomenclaturesBedsDetailPage.getLastModificationDate()).isNotNull();
        assertThat(nomenclaturesBedsDetailPage.getIdAdminModified()).isNotNull();
        assertThat(nomenclaturesBedsDetailPage.getCreationDate()).isNotNull();
        assertThat(nomenclaturesBedsDetailPage.getIdAdminCreated()).isNotNull();
        assertThat(nomenclaturesBedsDetailPage.getIsDeleted()).isEqualTo(bed.getIsDeleted());
    }

    @Step
    public void should_see_validation_message(Bed bed) {
        if (bed == null) {
            throw new RuntimeException("The expected result of 'Bed' is null. Please, re-check it.");
        }
        assertThat(nomenclaturesBedsDetailPage.getErrorNameMessage()).isEqualTo(bed.getName());
        assertThat(nomenclaturesBedsDetailPage.getErrorNumberOfBedsForAdultsMessage()).isEqualTo(bed.getNumberOfBedsForAdults());
        assertThat(nomenclaturesBedsDetailPage.getErrorNumberOfBedsForChildMessage()).isEqualTo(bed.getNumberOfBedsForChild());
    }
}
