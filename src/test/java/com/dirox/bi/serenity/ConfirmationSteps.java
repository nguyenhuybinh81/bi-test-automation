package com.dirox.bi.serenity;

import static org.assertj.core.api.Assertions.assertThat;

import com.dirox.bi.pages.ConfirmationPopupFactory;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class ConfirmationSteps extends ScenarioSteps{
    private static final long serialVersionUID = 1L;

    @Step
    public void click_delete_anyway_button_on_popup() {
        assertThat(ConfirmationPopupFactory.getConfirmationPopup(getDriver()).isCurrentPageAt()).isTrue();
        ConfirmationPopupFactory.getConfirmationPopup(getDriver()).clickDeleteAnyway();
        String msg = ConfirmationPopupFactory.getConfirmationPopup(getDriver()).getSuccessMessage();
        Serenity.setSessionVariable("confirmationMessage").to(msg);
        //set status after deleting
        Serenity.setSessionVariable("isDeleted").to(true);
    }
    
    @Step
    public void click_cancel_button_on_popup() {
        assertThat(ConfirmationPopupFactory.getConfirmationPopup(getDriver()).isCurrentPageAt()).isTrue();
        ConfirmationPopupFactory.getConfirmationPopup(getDriver()).clickCancel();
    }
    
    @Step
    public void see_confirmation_message_on_popup(String message) {
        String actualMsg = Serenity.sessionVariableCalled("confirmationMessage");
        assertThat(actualMsg).isEqualTo(message);
    }
    
}
