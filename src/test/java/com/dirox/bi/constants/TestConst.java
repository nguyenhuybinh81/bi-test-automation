package com.dirox.bi.constants;

public class TestConst {
    public static final String DATA_PATH = "src/test/resources/test-data/";
    public static final String IMAGES_PATH = "images/";
    public static final String SMTP_HOST = "https://ethereal.email/messages";
    
}
